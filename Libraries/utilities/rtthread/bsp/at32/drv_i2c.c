#include <rtdevice.h>
#include <rthw.h>
#include "ch0x0_board.h"

#define SDA_PIN     (9)
#define SCL_PIN     (8)




static void at32_set_sda(void *data, rt_int32_t state)
{
    PAout(SDA_PIN) = state;
   // rt_pin_write(SDA_PIN, state);
}

static void at32_set_scl(void *data, rt_int32_t state)
{
    PAout(SCL_PIN) = state;
    //rt_pin_write(SCL_PIN, state);
}

static rt_int32_t at32_get_sda(void *data)
{
    return PAin(SDA_PIN);
   // return rt_pin_read(SDA_PIN);
}

static rt_int32_t at32_get_scl(void *data)
{
    return PAin(SCL_PIN);
   // return rt_pin_read(SCL_PIN);
}

static void _udelay(rt_uint32_t us)
{
    __NOP();
    __NOP();
    __NOP();
    __NOP();
   // rt_hw_us_delay(us);
}


static const struct rt_i2c_bit_ops bit_ops = {
	RT_NULL,
	at32_set_sda,
	at32_set_scl,
	at32_get_sda,
	at32_get_scl,

	_udelay,

	1,
	2
};


int rt_i2c_init(void)
{
	struct rt_i2c_bus_device *bus;

	bus = rt_malloc(sizeof(struct rt_i2c_bus_device));

	rt_memset((void *)bus, 0, sizeof(struct rt_i2c_bus_device));

	bus->priv = (void *)&bit_ops;

	rt_i2c_bit_add_bus(bus, "i2c0");

    rt_pin_mode(SCL_PIN, PIN_MODE_OUTPUT_OD);
    rt_pin_mode(SDA_PIN, PIN_MODE_OUTPUT_OD);
    
	return RT_EOK;
}
