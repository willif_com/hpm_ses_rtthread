/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2022-03-24     shelton      first version
 */

#include <rtthread.h>
#include <rthw.h>
#include "ch0x0_board.h"


#ifdef BSP_USING_ON_CHIP_FLASH

#if defined(PKG_USING_FAL)
#include "fal.h"
#endif



/**
  * @brief  gets the page of a given address
  * @param  addr: address of the flash memory
  * @retval the page of a given address
  */
static rt_uint32_t get_page(uint32_t addr)
{
    rt_uint32_t page = 0;

    page = RT_ALIGN_DOWN(addr, SECTOR_SIZE);

    return page;
}

rt_size_t at32_flash_read(rt_device_t dev, rt_off_t pos, void *buffer, rt_size_t size)
{
    rt_memcpy(buffer, (void*)pos, size);
    return size;
}


rt_size_t at32_flash_write(rt_device_t dev, rt_off_t pos, const void *buffer, rt_size_t size)
{
    rt_err_t result        = RT_EOK;
    rt_uint32_t end_addr   = pos + size;
    rt_uint32_t *buf   = (rt_uint32_t*)buffer;
    
    //printf("at32_flash_write:0x%X, size:%d\r\n", pos, size);

    flash_unlock();
    rt_base_t level = rt_hw_interrupt_disable();
    while (pos < end_addr)
    {
        if (flash_word_program(pos, *((rt_uint32_t *)buf)) == FLASH_OPERATE_DONE)
        {
            //printf("ADDR:0x%X, VAL:0x%X, 0x%X\r\n", pos, *(rt_uint32_t *)pos, *(rt_uint32_t *)buf);
            if (*(rt_uint32_t *)pos != *(rt_uint32_t *)buf)
            {
                result = -RT_ERROR;
                break;
            }
            pos += 4;
            buf ++;
        }
        else
        {
            result = -RT_ERROR;
            break;
        }
    }
    rt_hw_interrupt_enable(level);
    flash_lock();

    if (result != RT_EOK)
    {
        return result;
    }

    return size;
}

static rt_err_t at32_flash_open(rt_device_t dev, rt_uint16_t oflag)
{
    return RT_EOK;
}

int at32_flash_erase(rt_uint32_t addr, rt_uint32_t size)
{
    rt_err_t result = RT_EOK;
    rt_uint32_t end_addr = addr + size;
    rt_uint32_t page_addr = 0;

    flash_unlock();

    while(addr < end_addr)
    {
        page_addr = get_page(addr);

        //printf("at32_flash_erase: page_addr:0x%X\r\n", page_addr);
        if(flash_sector_erase(page_addr) != FLASH_OPERATE_DONE)
        {
            result = -RT_ERROR;
            goto __exit;
        }

        addr += SECTOR_SIZE;
    }

    flash_lock();

__exit:
    if(result != RT_EOK)
    {
        return result;
    }

    return size;
}

static rt_err_t at32_flash_control(rt_device_t dev, int cmd, void *args)
{
    uint32_t blk = (uint32_t)args;
    uint32_t erase_addr;
    // printf("%s cmd:0x%X block:%d\r\n", __FUNCTION__, cmd, blk);

    if (cmd == RT_DEVICE_CTRL_BLK_GETGEOME)
    {
        struct rt_device_blk_geometry *geo;

        geo = (struct rt_device_blk_geometry *)args;
        if (geo == RT_NULL)
        {
            return -RT_ERROR;
        }

        geo->bytes_per_sector = geo->block_size = SECTOR_SIZE;
        geo->sector_count = 512;
    }

    if (cmd == RT_DEVICE_CTRL_BLK_ERASE)
    {
        erase_addr = blk * SECTOR_SIZE;
        rt_base_t level = rt_hw_interrupt_disable();
        at32_flash_erase(erase_addr, SECTOR_SIZE);
        rt_hw_interrupt_enable(level);
    }
    return RT_EOK;
}



int rt_hw_on_chip_flash_init(void)
{
    struct rt_device *dev;

    dev = rt_malloc(sizeof(struct rt_device));
    dev->type = RT_Device_Class_MTD;
    dev->rx_indicate = RT_NULL;
    dev->tx_complete = RT_NULL;
    dev->init = RT_NULL;
    dev->open = at32_flash_open;
    dev->close = RT_NULL;
    dev->read = at32_flash_read;
    dev->write = at32_flash_write;
    dev->control = at32_flash_control;
    dev->user_data = RT_NULL;

    return rt_device_register(dev, "ocflash", RT_DEVICE_FLAG_RDWR);
}



#if defined(PKG_USING_FAL)
static int fal_flashinit(void);
static int fal_flash_read(long offset, rt_uint8_t *buf, rt_uint32_t size);
static int fal_flash_write(long offset, const rt_uint8_t *buf, rt_uint32_t size);
static int fal_flash_erase(long offset, rt_uint32_t size);

struct fal_flash_dev at32_onchip_flash =
{
    "onchip_flash",
    AT32_FLASH_START_ADRESS,
    AT32_FLASH_SIZE,
    2048,
    {
        NULL,
        fal_flash_read,
        fal_flash_write,
        fal_flash_erase
    }
};

static int fal_flashinit(void)
{
    at32_onchip_flash.blk_size = flash_get_sector_size();
    return RT_EOK;
}

static int fal_flash_read(long offset, rt_uint8_t *buf, rt_uint32_t size)
{
    return at32_flash_read(at32_onchip_flash.addr + offset, buf, size);
}

static int fal_flash_write(long offset, const rt_uint8_t *buf, rt_uint32_t size)
{
    return at32_flash_write(at32_onchip_flash.addr + offset, buf, size);
}

static int fal_flash_erase(long offset, rt_uint32_t size)
{
    return at32_flash_erase(at32_onchip_flash.addr + offset, size);
}

#endif
#endif /* BSP_USING_ON_CHIP_FLASH */
