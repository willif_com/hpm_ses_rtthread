#include <rtthread.h>
#include "ch0x0_board.h"
#include <stdio.h>
#include <rtdevice.h>


#define BSP_USING_UART1
#define BSP_USING_UART2
//#define BSP_USING_UART3

enum
{
#ifdef BSP_USING_UART1
    UART1_INDEX,
#endif
#ifdef BSP_USING_UART2
    UART2_INDEX,
#endif
#ifdef BSP_USING_UART3
    UART3_INDEX,
#endif
};

typedef struct
{
    struct rt_serial_device     serial;
    usart_type                  *USARTx;
    IRQn_Type                   irq_uart;
    IRQn_Type                   irq_dma_tx;
    IRQn_Type                   irq_dma_rx;
    uint32_t                    setting_recv_len;      /* setting receive len */
    uint32_t                    last_recv_index;       /* last receive index */
    dma_channel_type            *tx_dma_ch;
    dma_channel_type            *rx_dma_ch;
    uint32_t                    rx_buf_addr;
    uint32_t                    baud;
    const char *name;
} at32_uart_t;



at32_uart_t uart_obj[] =
{
#ifdef BSP_USING_UART1
        {
            .USARTx = USART1,
            .irq_uart = USART1_IRQn,
            .irq_dma_tx = DMA1_Channel4_IRQn,
            .irq_dma_rx = DMA1_Channel5_IRQn,
            .setting_recv_len = 0,
            .last_recv_index = 0,
            .tx_dma_ch = DMA1_CHANNEL4,
            .rx_dma_ch = DMA1_CHANNEL5,
            .name = "uart1",
            .baud = 115200,
        },
#endif
#ifdef BSP_USING_UART2
        {
            .USARTx = USART2,
            .irq_uart = USART2_IRQn,
            .irq_dma_tx = DMA1_Channel7_IRQn,
            .irq_dma_rx = DMA1_Channel6_IRQn,
            .setting_recv_len = 0,
            .last_recv_index = 0,
            .tx_dma_ch = DMA1_CHANNEL7,
            .rx_dma_ch = DMA1_CHANNEL6,
            .name = "uart2",
            .baud = 115200,
        },
#endif
#ifdef BSP_USING_UART3
        {
            .USARTx = USART3,
            .irq_uart = USART3_IRQn,
            .irq_dma_tx = DMA1_Channel2_IRQn,
            .irq_dma_rx = DMA1_Channel3_IRQn,
            .setting_recv_len = 0,
            .last_recv_index = 0,
            .tx_dma_ch = DMA1_CHANNEL2,
            .rx_dma_ch = DMA1_CHANNEL3,
            .name = "uart2",
            .baud = 115200,
        },
#endif
};


static void uart_setup_tx_dma(at32_uart_t *uart, uint8_t *buf, uint32_t len)
{
    dma_init_type dma_init_struct;

    dma_default_para_init(&dma_init_struct);
    dma_init_struct.buffer_size = len;
    dma_init_struct.direction = DMA_DIR_MEMORY_TO_PERIPHERAL;
    dma_init_struct.memory_base_addr = (uint32_t)buf;
    dma_init_struct.memory_data_width = DMA_MEMORY_DATA_WIDTH_BYTE;
    dma_init_struct.memory_inc_enable = TRUE;
    dma_init_struct.peripheral_base_addr = (uint32_t)&uart->USARTx->dt;
    dma_init_struct.peripheral_data_width = DMA_PERIPHERAL_DATA_WIDTH_BYTE;
    dma_init_struct.peripheral_inc_enable = FALSE;
    dma_init_struct.priority = DMA_PRIORITY_MEDIUM;
    dma_init_struct.loop_mode_enable = FALSE;
    dma_init(uart->tx_dma_ch, &dma_init_struct);
}


static rt_err_t at32_configure(struct rt_serial_device *serial, struct serial_configure *cfg)
{
    at32_uart_t *uart = rt_container_of(serial, at32_uart_t, serial);

    RT_ASSERT(serial != RT_NULL);
    RT_ASSERT(cfg != RT_NULL);

    usart_init(uart->USARTx, cfg->baud_rate, USART_DATA_8BITS, USART_STOP_1_BIT);
    usart_transmitter_enable(uart->USARTx, TRUE);
    usart_receiver_enable(uart->USARTx, TRUE);
    usart_enable(uart->USARTx, TRUE);
    return RT_EOK;
}

static rt_err_t at32_control(struct rt_serial_device *serial, int cmd, void *arg)
{
    at32_uart_t *uart = rt_container_of(serial, at32_uart_t, serial);
    uint32_t arg_val;
    RT_ASSERT(serial != RT_NULL);

    switch (cmd)
    {
        case RT_DEVICE_CTRL_CLR_INT:
            nvic_irq_disable(uart->irq_uart);
            usart_interrupt_enable(uart->USARTx, USART_RDBF_INT, FALSE);
            break;
        case RT_DEVICE_CTRL_SET_INT:
            nvic_irq_enable(uart->irq_uart, 2, 1);
            usart_interrupt_enable(uart->USARTx, USART_RDBF_INT, TRUE);
            break;
        case RT_DEVICE_CTRL_CONFIG:
            arg_val = (uint32_t)arg;
            
            if(arg_val == RT_DEVICE_FLAG_DMA_RX)
            {
                struct rt_serial_rx_fifo* rx_fifo = (struct rt_serial_rx_fifo*)serial->serial_rx;
                uart->rx_buf_addr = (uint32_t)rx_fifo->buffer;
                
                dma_init_type dma_init_struct;

                dma_default_para_init(&dma_init_struct);
                dma_init_struct.buffer_size = serial->config.bufsz;
                dma_init_struct.direction = DMA_DIR_PERIPHERAL_TO_MEMORY;
                dma_init_struct.memory_base_addr = (uint32_t)uart->rx_buf_addr;
                dma_init_struct.memory_data_width = DMA_MEMORY_DATA_WIDTH_BYTE;
                dma_init_struct.memory_inc_enable = TRUE;
                dma_init_struct.peripheral_base_addr = (uint32_t)&uart->USARTx->dt;
                dma_init_struct.peripheral_data_width = DMA_PERIPHERAL_DATA_WIDTH_BYTE;
                dma_init_struct.peripheral_inc_enable = FALSE;
                dma_init_struct.priority = DMA_PRIORITY_MEDIUM;
                dma_init_struct.loop_mode_enable = FALSE;
                dma_init(uart->rx_dma_ch, &dma_init_struct);
                
                uart->setting_recv_len = serial->config.bufsz;  
                
                usart_dma_receiver_enable(uart->USARTx, TRUE);
                dma_interrupt_enable(uart->rx_dma_ch, DMA_FDT_INT, TRUE);
                dma_channel_enable(uart->rx_dma_ch, TRUE);
                nvic_irq_enable(uart->irq_dma_rx, 0, 0);
                
                usart_interrupt_enable(uart->USARTx, USART_IDLE_INT, TRUE);
                nvic_irq_enable(uart->irq_uart, 2, 1);
            }
            
            if(arg_val == RT_DEVICE_FLAG_DMA_TX)
            {
                usart_dma_transmitter_enable(uart->USARTx, TRUE);
                uart_setup_tx_dma(uart, RT_NULL, 0);
                dma_interrupt_enable(uart->tx_dma_ch, DMA_FDT_INT, TRUE);
                nvic_irq_enable(uart->irq_dma_tx, 0, 0);
            }

            break;
        case 0xFFFD:
            dma_channel_enable(uart->tx_dma_ch, FALSE);
            break;
        case 0xFFFE:
            dma_channel_enable(uart->tx_dma_ch, TRUE);
            break;
        default:
            break;
  }

  return RT_EOK;
}

static int at32_putc(struct rt_serial_device *serial, char ch)
{
    RT_ASSERT(serial != RT_NULL);
    
    at32_uart_t *uart = rt_container_of(serial, at32_uart_t, serial);

    while (usart_flag_get(uart->USARTx, USART_TDBE_FLAG) == RESET);
    usart_data_transmit(uart->USARTx, (uint8_t)ch);

    return 1;
}

static int at32_getc(struct rt_serial_device *serial)
{
    int ch;
    RT_ASSERT(serial != RT_NULL);
    at32_uart_t *uart = rt_container_of(serial, at32_uart_t, serial);

    ch = -1;
    if (usart_flag_get(uart->USARTx, USART_RDBF_FLAG) != RESET) {
        ch = usart_data_receive(uart->USARTx) & 0xff;
    }

    return ch;
}

static rt_size_t at32_dma_transmit(struct rt_serial_device *serial, rt_uint8_t *buf, rt_size_t size, int direction)
{
    at32_uart_t *uart = rt_container_of(serial, at32_uart_t, serial);
    
    if(direction == RT_SERIAL_DMA_TX)
    {
        dma_channel_enable(uart->tx_dma_ch, FALSE);
        uart->tx_dma_ch->maddr = (uint32_t)buf;
        dma_data_number_set(uart->tx_dma_ch, size);
        dma_channel_enable(uart->tx_dma_ch, TRUE);
    }
    
    if(direction == RT_SERIAL_DMA_RX)
    {
        
    }

    return size;
}

static const struct rt_uart_ops at32_uart_ops =
{
    at32_configure,
    at32_control,
    at32_putc,
    at32_getc,
    at32_dma_transmit,
};


int rt_hw_uart_init(void)
{
    struct serial_configure config = RT_SERIAL_CONFIG_DEFAULT;

    for (int i = 0; i < ARRAY_SIZE(uart_obj); i++)
    {
        config.baud_rate = uart_obj[i].baud;
        uart_obj[i].serial.ops = &at32_uart_ops;
        uart_obj[i].serial.config = config;
        rt_hw_serial_register(&uart_obj[i].serial, uart_obj[i].name, RT_DEVICE_FLAG_RDWR | RT_DEVICE_FLAG_DMA_TX | RT_DEVICE_FLAG_DMA_RX | RT_DEVICE_FLAG_INT_RX, RT_NULL);
    }
    return RT_EOK;
}


static void uart_irq(struct rt_serial_device *serial)
{
    RT_ASSERT(serial != RT_NULL);
    at32_uart_t *uart = rt_container_of(serial, at32_uart_t, serial);
    
    if (usart_flag_get(uart->USARTx, USART_RDBF_FLAG) != RESET) {
        usart_flag_clear(uart->USARTx, USART_RDBF_FLAG);
        rt_hw_serial_isr(serial, RT_SERIAL_EVENT_RX_IND);
    }
    
    if (usart_flag_get(uart->USARTx, USART_IDLEF_FLAG) != RESET) {
        usart_flag_clear(uart->USARTx, USART_IDLEF_FLAG);
        uint8_t dummy = uart->USARTx->dt;
        uint32_t recv_total_index, recv_len; 

        recv_total_index = uart->setting_recv_len - dma_data_number_get(uart->rx_dma_ch);
        recv_len = recv_total_index - uart->last_recv_index;
        uart->last_recv_index = recv_total_index;
        
        rt_hw_serial_isr(serial, RT_SERIAL_EVENT_RX_DMADONE | (recv_len << 8));
    }

    
    if (usart_flag_get(uart->USARTx, USART_ROERR_FLAG) != RESET) {
        usart_flag_clear(uart->USARTx, USART_ROERR_FLAG);
    }
    else
    {
        if (usart_flag_get(uart->USARTx, USART_CTSCF_FLAG) != RESET) {
            usart_flag_clear(uart->USARTx, USART_CTSCF_FLAG);
        }

        if (usart_flag_get(uart->USARTx, USART_BFF_FLAG) != RESET) {
            usart_flag_clear(uart->USARTx, USART_BFF_FLAG);
        }

        if (usart_flag_get(uart->USARTx, USART_TDC_FLAG) != RESET) {
            usart_flag_clear(uart->USARTx, USART_TDC_FLAG);
        }
    }
}


static void uart_rx_dma_irq(at32_uart_t *uart)
{
    rt_size_t recv_len;

    recv_len = uart->setting_recv_len - uart->last_recv_index;
    
    /* reset last recv index */
    uart->last_recv_index = 0;

    dma_channel_enable(uart->rx_dma_ch, FALSE);
    uart->rx_dma_ch->maddr = (uint32_t)uart->rx_buf_addr;
    dma_data_number_set(uart->rx_dma_ch, uart->setting_recv_len);
    dma_channel_enable(uart->rx_dma_ch, TRUE);
    
    rt_hw_serial_isr(&uart->serial, RT_SERIAL_EVENT_RX_DMADONE | (recv_len << 8));
}




void USART1_IRQHandler(void)  {uart_irq(&uart_obj[UART1_INDEX].serial);}
void USART2_IRQHandler(void)  {uart_irq(&uart_obj[UART2_INDEX].serial);}


void DMA1_Channel4_IRQHandler(void) 
{
    dma_flag_clear(DMA1_FDT4_FLAG); 
    rt_hw_serial_isr(&uart_obj[UART1_INDEX].serial, RT_SERIAL_EVENT_TX_DMADONE);
}

void DMA1_Channel5_IRQHandler(void) 
{
    dma_flag_clear(DMA1_FDT5_FLAG); 
    uart_rx_dma_irq(&uart_obj[UART1_INDEX]);
}

void DMA1_Channel7_IRQHandler(void)
{
    dma_flag_clear(DMA1_FDT7_FLAG); 
    rt_hw_serial_isr(&uart_obj[UART2_INDEX].serial, RT_SERIAL_EVENT_TX_DMADONE);
}

void DMA1_Channel6_IRQHandler(void)
{
    dma_flag_clear(DMA1_FDT6_FLAG); 
    uart_rx_dma_irq(&uart_obj[UART2_INDEX]);
}



