
#include <rtthread.h>
#include <stdio.h>
#include <rthw.h>
#include <rtdevice.h>
#include "ch0x0_board.h"

#define BSP_USING_SPI1

enum
{
#ifdef BSP_USING_SPI1
    SPI1_INDEX,
#endif
};

typedef struct
{
    struct rt_spi_bus       bus;
    spi_type                *SPIx;
    dma_channel_type        *tx_dma_ch;     /* spi tx dma channel */
    dma_channel_type        *rx_dma_ch;     /* spi rx dma channel */
    const char              *name;
    IRQn_Type               irq_dma;
    rt_sem_t                sem;
}at32_spi_bus_t;


typedef struct
{
    struct rt_spi_device    spi_dev;
    uint8_t                 cs_pin;             /* cs pin */
    uint8_t                 cs_delay_us;
}at32_spi_dev_t;


static at32_spi_bus_t spi_bus_obj[] =
{
#ifdef BSP_USING_SPI1
        {
            .SPIx = SPI1,
            .tx_dma_ch = DMA1_CHANNEL3,
            .rx_dma_ch = DMA1_CHANNEL2,
            .name = "spi1",
            .irq_dma = DMA1_Channel2_IRQn,
        },
#endif
};


static void _spi_init(spi_type* spi_x, uint32_t baud)
{
    spi_init_type spi_init_struct;
    spi_default_para_init(&spi_init_struct);
    
    uint32_t spi_apb_clock;
    uint32_t max_hz= baud;
    crm_clocks_freq_type clocks_struct;

    crm_clocks_freq_get(&clocks_struct);
    spi_apb_clock = clocks_struct.apb2_freq;

    if(max_hz >= (spi_apb_clock / 2))
    {
        spi_init_struct.mclk_freq_division = SPI_MCLK_DIV_2;
    }
    else if (max_hz >= (spi_apb_clock / 4))
    {
        spi_init_struct.mclk_freq_division = SPI_MCLK_DIV_4;
    }
    else if (max_hz >= (spi_apb_clock / 8))
    {
        spi_init_struct.mclk_freq_division = SPI_MCLK_DIV_8;
    }
    else if (max_hz >= (spi_apb_clock / 16))
    {
        spi_init_struct.mclk_freq_division = SPI_MCLK_DIV_16;
    }
    else if (max_hz >= (spi_apb_clock / 32))
    {
        spi_init_struct.mclk_freq_division = SPI_MCLK_DIV_32;
    }
    else if (max_hz >= (spi_apb_clock / 64))
    {
        spi_init_struct.mclk_freq_division = SPI_MCLK_DIV_64;
    }
    else if (max_hz >= (spi_apb_clock / 128))
    {
        spi_init_struct.mclk_freq_division = SPI_MCLK_DIV_128;
    }
    else
    {
        /* min prescaler 256 */
        spi_init_struct.mclk_freq_division = SPI_MCLK_DIV_256;
    }
    
    spi_init_struct.master_slave_mode = SPI_MODE_MASTER;
    spi_init(spi_x, &spi_init_struct);
    spi_enable(spi_x, TRUE);
    spi_i2s_dma_transmitter_enable(spi_x, TRUE);
    spi_i2s_dma_receiver_enable(spi_x, TRUE);
}

static rt_err_t configure(struct rt_spi_device* device, struct rt_spi_configuration* configuration)
{
    at32_spi_dev_t *at_dev = rt_container_of(device, at32_spi_dev_t, spi_dev);
    at_dev->cs_delay_us = configuration->reserved;
//    at32_spi_bus_t *at_bus = rt_container_of(device->bus, at32_spi_bus_t, bus);
    return RT_EOK;
}




//static uint8_t spi_xfer_byte(spi_type *SPIx, uint8_t data)
//{
//    while(spi_i2s_flag_get(SPIx, SPI_I2S_TDBE_FLAG) == RESET);
//    spi_i2s_data_transmit(SPIx, data);
//    while(spi_i2s_flag_get(SPIx, SPI_I2S_RDBF_FLAG) == RESET);
//    return spi_i2s_data_receive(SPIx);
//}

static rt_uint32_t xfer(struct rt_spi_device* device, struct rt_spi_message* message)
{
    rt_uint32_t size = message->length;
    rt_uint8_t *send_ptr = (rt_uint8_t*)message->send_buf;
    rt_uint8_t *recv_ptr = message->recv_buf;
    at32_spi_dev_t *spi_dev = rt_container_of(device, at32_spi_dev_t, spi_dev);
    at32_spi_bus_t *spi_bus = rt_container_of(device->bus, at32_spi_bus_t, bus);
    
    static uint8_t dummy_buf[1];
    

    rt_pin_write(spi_dev->cs_pin, 0);
    
    if(spi_dev->cs_delay_us)  rt_hw_us_delay(spi_dev->cs_delay_us);     /* insert cs delay */
    
    dma_channel_enable(spi_bus->rx_dma_ch, FALSE);
    dma_channel_enable(spi_bus->tx_dma_ch, FALSE);
    spi_bus->tx_dma_ch->ctrl_bit.mincm = 1;
    spi_bus->rx_dma_ch->ctrl_bit.mincm = 1;
    
    if(send_ptr == RT_NULL)
    {
        spi_bus->tx_dma_ch->maddr = (uint32_t)dummy_buf;
        spi_bus->rx_dma_ch->maddr = (uint32_t)recv_ptr;
        spi_bus->tx_dma_ch->ctrl_bit.mincm = 0;
    }
    else if(recv_ptr == RT_NULL)
    {
        spi_bus->tx_dma_ch->maddr = (uint32_t)send_ptr;
        spi_bus->rx_dma_ch->maddr = (uint32_t)dummy_buf;
        spi_bus->rx_dma_ch->ctrl_bit.mincm = 0;
    }
    else
    {
        spi_bus->tx_dma_ch->maddr = (uint32_t)send_ptr;
        spi_bus->rx_dma_ch->maddr = (uint32_t)recv_ptr;
    }
    
    dma_data_number_set(spi_bus->tx_dma_ch, size);
    dma_data_number_set(spi_bus->rx_dma_ch, size);
    dma_channel_enable(spi_bus->rx_dma_ch, TRUE);
    dma_channel_enable(spi_bus->tx_dma_ch, TRUE);
    
   // dma_interrupt_enable(spi_bus->rx_dma_ch, DMA_FDT_INT, TRUE);
  //  rt_sem_take(spi_bus->sem, RT_WAITING_FOREVER);
    while(dma_data_number_get(spi_bus->rx_dma_ch) != 0) {};

//    while(size--)
//    {
//        rt_uint16_t data = 0xFF;
//        if(send_ptr != RT_NULL)
//        {
//            data = *send_ptr++;
//        }

//        if((size == 0) && (message->cs_release))
//        {  
//            data = spi_xfer_byte(at32_spib->SPIx, data);
//            rt_pin_write(at32_spid->cs_pin, 1);
//        }
//        else
//        {
//            data = spi_xfer_byte(at32_spib->SPIx, data);
//        }
//        if(recv_ptr != RT_NULL)
//        {
//            *recv_ptr++ = data;
//        }
//    }

    if(message->cs_release)
    {
        if(spi_dev->cs_delay_us)  rt_hw_us_delay(spi_dev->cs_delay_us/2);
        rt_pin_write(spi_dev->cs_pin, 1);
    }
    return message->length;
}


static const struct rt_spi_ops ops =
{
    configure,
    xfer
};


int rt_hw_spi_init(void)
{
    for (int i = 0; i < ARRAY_SIZE(spi_bus_obj); i++)
    {
        spi_bus_obj[i].bus.ops = &ops;
        spi_bus_obj[i].sem = rt_sem_create("sem", 0, RT_IPC_FLAG_FIFO);

        _spi_init(spi_bus_obj[i].SPIx, 8*1000*1000);
        
        /* DMA */
        dma_init_type dma_init_struct;
        dma_default_para_init(&dma_init_struct);
        
        dma_init_struct.direction = DMA_DIR_MEMORY_TO_PERIPHERAL;
        dma_init_struct.memory_inc_enable = TRUE;
        dma_init_struct.peripheral_base_addr = (uint32_t)&spi_bus_obj[i].SPIx->dt;
        dma_init_struct.peripheral_inc_enable = FALSE;
        dma_init(spi_bus_obj[i].tx_dma_ch, &dma_init_struct);
        
        dma_init_struct.direction = DMA_DIR_PERIPHERAL_TO_MEMORY;
        dma_init_struct.memory_inc_enable = TRUE;
        dma_init_struct.peripheral_base_addr = (uint32_t)&spi_bus_obj[i].SPIx->dt;
        dma_init_struct.peripheral_inc_enable = FALSE;
        dma_init(spi_bus_obj[i].rx_dma_ch, &dma_init_struct);
        
        nvic_irq_enable(spi_bus_obj[i].irq_dma, 1, 0);
        
        return rt_spi_bus_register(&spi_bus_obj[i].bus, spi_bus_obj[i].name, &ops); 
    }
    return RT_EOK;
}


int rt_hw_spi_device_init(const char *name, const char *bus, rt_uint32_t cs_pin)
{
    at32_spi_dev_t *at_dev = rt_malloc(sizeof(at32_spi_dev_t));
    at32_spi_bus_t *at_bus = (at32_spi_bus_t*)rt_device_find(bus);
    
    at_dev->cs_pin = cs_pin;
    at_dev->cs_delay_us = 0;
    
    /* set spi cs pin to output mode */
    rt_pin_mode(at_dev->cs_pin, PIN_MODE_OUTPUT);
    rt_pin_write(at_dev->cs_pin, 1);
    
    return rt_spi_bus_attach_device(&at_dev->spi_dev, name, bus, at_dev);
}

//void DMA1_Channel2_IRQHandler(void) 
//{
//    dma_flag_clear(DMA1_FDT2_FLAG); 
//    rt_sem_release(spi_bus_obj[SPI1_INDEX].sem);
//}




