/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-5      SummerGift   first version
 * 2018-12-11     greedyhao    Porting for stm32f7xx
 * 2019-01-03     zylx         modify DMA initialization and spixfer function
 * 2020-01-15     whj4674672   Porting for stm32h7xx
 * 2020-06-18     thread-liu   Porting for stm32mp1xx
 * 2020-10-14     Dozingfiretruck   Porting for stm32wbxx
 */

#include <rtthread.h>
#include <rtdevice.h>
#include "board.h"
#include <stdio.h>
#include "stm32l4xx_spi.h"

#include "drv_spi.h"
#include "drv_config.h"
#include <string.h>




typedef struct
{
    uint8_t cs_pin;         /* cs pin */
    uint8_t instance;       /* spi instance */
    uint8_t dma_tx_chl;     /* spi tx dma channel */
    uint8_t dma_rx_chl;     /* spi rx dma channel */
    uint8_t dma_instance;   /* dma instance */
}spi_usr_t;


void bmi_spi_init(void)
{
//	GPIO_InitTypeDef GPIO_InitStruct;

//    __HAL_RCC_GPIOA_CLK_ENABLE();
//    __HAL_RCC_GPIOB_CLK_ENABLE();
//    
//    GPIO_InitStruct.Pin = GPIO_PIN_8 | GPIO_PIN_11 | GPIO_PIN_12 | GPIO_PIN_15;
//    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
//    GPIO_InitStruct.Pull = GPIO_PULLUP;
//    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
//    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
//    HAL_GPIO_WritePin(GPIOA,GPIO_PIN_8, GPIO_PIN_SET);
//    HAL_GPIO_WritePin(GPIOA,GPIO_PIN_11, GPIO_PIN_SET);
//    HAL_GPIO_WritePin(GPIOA,GPIO_PIN_12, GPIO_PIN_SET);
//    HAL_GPIO_WritePin(GPIOA,GPIO_PIN_15, GPIO_PIN_SET);
//    
//    GPIO_InitStruct.Pin = GPIO_PIN_5 | GPIO_PIN_12;
//    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
//    GPIO_InitStruct.Pull = GPIO_PULLUP;
//    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
//    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
//    HAL_GPIO_WritePin(GPIOB,GPIO_PIN_5, GPIO_PIN_SET);
//    HAL_GPIO_WritePin(GPIOB,GPIO_PIN_12, GPIO_PIN_SET);

	SPI2_Init();

}

static rt_uint32_t spixfer(struct rt_spi_device *device, struct rt_spi_message *message)
{
    rt_uint32_t size = message->length;
    rt_uint8_t *send_ptr = (rt_uint8_t*)message->send_buf;
    rt_uint8_t *recv_ptr = message->recv_buf;
    spi_usr_t *usr = device->parent.user_data;

    static uint8_t dummy_buf[1];
    
    rt_pin_write(usr->cs_pin, 0);

    while(size--)
    {
        rt_uint16_t data = 0xFF;
        if(send_ptr != RT_NULL)
        {
            data = *send_ptr++;
        }

        if((size == 0) && (message->cs_release))
        {  
            data = SPI_ReadWrite(usr->instance, data);
            rt_pin_write(usr->cs_pin, 1);
        }
        else
        {
            data = SPI_ReadWrite(usr->instance, data);
        }
        if(recv_ptr != RT_NULL)
        {
            *recv_ptr++ = data;
        }
    }

    return message->length;
}

static rt_err_t spi_configure(struct rt_spi_device *device,
                              struct rt_spi_configuration *configuration)
{
    RT_ASSERT(device != RT_NULL);
    RT_ASSERT(configuration != RT_NULL);

    return RT_EOK;
}

static const struct rt_spi_ops stm_spi_ops =
{
    .configure = spi_configure,
    .xfer = spixfer,
};

static int rt_hw_spi_bus_init(const char *name)
{
    rt_err_t result;
    struct rt_spi_bus *spi_bus = rt_malloc(sizeof(struct rt_spi_bus));
    return rt_spi_bus_register(spi_bus, name, &stm_spi_ops); 

}

int rt_hw_spi_init(const char *name)
{
    return rt_hw_spi_bus_init(name);
}

int rt_hw_spi_device_init(const char *name, const char *bus, rt_uint32_t cs_pin)
{
    uint32_t instance;
    sscanf(bus, "spi%d", &instance);
    
    struct rt_spi_device *dev = rt_malloc(sizeof(struct rt_spi_device));
    spi_usr_t *usr = rt_malloc(sizeof(spi_usr_t));
    
    usr->instance = instance;
    usr->cs_pin = cs_pin;
    
    bmi_spi_init();
    
    /* set spi cs pin to output mode */
    rt_pin_mode(cs_pin, PIN_MODE_OUTPUT);
    rt_pin_write(cs_pin, 1);
    
    return rt_spi_bus_attach_device(dev, name, bus, usr);
}




