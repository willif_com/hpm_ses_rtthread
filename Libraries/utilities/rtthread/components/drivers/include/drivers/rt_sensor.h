#ifndef __CH_RT_SENSOR_H__
#define __CH_RT_SENSOR_H__

#include <stdint.h>

    

#define RT_SENSOR_POS_ACC                   (0)
#define RT_SENSOR_POS_GYR                   (1)
#define RT_SENSOR_POS_MAG                   (2)
#define RT_SENSOR_POS_GYR_TEMPERATURE       (3)
#define RT_SENSOR_ENTER_LOW_POWER           (4)
#define RT_SENSOR_POS_ACC_FIFO              (9)
#define RT_SENSOR_POS_GYR_FIFO              (10)
#define RT_SENSOR_POS_MAG_FIFO              (11)
#define RT_SENSOR_POS_ACC_BW                (20)
#define RT_SENSOR_POS_ACC_RG                (21)
#define RT_SENSOR_POS_GYR_BW                (22)
#define RT_SENSOR_POS_GYR_RG                (23)
#define RT_SENSOR_POS_MAG_BW                (24)
#define RT_SENSOR_POS_MAG_RG                (25)
#define RT_SENSOR_POS_ACC_CLEAR_FIFO        (27)
#define RT_SENSOR_POS_ACC_WM_FIFO_INT       (29)
#define RT_SENSOR_POS_GYR_WM_FIFO_INT       (30)
#define RT_SENSOR_POS_FOR                   (60)
#define RT_SENSOR_POS_ACC_RST               (61)
#define RT_SENSOR_POS_GYR_RST               (62)
#define RT_SENSOR_POS_GYR_ENABLE_FIFO       (63)
#define RT_SENSOR_POS_GYR_DISABLE_FIFO      (64)
#define RT_SENSOR_POS_CUSTOM                (255)

#define RT_ROT_Z_0                          (0)
#define RT_ROT_Z_P90                        (1)
#define RT_ROT_Z_P180                       (2)
#define RT_ROT_Z_N90                        (3)
#define RT_ROT_X_180                        (4)
#define RT_ROT_Y_180                        (5)
#define RT_ROT_Y_180_ZN90                   (6)
#define RT_ROT_Y_180_ZP90                   (7)

#endif


