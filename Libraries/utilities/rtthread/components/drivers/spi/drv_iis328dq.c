#include <drv_iis328dq.h>
#include "iis328dq_data.h"
#define BORT_INIT 0
#define TEST_FIR 0

iis328_config sensor_cfg;

rt_err_t rt_iis328_init(rt_device_t dev)
{
    return RT_EOK;
}

rt_err_t rt_iis328_open(rt_device_t dev, rt_uint16_t oflag)
{
    //读取设备的id号
     /* SPI1 MOSI:PA7     MISO:PA6    CLK:PA5    CS1:PB0   CS2:PA0 */
    uint8_t dev_id = 0;
    uint8_t ret = 0;
    uint8_t sendbuf = 0;
    uint8_t recvbuf = 0;
    
    /* 0x0F：获取id号 */
    rt_iis328_read(dev, IIS328_WHO_AM_I, &dev_id, 1);
 
    if(dev_id == IIS328_WHO_AM_I_ID)
    {
        /* 读 改 写 */
        /* 0x23：配置为块更新，8g量程 */
        rt_iis328_read(dev, IIS328_CTRL_REG4, &recvbuf, sizeof(recvbuf));
        
        sendbuf = recvbuf | BLOCK_DATA_UPDATA | FULL_SCALE_8G;
        rt_iis328_write(dev, IIS328_CTRL_REG4, &sendbuf, sizeof(sendbuf));
        
        /* 0x21：关闭高通滤波器 通过内部低通滤波器输出数据*/
        rt_iis328_read(dev, IIS328_CTRL_REG2, &recvbuf, sizeof(recvbuf));

        recvbuf = 0;
        if(param.filter_swithc == 0x07)
        {
            sendbuf = recvbuf | (1 << 4);
            rt_iis328_write(dev, IIS328_CTRL_REG2, &sendbuf, sizeof(sendbuf));
            /* 读取0x25 这个虚拟的寄存器 是内部的高通滤波器内容清零 */
            rt_iis328_read(dev, HP_FILTER_RESET, &recvbuf, sizeof(recvbuf));
        }

        /* 0x20：配置频率 */
        rt_iis328_read(dev,  IIS328_CTRL_REG1, &recvbuf, sizeof(recvbuf));
        recvbuf &= ~0xF8;

        sendbuf = recvbuf | param.cfg.power_mode | param.cfg.odr;
        rt_iis328_write(dev, IIS328_CTRL_REG1, &sendbuf, sizeof(sendbuf));
    }
    return RT_EOK;
}

rt_err_t rt_iis328_close (rt_device_t dev)
{
    //关闭设备
    return RT_EOK;
}

rt_size_t rt_iis328_read(rt_device_t dev, rt_off_t pos, void *buffer, rt_size_t size)
{
    uint8_t ret = 0;
    uint8_t sendbuf[7] = {0};
    uint8_t read_iis328 = 0;
    rt_iis328_t *iis328_dev = (rt_iis328_t *)dev;

    switch(pos)
    {
        case IIS328_WHO_AM_I:                             /* 0x0F：获取id号 */

        case IIS328_CTRL_REG1:                            /* 0x20：读取配置 */
 
        case IIS328_CTRL_REG2:                            /* 0x21：读取配置 */
    
        case IIS328_CTRL_REG4:                            /* 0x23：读取配置 */
            
        case IIS328_STATUS_REG:                           /* 0x27：读取标志位 */
            read_iis328 = IIS328_SPI_READ | pos;
            ret = rt_spi_send_then_recv(iis328_dev->acc_spid, &read_iis328, 1, buffer, size);
            break;
        case IIS328_ACC_DATA:                             /*0x28 - 0x2D： 读取XYZ三轴的加速度数据 */
            sendbuf[0] = IIS328_SPI_READ | IIS328_ACC_DATA;
            ret = rt_spi_transfer(iis328_dev->acc_spid, sendbuf, buffer, 7);
            break;
        delault:
            break;
    }
    return RT_EOK;
}

rt_size_t rt_iis328_write(rt_device_t dev, rt_off_t pos, const void *buffer, rt_size_t size)
{
    uint8_t ret = 0;
    uint8_t write_iis328 = 0;
    rt_iis328_t *iis328_dev = (rt_iis328_t *)dev;
    
    switch(pos)
    {
        case IIS328_CTRL_REG1:
 
        case IIS328_CTRL_REG2:

        case IIS328_CTRL_REG4:
            write_iis328 = IIS328_SPI_WRITE | pos;
            ret = rt_spi_send_then_send(iis328_dev->acc_spid, &write_iis328, 1, buffer, size);
            break;
        delault:
            break;
    }
    return RT_EOK;
}

rt_err_t  rt_iis328_control(rt_device_t dev, int cmd, void *args)
{
    /* 低功耗模式 */
    /* normal模式 */
    /* 50Hz       */
    /* 100Hz      */
    /* 400Hz      */
    /* 1000Hz     */
    return RT_EOK;
}

int rt_hw_iis328_init(const char *name, const char *acc_spid_name, uint32_t irq_acc)
{
    rt_iis328_t *iis328_dev = rt_malloc(sizeof(rt_iis328_t));
    if(!iis328_dev)
        return RT_ERROR;
    
    iis328_dev->acc_spid = (struct rt_spi_device *)rt_device_find(acc_spid_name);
    if(!iis328_dev->acc_spid)
        return RT_ERROR;
    
    iis328_dev->parent.type = RT_Device_Class_Miscellaneous;
    iis328_dev->parent.rx_indicate = RT_NULL;
    iis328_dev->parent.tx_complete = RT_NULL;
    iis328_dev->parent.init = rt_iis328_init;
    iis328_dev->parent.open = rt_iis328_open;
    iis328_dev->parent.close = rt_iis328_close;
    iis328_dev->parent.read = rt_iis328_read;
    iis328_dev->parent.write = rt_iis328_write;
    iis328_dev->parent.control = rt_iis328_control;
    iis328_dev->parent.user_data = &iis328_dev;
    
    iis328_dev->irq_acc = irq_acc;
    iis328_dev->acc_fifo_read_size = 5;
    iis328_dev->for_setting = 0;

    return rt_device_register(&iis328_dev->parent, name, RT_DEVICE_FLAG_RDWR);
}
