#include <rtthread.h>
#include <stdio.h>
#include <math.h>
#include <rtdevice.h>
#include "drivers/rt_sensor.h"


#define BSPL06_DEBUG		0
#if ( BSPL06_DEBUG == 1 )
#include <stdio.h>
#define BSPL06_TRACE	printf
#else
#define BSPL06_TRACE(...)
#endif



#define SPL_PRODUCT_ID_ADDR         0x0D
#define SPL_PRS_CFG_ADDR            0x06
#define SPL_TMP_CFG_ADDR            0x07
#define SPL_CFG_REG_ADDR            0x09
#define SPL_MEAS_CFG_ADDR           0x08
#define SPL_RESET_ADDR              0x0C
#define SPL_TMP_B2_ADDR             0x03
#define SPL_PRS_B2_ADDR             0x00
#define SPL_COEF_START_ADDR         (0x10)

typedef struct
{	
    int16_t c0, c1;
    int32_t c00, c10;
    int16_t c01, c11, c20, c21, c30;
}coe_t;


typedef struct 
{	
    struct rt_device            parent;
    struct rt_spi_device         *spid;
    coe_t                       coe;
    int32_t                     kp;    
    int32_t                     kt;
    int32_t                     press_raw;
    int32_t                     temp_raw;
    float                       temperature;
    float                       pressure;
    float                       altitude;
}spl06_t;


static void spl06_write_register(rt_device_t dev, uint8_t addr, uint8_t val)
{
    spl06_t *spl06 = (spl06_t*)dev;

    uint8_t send_buf[2];
    
    send_buf[0] = (0<<7) | addr;
    send_buf[1] = val;
    
    rt_spi_send(spl06->spid, send_buf, 2);
}

static void spl06_read(rt_device_t dev, uint8_t addr, uint8_t *rx_buf, uint32_t len)
{
    spl06_t *spl06 = (spl06_t*)dev;
    uint8_t send_buf[1];
    
    send_buf[0] = (1<<7) | addr;
    rt_spi_send_then_recv(spl06->spid, send_buf, 1, rx_buf, len);
}

static uint8_t spl06_read_register(rt_device_t dev, uint8_t addr)
{
    uint8_t recv_buf[1];
    spl06_read(dev, addr, recv_buf, 1);
    return recv_buf[0];
}

static void _spl_dump_coe(coe_t *coe)
{
    BSPL06_TRACE("c0:%d\r\n", coe->c0);
    BSPL06_TRACE("c1:%d\r\n", coe->c1);
    BSPL06_TRACE("c00:%d\r\n", coe->c00);
    BSPL06_TRACE("c10:%d\r\n", coe->c10);
    BSPL06_TRACE("c01:%d\r\n", coe->c01);
    BSPL06_TRACE("c11:%d\r\n", coe->c11);
    BSPL06_TRACE("c20:%d\r\n", coe->c20);
    BSPL06_TRACE("c21:%d\r\n", coe->c21);
    BSPL06_TRACE("c30:%d\r\n", coe->c30);
}


void spl0601_rateset(rt_device_t dev, uint8_t iSensor, uint8_t u8SmplRate, uint8_t u8OverSmpl)
{
    spl06_t *spl06 = (spl06_t*)dev;
    uint8_t reg = 0;
    int32_t i32kpkt = 0;
    switch(u8SmplRate)
    {
        case 2:
            reg |= (1<<5);
            break;
        case 4:
            reg |= (2<<5);
            break;
        case 8:
            reg |= (3<<5);
            break;
        case 16:
            reg |= (4<<5);
            break;
        case 32:
            reg |= (5<<5);
            break;
        case 64:
            reg |= (6<<5);
            break;
        case 128:
            reg |= (7<<5);
            break;
        case 1:
        default:
            break;
    }
    switch(u8OverSmpl)
    {
        case 2:
            reg |= 1;
            i32kpkt = 1572864;
            break;
        case 4:
            reg |= 2;
            i32kpkt = 3670016;
            break;
        case 8:
            reg |= 3;
            i32kpkt = 7864320;
            break;
        case 16:
            i32kpkt = 253952;
            reg |= 4;
            break;
        case 32:
            i32kpkt = 516096;
            reg |= 5;
            break;
        case 64:
            i32kpkt = 1040384;
            reg |= 6;
            break;
        case 128:
            i32kpkt = 2088960;
            reg |= 7;
            break;
        case 1:
        default:
            i32kpkt = 524288;
            break;
    }

    if(iSensor == 0)
    {
        spl06->kp = i32kpkt;
        spl06_write_register(dev, SPL_PRS_CFG_ADDR, reg);
        if(u8OverSmpl > 8)
        {
            reg = spl06_read_register(dev, SPL_CFG_REG_ADDR);
            spl06_write_register(dev, SPL_CFG_REG_ADDR, reg | 0x04);
        }
    }
    if(iSensor == 1)
    {
        spl06->kt = i32kpkt;
        spl06_write_register(dev, SPL_TMP_CFG_ADDR, reg | 0x80);  //Using mems temperature
        if(u8OverSmpl > 8)
        {
            reg = spl06_read_register(dev, SPL_CFG_REG_ADDR);
            spl06_write_register(dev, SPL_CFG_REG_ADDR, reg | 0x08);
        }
    }
}




uint32_t spl_get_prs_tem(rt_device_t dev, float *prs, float *tmp)
{
    spl06_t *spl06 = (spl06_t*)dev;
    uint8_t buf[6];
    int32_t raw_temp, raw_prs;
    
    uint8_t reg_addr = SPL_PRS_B2_ADDR;
    
    spl06_read(dev, reg_addr, buf, sizeof(buf));
    
    raw_temp = ((int32_t)buf[3]<<16) | ((int32_t)buf[4]<<8) | ((int32_t)buf[5]);
    raw_temp = (raw_temp & 0x800000) ? (0xFF000000 | raw_temp) : raw_temp;
    spl06->temp_raw = raw_temp;
    
    raw_prs = ((int32_t)buf[0]<<16) | ((int32_t)buf[1]<<8) | ((int32_t)buf[2]);
    raw_prs = (raw_prs & 0x800000) ? (0xFF000000 | raw_prs) : raw_prs;
    spl06->press_raw = raw_prs;
    
    // calculate
    float ftsc = (float)spl06->temp_raw / spl06->kt;
    float fpsc = (float)spl06->press_raw / spl06->kp;
    float qua2 = (float)spl06->coe.c10 + fpsc * ((float)spl06->coe.c20 + fpsc * (float)spl06->coe.c30);
    float qua3 = ftsc * fpsc * ((float)spl06->coe.c11 + fpsc * (float)spl06->coe.c21);
            

    float fp = (float)spl06->coe.c00 + fpsc * qua2 + ftsc * (float)spl06->coe.c01 + qua3;

    // altitude calculations based on http://www.kansasflyer.org/index.asp?nav=Avi&sec=Alti&tab=Theory&pg=1

    // tropospheric properties (0-11km) for standard atmosphere
    const float T1 = 15.0f + 273.15f;       // temperature at base height in Kelvin
    const float a  = -6.5f / 1000.0f;       // temperature gradient in degrees per metre
    const float g  = 9.80665f;              // gravity constant in m/s/s
    const float R  = 287.05f;               // ideal gas constant in J/kg/K
    const float msl_pressure = 101325.0f;   // in Pa
    
    float pK = fp / msl_pressure;
    spl06->temperature = (float)spl06->coe.c0 * 0.5f + (float)spl06->coe.c1 * ftsc;
    spl06->pressure = fp;
    spl06->altitude = (((powf(pK, (-(a * R) / g))) * T1) - T1) / a;
            
    *prs = spl06->pressure;
    *tmp = spl06->temperature;
    return 0;
}


//static float prs2altitude(float prs)
//{
//    return (1.0 - pow(prs / 101325, 0.190284)) * 287.15 / 0.0065;
//}


void spl0601_get_calib_param(rt_device_t dev)
{
    spl06_t *spl06 = (spl06_t*)dev;
    uint8_t buf[18] = { 0 };

    uint8_t reg_addr = SPL_COEF_START_ADDR;
    spl06_read(dev, reg_addr, buf, sizeof(buf));

     
    spl06->coe.c0 = (uint16_t)buf[0] << 4 | (uint16_t)buf[1] >> 4;
    spl06->coe.c0 = (spl06->coe.c0 & 1 << 11) ? (0xf000 | spl06->coe.c0) : spl06->coe.c0;

    spl06->coe.c1 = (uint16_t)(buf[1] & 0x0f) << 8 | (uint16_t)buf[2];
    spl06->coe.c1 = (spl06->coe.c1 & 1 << 11) ? (0xf000 | spl06->coe.c1) : spl06->coe.c1;

    spl06->coe.c00 = (uint32_t)buf[3] << 12 | (uint32_t)buf[4] << 4 | (uint16_t)buf[5] >> 4;
    spl06->coe.c00 = (spl06->coe.c00 & 1 << 19) ? (0xfff00000 | spl06->coe.c00) : spl06->coe.c00;

    spl06->coe.c10 = (uint32_t)(buf[5] & 0x0f) << 16 | (uint32_t)buf[6] << 8 | (uint32_t)buf[7];
    spl06->coe.c10 = (spl06->coe.c10 & 1 << 19) ? (0xfff00000 | spl06->coe.c10) : spl06->coe.c10;

    spl06->coe.c01 = (uint16_t)buf[8] << 8 | buf[9];
    spl06->coe.c11 = (uint16_t)buf[10] << 8 | buf[11];
    spl06->coe.c20 = (uint16_t)buf[12] << 8 | buf[13];
    spl06->coe.c21 = (uint16_t)buf[14] << 8 | buf[15];
    spl06->coe.c30 = (uint16_t)buf[16] << 8 | buf[17];
    
}

static rt_err_t rt_spl06_open(rt_device_t dev, rt_uint16_t oflag)
{
    uint8_t val, ret;
    spl06_t *spl06 = (spl06_t*)dev;
    
    val = spl06_read_register(dev, SPL_PRODUCT_ID_ADDR);
    
    if((val & 0xF0) == 0x10)
    {
        spl0601_get_calib_param(dev);
        _spl_dump_coe(&spl06->coe);

        spl0601_rateset(dev, 0, 64, 32);
        spl0601_rateset(dev, 1, 32, 8);
        
        spl06_write_register(dev, SPL_CFG_REG_ADDR, (1<<2) | (1<<3));
        spl06_write_register(dev, SPL_MEAS_CFG_ADDR, 0x07);              /* continue P and T */
        
        ret = RT_EOK;
    }
    else
    {
        ret = RT_EIO;
    }
    return ret;
}


static rt_size_t rt_spl06_read(rt_device_t dev, rt_off_t pos, void* buffer, rt_size_t size)
{
    float t, p;
    spl_get_prs_tem(dev, &p, &t);

    //p = prs2altitude(p);
        
    rt_memcpy(buffer, (uint8_t*)&p, sizeof(p));
    return size;
}




int rt_hw_spl06_init(const char *name, const char *spid_name, uint32_t irq)
{
    spl06_t *spl06 = rt_malloc(sizeof(spl06_t));
    if(!spl06)
    {
        return RT_ERROR;
    }
    
    spl06->spid = (struct rt_spi_device *)rt_device_find(spid_name);
    if(!spl06->spid)
    {
        return RT_ERROR;
    }
    
	spl06->parent.type               = RT_Device_Class_Sensor;
	spl06->parent.rx_indicate        = RT_NULL;
	spl06->parent.tx_complete        = RT_NULL;
	spl06->parent.init               = RT_NULL;
	spl06->parent.open               = rt_spl06_open;
	spl06->parent.close              = RT_NULL;
	spl06->parent.read               = rt_spl06_read;
	spl06->parent.write              = RT_NULL;
	spl06->parent.control            = RT_NULL;
	spl06->parent.user_data          = &spl06;

    return rt_device_register(&spl06->parent, name, RT_DEVICE_FLAG_RDWR);
}

