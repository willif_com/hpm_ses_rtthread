#include <rtthread.h>
#include <stdio.h>
#include <math.h>
#include <rtdevice.h>
#include "drivers/rt_sensor.h"


#define CRMXXX_DEBUG		0
#if ( CRMXXX_DEBUG == 1 )
#include <stdio.h>
#define CRMXXX_TRACE	printf
#else
#define CRMXXX_TRACE(...)
#endif

typedef struct
{
    uint8_t     reg_val;
    float       ratio;     
}crmxxx_cfg_t;

typedef struct
{
    struct                  rt_device parent;
    struct rt_spi_device   *spi_devce_x;
    struct rt_spi_device   *spi_devce_y;
    struct rt_spi_device   *spi_devce_z;
    uint8_t                 for_setting;/* frame of reference setting */
    uint8_t                 gyr_range;
}rt_crm_t;

static const crmxxx_cfg_t crmxxx_gyr_rg_sel[] = 
{
    {3,   96},   /* 75DPS */
    {2,   48},   /* 150DPS */
    {1,   24},   /* 300DPS */
    {0,   8},   /* 900DPS */
};

static rt_err_t rt_crmxxx_init(rt_device_t dev)
{    
    return RT_EOK;
}



static void _process_for(int16_t *buf, uint8_t for_setting)
{
    int16_t tmp;
    switch(for_setting)
    {
        case RT_ROT_Z_P90: /*  X' = Y,  Y' = -X */
            tmp = buf[0];
            buf[0] = buf[1];
            buf[1] = -tmp;
            break;
        case RT_ROT_Z_N90:  /*  X' = -Y,  Y' = X */
            tmp = buf[0];
            buf[0] = -buf[1];
            buf[1] = tmp;
            break;
        case RT_ROT_Z_P180: /*  X' = -X,  Y' = -Y */
            buf[0] = -buf[0];
            buf[1] = -buf[1];
            break;
        case RT_ROT_X_180:
            buf[1] = -buf[1];
            buf[2] = -buf[2];
            break;
        case RT_ROT_Y_180:
            buf[0] = -buf[0];
            buf[2] = -buf[2];
            break;
        case RT_ROT_Y_180_ZN90:
            tmp = buf[0];
            buf[0] = buf[1];
            buf[1] = tmp;
            buf[2] = -buf[2];
            break;
        case RT_ROT_Y_180_ZP90:
            tmp = buf[0];
            buf[0] = -buf[1];
            buf[1] = -tmp;
            buf[2] = -buf[2];
            break;
    }
}

static int crmxxx_get_data(struct rt_spi_device *device, float *gyr, float *tempature)
{
    rt_crm_t *crm = (rt_crm_t*)device->user_data;
    
    uint8_t tx_buf[6] = {0};
    uint8_t rx_buf[6] = {0};
    
    tx_buf[0] = (1<<5) | (crmxxx_gyr_rg_sel[crm->gyr_range].reg_val << 3); /* RRS=1 */
    tx_buf[5] = ~(tx_buf[0] + tx_buf[1] + tx_buf[2] + tx_buf[3] + tx_buf[4]);
    
    rt_spi_transfer(device, tx_buf, rx_buf, 6);
    
    uint8_t chksum = ~(rx_buf[0] + rx_buf[1] + rx_buf[2] + rx_buf[3] + rx_buf[4]);
    
    if(rx_buf[5] == chksum && rx_buf[0] == crmxxx_gyr_rg_sel[crm->gyr_range].reg_val)
    {
        gyr[0] = ((int16_t)((rx_buf[1]<<8) | rx_buf[2]));
        gyr[0] /= crmxxx_gyr_rg_sel[crm->gyr_range].ratio;
        
        tempature[0] = ((int16_t)((rx_buf[3]<<8) | rx_buf[4]));
        tempature[0] -= 531;
        tempature[0] /= 2.75;
        return RT_EOK;
    }
    
    return -RT_ERROR;
}

#define AVG_CNT  (3)

static rt_size_t rt_crmxxx_read(rt_device_t dev, rt_off_t pos, void* buffer, rt_size_t size)
{
    int i;
    float *buf = buffer;
    
    rt_crm_t *crm = (rt_crm_t*)dev;
    float gx, gy, gz, tempx, tempy, tempz;
    static float stempz;
    switch(pos)
    {
        case RT_SENSOR_POS_ACC:
        case RT_SENSOR_POS_ACC_FIFO:
            buf[0] = 0;
            buf[1] = 0;
            buf[2] = 0;
            break;
        case RT_SENSOR_POS_GYR:
        case RT_SENSOR_POS_GYR_FIFO:
            buf[0] = 0;
            buf[1] = 0;
            buf[2] = 0;
        
            for(i=0; i<AVG_CNT; i++)
            {
                if(crmxxx_get_data(crm->spi_devce_x, &gx, &tempx) == RT_EOK)
                {
                    buf[0] += -gx;
                }
                
                if(crmxxx_get_data(crm->spi_devce_y, &gy, &tempy) == RT_EOK)
                {
                    buf[1] += -gz;
                }
            
                if(crmxxx_get_data(crm->spi_devce_z, &gz, &tempz) == RT_EOK)
                {
                    buf[2] += -gy;
                    stempz = tempz;
                }
            }
            
            buf[0] /= AVG_CNT;
            buf[1] /= AVG_CNT;
            buf[2] /= AVG_CNT;

            break;
        case RT_SENSOR_POS_GYR_TEMPERATURE:
                buf[0] = stempz; /* just for covionence */
            break;
    }
    return size;
}

static rt_size_t rt_crmxxx_write(rt_device_t dev, rt_off_t pos, const void* buffer, rt_size_t size)
{
    uint8_t *val = (uint8_t *)buffer;
    rt_crm_t *crm = (rt_crm_t*)dev;
    
    switch(pos)
    {
        case RT_SENSOR_POS_GYR_RST:

            break;
        case RT_SENSOR_POS_FOR:
            crm->for_setting = val[0];
            break;
        case RT_SENSOR_POS_GYR_BW:

            break;
        case RT_SENSOR_POS_GYR_RG:
            crm->gyr_range = *val;
            break;
    }
    return size;
}



int rt_hw_crmxxx_init(const char *name, const char *spi_name_x, const char *spi_name_y, const char *spi_name_z)
{
    rt_crm_t *crm = rt_malloc(sizeof(rt_crm_t));
    
    if(crm == RT_NULL)
    {
        return -RT_ENOMEM;
    }
    
    crm->spi_devce_x = (struct rt_spi_device *)rt_device_find(spi_name_x);
    crm->spi_devce_y = (struct rt_spi_device *)rt_device_find(spi_name_y);
    crm->spi_devce_z = (struct rt_spi_device *)rt_device_find(spi_name_z);
    
    struct rt_spi_configuration configuration = {0};
    configuration.reserved = 26; /* CRM need insert 20us delay before CS->0 and CS->1 */
    
    rt_spi_configure(crm->spi_devce_x, &configuration);
    rt_spi_configure(crm->spi_devce_y, &configuration);
    rt_spi_configure(crm->spi_devce_z, &configuration);
    
	crm->parent.type               = RT_Device_Class_Sensor;
	crm->parent.rx_indicate        = RT_NULL;
	crm->parent.tx_complete        = RT_NULL;
	crm->parent.init               = rt_crmxxx_init;
	crm->parent.open               = RT_NULL;
	crm->parent.close              = RT_NULL;
	crm->parent.read               = rt_crmxxx_read;
	crm->parent.write              = rt_crmxxx_write;
	crm->parent.control            = RT_NULL;
	crm->parent.user_data          = crm;
    
    crm->spi_devce_x->user_data = crm;
    crm->spi_devce_y->user_data = crm;
    crm->spi_devce_z->user_data = crm;
 
    crm->for_setting = 0;
    crm->gyr_range = 0;
    
    return rt_device_register(&crm->parent, name, RT_DEVICE_FLAG_RDWR);
}


