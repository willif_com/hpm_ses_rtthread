#include <rtthread.h>
#include <stdio.h>
#include <math.h>
#include <rtdevice.h>
#include "drivers/rt_sensor.h"

#define XV7001_DEBUG		0
#if ( XV7001_DEBUG == 1 )
#include <stdio.h>
#define XV7001_TRACE	printf
#else
#define XV7001_TRACE(...)
#endif

#define VX7001_DSPCTL1      (0x01)
#define VX7001_DSPCTL2      (0x02)
#define VX7001_DSPCTL3      (0x03)
#define VX7001_STSRD        (0x04)
#define VX7001_STBY         (0x07)
#define VX7001_TEMPRD       (0x08)
#define VX7001_SWRST        (0x09)
#define VX7001_DATACCON     (0x0A)
#define VX7001_OUTCTL1      (0x0B)
#define VX7001_AUTOC        (0x0C)
#define VX7001_DSPRES       (0x0D)
#define VX7001_MEMLOAD      (0x1B)
#define VX7001_TSDATAFORMAT (0x1C)
#define VX7001_IFCTL        (0x1F)

typedef struct
{
    struct                  rt_device parent;
    struct rt_spi_device   *spid;
    uint8_t                 rx_gyr_dma_buf[12];   /* async rx buf */
    uint8_t                 irq;        /* gyr intruupt pin */
    uint8_t                 gyr_chip_id;
    uint8_t                 gyr_fifo_read_size;
    float                   gyr_ratio;  /* ratio for DPS */
}rt_xv7000_t;


static void xv_write_register(rt_device_t dev, uint8_t addr, uint8_t val)
{
    rt_xv7000_t *xv = (rt_xv7000_t*)dev;

    uint8_t send_buf[2];
    
    send_buf[0] = (0<<7) | addr;
    send_buf[1] = val;
    
    rt_spi_send(xv->spid, send_buf, 2);
}

static void xv_read(rt_device_t dev, uint8_t addr, uint8_t *rx_buf, uint32_t len)
{
    rt_xv7000_t *xv = (rt_xv7000_t*)dev;
    uint8_t send_buf[1];
    
    send_buf[0] = (1<<7) | addr;
    rt_spi_send_then_recv(xv->spid, send_buf, 1, rx_buf, len);
}

static uint8_t xv_read_register(rt_device_t dev, uint8_t addr)
{
    uint8_t recv_buf[1];
    xv_read(dev, addr, recv_buf, 1);
    return recv_buf[0];
}


static rt_err_t rt_xv7001_open(rt_device_t dev, rt_uint16_t oflag)
{
    rt_xv7000_t *xv = (rt_xv7000_t*)dev;
    
    xv_write_register(dev, VX7001_DSPCTL1, 0x21);
    if(xv_read_register(dev, VX7001_DSPCTL1) == 0x21)
    {
        xv_write_register(dev, VX7001_IFCTL, 0x00);
        xv_write_register(dev, VX7001_OUTCTL1, 0x01);
        
        xv_write_register(dev, VX7001_DSPCTL3, 0x0); /* FS  */
//    xv_write_register(dev, VX7001_DSPCTL3, 0x01); /* FS /2 */
//        xv_write_register(dev, VX7001_DSPCTL3, 0x03); /* FS /8 */
        xv_write_register(dev, VX7001_DSPCTL3, 0x05); /* FS /32 */
//        xv_write_register(dev, VX7001_DSPCTL3, 0x06); /* FS /64 */
//        xv_write_register(dev, VX7001_DSPCTL3, 0x07); /* FS /128 */
        

//        xv_write_register(dev, VX7001_DSPCTL2, 0x00 | (2<<4));   /* LPF 10Hz */
        xv_write_register(dev, VX7001_DSPCTL2, 0x01 | (2<<4));   /* LPF 35Hz */
//        xv_write_register(dev, VX7001_DSPCTL2, 0x02 | (2<<4));   /* LPF 45Hz */
//        xv_write_register(dev, VX7001_DSPCTL2, 0x03 | (2<<4));   /* LPF 50Hz */
//        xv_write_register(dev, VX7001_DSPCTL2, 0x05 | (2<<4));   /* LPF 85Hz */
//        xv_write_register(dev, VX7001_DSPCTL2, 0x06| (2<<4));   /* LPF 100Hz */
//        xv_write_register(dev, VX7001_DSPCTL2, 0x07| (2<<4));   /* LPF 140Hz */
//        xv_write_register(dev, VX7001_DSPCTL2, 0x08| (2<<4));   /* LPF 175Hz */
//        xv_write_register(dev, VX7001_DSPCTL2, 0x09 | (2<<4));   /* LPF 200Hz */
//        xv_write_register(dev, VX7001_DSPCTL2, 0x0D | (2<<4));   /* LPF 500Hz */
        
        xv->gyr_ratio = (1.0F / 17920);
        XV7001_TRACE("XV found\r\n");
    }
    else
    {
        XV7001_TRACE("XV not found\r\n");
        return RT_ERROR;
    }
    
    return RT_EOK;
}


static rt_size_t rt_xv7001_read(rt_device_t dev, rt_off_t pos, void* buffer, rt_size_t size)
{
    float *buf = buffer;
    int32_t val;
    rt_xv7000_t *xv = (rt_xv7000_t*)dev;
    
    switch(pos)
    {
        case RT_SENSOR_POS_GYR_TEMPERATURE:
            xv_read(dev, VX7001_TEMPRD, xv->rx_gyr_dma_buf, 2);
            val = (int32_t)(xv->rx_gyr_dma_buf[0]<<8 | xv->rx_gyr_dma_buf[1]<<0);
            val >>= 4;
            buf[0] = (float)val / 16.0F;
            break;
        case RT_SENSOR_POS_GYR:
            xv_read(dev, VX7001_DATACCON, xv->rx_gyr_dma_buf, 3);
            val = (int32_t)((xv->rx_gyr_dma_buf[0]<<24) | xv->rx_gyr_dma_buf[1]<<16 | xv->rx_gyr_dma_buf[2]<<8);
            val >>= 8;
            buf[0] = -xv->gyr_ratio * (float)val;
            break;
        default:
            break;
    }
    return size;
}





int rt_hw_xv7001_init(const char *name, const char *spid_name, uint32_t irq)
{
    rt_xv7000_t *xv = rt_malloc(sizeof(rt_xv7000_t));
    if(!xv)
    {
        return RT_ERROR;
    }
    
    xv->spid = (struct rt_spi_device *)rt_device_find(spid_name);
    if(!xv->spid)
    {
        return RT_ERROR;
    }
    
	xv->parent.type               = RT_Device_Class_Miscellaneous;
	xv->parent.rx_indicate        = RT_NULL;
	xv->parent.tx_complete        = RT_NULL;
	xv->parent.init               = RT_NULL;
	xv->parent.open               = rt_xv7001_open;
	xv->parent.close              = RT_NULL;
	xv->parent.read               = rt_xv7001_read;
	xv->parent.write              = RT_NULL;
	xv->parent.control            = RT_NULL;
	xv->parent.user_data          = &xv;
    xv->irq = irq;
    
    return rt_device_register(&xv->parent, name, RT_DEVICE_FLAG_RDWR);
}





