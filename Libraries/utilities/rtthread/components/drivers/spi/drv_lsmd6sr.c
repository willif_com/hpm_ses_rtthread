#include <rtthread.h>
#include <stdio.h>
#include <rtdevice.h>
#include "drivers/rt_sensor.h"


#define LSMD_DEBUG		0
#if ( LSMD_DEBUG == 1 )
#include <stdio.h>
#define LSMD_TRACE	printf
#else
#define LSMD_TRACE(...)
#endif

#define LSM6DSL_ACC_GYRO_WHO_AM_I           0x6A

/************** Device Register  *******************/

#define LSM6DSL_ACC_GYRO_FUNC_CFG_ACCESS    0x01

#define LSM6DSL_ACC_GYRO_SENSOR_SYNC_TIME   0x04
#define LSM6DSL_ACC_GYRO_SENSOR_RES_RATIO   0x05

#define LSM6DSL_ACC_GYRO_FIFO_CTRL1         0x06
#define LSM6DSL_ACC_GYRO_FIFO_CTRL2         0x07
#define LSM6DSL_ACC_GYRO_FIFO_CTRL3         0x08
#define LSM6DSL_ACC_GYRO_FIFO_CTRL4         0x09
#define LSM6DSL_ACC_GYRO_FIFO_CTRL5         0x0A

#define LSM6DSL_ACC_GYRO_DRDY_PULSE_CFG_G   0x0B
#define LSM6DSL_ACC_GYRO_INT1_CTRL          0x0D
#define LSM6DSL_ACC_GYRO_INT2_CTRL          0x0E
#define LSM6DSL_ACC_GYRO_WHO_AM_I_REG       0x0F
#define LSM6DSL_ACC_GYRO_CTRL1_XL           0x10
#define LSM6DSL_ACC_GYRO_CTRL2_G            0x11
#define LSM6DSL_ACC_GYRO_CTRL3_C            0x12
#define LSM6DSL_ACC_GYRO_CTRL4_C            0x13
#define LSM6DSL_ACC_GYRO_CTRL5_C            0x14
#define LSM6DSL_ACC_GYRO_CTRL6_C            0x15
#define LSM6DSL_ACC_GYRO_CTRL7_G            0x16
#define LSM6DSL_ACC_GYRO_CTRL8_XL           0x17
#define LSM6DSL_ACC_GYRO_CTRL9_XL           0x18
#define LSM6DSL_ACC_GYRO_CTRL10_C           0x19

#define LSM6DSL_ACC_GYRO_MASTER_CONFIG      0x1A
#define LSM6DSL_ACC_GYRO_WAKE_UP_SRC        0x1B
#define LSM6DSL_ACC_GYRO_TAP_SRC            0x1C
#define LSM6DSL_ACC_GYRO_D6D_SRC            0x1D
#define LSM6DSL_ACC_GYRO_STATUS_REG         0x1E

#define LSM6DSL_ACC_GYRO_OUT_TEMP_L         0x20
#define LSM6DSL_ACC_GYRO_OUT_TEMP_H         0x21
#define LSM6DSL_ACC_GYRO_OUTX_L_G           0x22
#define LSM6DSL_ACC_GYRO_OUTX_H_G           0x23
#define LSM6DSL_ACC_GYRO_OUTY_L_G           0x24
#define LSM6DSL_ACC_GYRO_OUTY_H_G           0x25
#define LSM6DSL_ACC_GYRO_OUTZ_L_G           0x26
#define LSM6DSL_ACC_GYRO_OUTZ_H_G           0x27
#define LSM6DSL_ACC_GYRO_OUTX_L_XL          0x28
#define LSM6DSL_ACC_GYRO_OUTX_H_XL          0x29
#define LSM6DSL_ACC_GYRO_OUTY_L_XL          0x2A
#define LSM6DSL_ACC_GYRO_OUTY_H_XL          0x2B
#define LSM6DSL_ACC_GYRO_OUTZ_L_XL          0x2C
#define LSM6DSL_ACC_GYRO_OUTZ_H_XL          0x2D
#define LSM6DSL_ACC_GYRO_SENSORHUB1_REG     0x2E
#define LSM6DSL_ACC_GYRO_SENSORHUB2_REG     0x2F
#define LSM6DSL_ACC_GYRO_SENSORHUB3_REG     0x30
#define LSM6DSL_ACC_GYRO_SENSORHUB4_REG     0x31
#define LSM6DSL_ACC_GYRO_SENSORHUB5_REG     0x32
#define LSM6DSL_ACC_GYRO_SENSORHUB6_REG     0x33
#define LSM6DSL_ACC_GYRO_SENSORHUB7_REG     0x34
#define LSM6DSL_ACC_GYRO_SENSORHUB8_REG     0x35
#define LSM6DSL_ACC_GYRO_SENSORHUB9_REG     0x36
#define LSM6DSL_ACC_GYRO_SENSORHUB10_REG    0x37
#define LSM6DSL_ACC_GYRO_SENSORHUB11_REG    0x38
#define LSM6DSL_ACC_GYRO_SENSORHUB12_REG    0x39
#define LSM6DSL_ACC_GYRO_FIFO_STATUS1       0x3A
#define LSM6DSL_ACC_GYRO_FIFO_STATUS2       0x3B
#define LSM6DSL_ACC_GYRO_FIFO_STATUS3       0x3C
#define LSM6DSL_ACC_GYRO_FIFO_STATUS4       0x3D
#define LSM6DSL_ACC_GYRO_FIFO_DATA_OUT_L    0x3E
#define LSM6DSL_ACC_GYRO_FIFO_DATA_OUT_H    0x3F
#define LSM6DSL_ACC_GYRO_TIMESTAMP0_REG     0x40
#define LSM6DSL_ACC_GYRO_TIMESTAMP1_REG     0x41
#define LSM6DSL_ACC_GYRO_TIMESTAMP2_REG     0x42

#define LSM6DSL_ACC_GYRO_TIMESTAMP_L        0x49
#define LSM6DSL_ACC_GYRO_TIMESTAMP_H        0x4A

#define LSM6DSL_ACC_GYRO_STEP_COUNTER_L     0x4B
#define LSM6DSL_ACC_GYRO_STEP_COUNTER_H     0x4C

#define LSM6DSL_ACC_GYRO_SENSORHUB13_REG    0x4D
#define LSM6DSL_ACC_GYRO_SENSORHUB14_REG    0x4E
#define LSM6DSL_ACC_GYRO_SENSORHUB15_REG    0x4F
#define LSM6DSL_ACC_GYRO_SENSORHUB16_REG    0x50
#define LSM6DSL_ACC_GYRO_SENSORHUB17_REG    0x51
#define LSM6DSL_ACC_GYRO_SENSORHUB18_REG    0x52

#define LSM6DSL_ACC_GYRO_FUNC_SRC           0x53
#define LSM6DSL_ACC_GYRO_TAP_CFG1           0x58
#define LSM6DSL_ACC_GYRO_TAP_THS_6D         0x59
#define LSM6DSL_ACC_GYRO_INT_DUR2           0x5A
#define LSM6DSL_ACC_GYRO_WAKE_UP_THS        0x5B
#define LSM6DSL_ACC_GYRO_WAKE_UP_DUR        0x5C
#define LSM6DSL_ACC_GYRO_FREE_FALL          0x5D
#define LSM6DSL_ACC_GYRO_MD1_CFG            0x5E
#define LSM6DSL_ACC_GYRO_MD2_CFG            0x5F

#define LSM6DSL_ACC_GYRO_OUT_MAG_RAW_X_L    0x66
#define LSM6DSL_ACC_GYRO_OUT_MAG_RAW_X_H    0x67
#define LSM6DSL_ACC_GYRO_OUT_MAG_RAW_Y_L    0x68
#define LSM6DSL_ACC_GYRO_OUT_MAG_RAW_Y_H    0x69
#define LSM6DSL_ACC_GYRO_OUT_MAG_RAW_Z_L    0x6A
#define LSM6DSL_ACC_GYRO_OUT_MAG_RAW_Z_H    0x6B

#define LSM6DSL_ACC_GYRO_X_OFS_USR          0x73
#define LSM6DSL_ACC_GYRO_Y_OFS_USR          0x74
#define LSM6DSL_ACC_GYRO_Z_OFS_USR          0x75

/************** Embedded functions register mapping  *******************/
#define LSM6DSL_ACC_GYRO_SLV0_ADD                     0x02
#define LSM6DSL_ACC_GYRO_SLV0_SUBADD                  0x03
#define LSM6DSL_ACC_GYRO_SLAVE0_CONFIG                0x04
#define LSM6DSL_ACC_GYRO_SLV1_ADD                     0x05
#define LSM6DSL_ACC_GYRO_SLV1_SUBADD                  0x06
#define LSM6DSL_ACC_GYRO_SLAVE1_CONFIG                0x07
#define LSM6DSL_ACC_GYRO_SLV2_ADD                     0x08
#define LSM6DSL_ACC_GYRO_SLV2_SUBADD                  0x09
#define LSM6DSL_ACC_GYRO_SLAVE2_CONFIG                0x0A
#define LSM6DSL_ACC_GYRO_SLV3_ADD                     0x0B
#define LSM6DSL_ACC_GYRO_SLV3_SUBADD                  0x0C
#define LSM6DSL_ACC_GYRO_SLAVE3_CONFIG                0x0D
#define LSM6DSL_ACC_GYRO_DATAWRITE_SRC_MODE_SUB_SLV0  0x0E
#define LSM6DSL_ACC_GYRO_CONFIG_PEDO_THS_MIN          0x0F

#define LSM6DSL_ACC_GYRO_SM_STEP_THS                  0x13
#define LSM6DSL_ACC_GYRO_PEDO_DEB_REG                 0x14
#define LSM6DSL_ACC_GYRO_STEP_COUNT_DELTA             0x15

#define LSM6DSL_ACC_GYRO_MAG_SI_XX                    0x24
#define LSM6DSL_ACC_GYRO_MAG_SI_XY                    0x25
#define LSM6DSL_ACC_GYRO_MAG_SI_XZ                    0x26
#define LSM6DSL_ACC_GYRO_MAG_SI_YX                    0x27
#define LSM6DSL_ACC_GYRO_MAG_SI_YY                    0x28
#define LSM6DSL_ACC_GYRO_MAG_SI_YZ                    0x29
#define LSM6DSL_ACC_GYRO_MAG_SI_ZX                    0x2A
#define LSM6DSL_ACC_GYRO_MAG_SI_ZY                    0x2B
#define LSM6DSL_ACC_GYRO_MAG_SI_ZZ                    0x2C
#define LSM6DSL_ACC_GYRO_MAG_OFFX_L                   0x2D
#define LSM6DSL_ACC_GYRO_MAG_OFFX_H                   0x2E
#define LSM6DSL_ACC_GYRO_MAG_OFFY_L                   0x2F
#define LSM6DSL_ACC_GYRO_MAG_OFFY_H                   0x30
#define LSM6DSL_ACC_GYRO_MAG_OFFZ_L                   0x31
#define LSM6DSL_ACC_GYRO_MAG_OFFZ_H                   0x32

/* Accelero Full_ScaleSelection */
#define LSM6DSL_ACC_FULLSCALE_2G          ((uint8_t)0x00) /*!< ? g */
#define LSM6DSL_ACC_FULLSCALE_4G          ((uint8_t)0x08) /*!< ? g */
#define LSM6DSL_ACC_FULLSCALE_8G          ((uint8_t)0x0C) /*!< ? g */
#define LSM6DSL_ACC_FULLSCALE_16G         ((uint8_t)0x04) /*!< ?6 g */

/* Accelero Full Scale Sensitivity */
#define LSM6DSL_ACC_SENSITIVITY_2G     ((float)0.061f)  /*!< accelerometer sensitivity with 2 g full scale  [mgauss/LSB] */
#define LSM6DSL_ACC_SENSITIVITY_4G     ((float)0.122f)  /*!< accelerometer sensitivity with 4 g full scale  [mgauss/LSB] */
#define LSM6DSL_ACC_SENSITIVITY_8G     ((float)0.244f)  /*!< accelerometer sensitivity with 8 g full scale  [mgauss/LSB] */
#define LSM6DSL_ACC_SENSITIVITY_16G    ((float)0.488f)  /*!< accelerometer sensitivity with 12 g full scale [mgauss/LSB] */

/* Accelero Power Mode selection */
#define LSM6DSL_ACC_GYRO_LP_XL_DISABLED     ((uint8_t)0x00) /* LP disabled*/
#define LSM6DSL_ACC_GYRO_LP_XL_ENABLED      ((uint8_t)0x10) /* LP enabled*/

/* Output Data Rate */
#define LSM6DSL_ODR_BITPOSITION      ((uint8_t)0xF0)  /*!< Output Data Rate bit position */  
#define LSM6DSL_ODR_POWER_DOWN       ((uint8_t)0x00) /* Power Down mode       */
#define LSM6DSL_ODR_13Hz             ((uint8_t)0x10) /* Low Power mode        */
#define LSM6DSL_ODR_26Hz             ((uint8_t)0x20) /* Low Power mode        */ 
#define LSM6DSL_ODR_52Hz             ((uint8_t)0x30) /* Low Power mode        */
#define LSM6DSL_ODR_104Hz            ((uint8_t)0x40) /* Normal mode           */
#define LSM6DSL_ODR_208Hz            ((uint8_t)0x50) /* Normal mode           */
#define LSM6DSL_ODR_416Hz            ((uint8_t)0x60) /* High Performance mode */
#define LSM6DSL_ODR_833Hz            ((uint8_t)0x70) /* High Performance mode */
#define LSM6DSL_ODR_1660Hz           ((uint8_t)0x80) /* High Performance mode */
#define LSM6DSL_ODR_3330Hz           ((uint8_t)0x90) /* High Performance mode */
#define LSM6DSL_ODR_6660Hz           ((uint8_t)0xA0) /* High Performance mode */ 

/* Gyro Full Scale Selection */
#define LSM6DSL_GYRO_FS_245            ((uint8_t)0x00)  
#define LSM6DSL_GYRO_FS_500            ((uint8_t)0x04)  
#define LSM6DSL_GYRO_FS_1000           ((uint8_t)0x08)  
#define LSM6DSL_GYRO_FS_2000           ((uint8_t)0x0C)

/* Gyro Full Scale Sensitivity */ 
#define LSM6DSL_GYRO_SENSITIVITY_245DPS            ((float)8.750f) /**< Sensitivity value for 245 dps full scale  [mdps/LSB] */ 
#define LSM6DSL_GYRO_SENSITIVITY_500DPS            ((float)17.50f) /**< Sensitivity value for 500 dps full scale  [mdps/LSB] */ 
#define LSM6DSL_GYRO_SENSITIVITY_1000DPS           ((float)35.00f) /**< Sensitivity value for 1000 dps full scale [mdps/LSB] */ 
#define LSM6DSL_GYRO_SENSITIVITY_2000DPS           ((float)70.00f) /**< Sensitivity value for 2000 dps full scale [mdps/LSB] */ 

/* Gyro Power Mode selection */
#define LSM6DSL_ACC_GYRO_LP_G_DISABLED     ((uint8_t)0x00) /* LP disabled*/
#define LSM6DSL_ACC_GYRO_LP_G_ENABLED      ((uint8_t)0x80) /* LP enabled*/

/* Block Data Update */  
#define LSM6DSL_BDU_CONTINUOS               ((uint8_t)0x00)
#define LSM6DSL_BDU_BLOCK_UPDATE            ((uint8_t)0x40)

/* Auto-increment */
#define LSM6DSL_ACC_GYRO_IF_INC_DISABLED    ((uint8_t)0x00)
#define LSM6DSL_ACC_GYRO_IF_INC_ENABLED     ((uint8_t)0x04)

typedef struct
{
    uint8_t     reg_val;
    float       ratio;     
}lsmd_rg_t;

static const lsmd_rg_t acc_rg_sel[] = 
{
    {0,   (2.0F/32768.0F)},
    {2,   (4.0F/32768.0F)},
    {3,   (8.0F/32768.0F)},
    {1, (16.0F/32768.0F)},
};

static const lsmd_rg_t lsdm_gyr_rg_sel[] = 
{
    {0,   (0.00875F)}, /* 250DPS */
    {1,   (0.0175F)},  /* 500DPS */
    {2,   (0.03F)},    /* 1000DPS */
    {3,   (0.07F)},    /* 2000DPS */
    {4,   (0.14F)},
};


typedef struct
{
    struct                  rt_device parent;
    struct rt_spi_device    *spid;
    uint8_t                 rx_gyr_dma_buf[12];   /* async rx buf */
    uint8_t                 rx_acc_dma_buf[12];
    uint8_t                 irq;        /* gyr intruupt pin */
    uint8_t                 chip_id;
    uint8_t                 gyr_fifo_read_size;
    uint8_t                 acc_fifo_read_size;
    float                   acc_ratio;  /* ratio for G */
    float                   gyr_ratio;  /* ratio for DPS */
    uint8_t                 for_setting;/* frame of reference setting */
}rt_lsmd_t;


static void lsmd_write_register(rt_device_t dev, uint8_t addr, uint8_t val)
{
    rt_lsmd_t *lsmd = (rt_lsmd_t*)dev;

    uint8_t send_buf[2];
    
    send_buf[0] = (0<<7) | addr;
    send_buf[1] = val;
    
    rt_spi_send(lsmd->spid, send_buf, 2);
}

static void lsmd_read(rt_device_t dev, uint8_t addr, uint8_t *rx_buf, uint32_t len)
{
    rt_lsmd_t *lsmd = (rt_lsmd_t*)dev;
    uint8_t send_buf[1];
    
    send_buf[0] = (1<<7) | addr;
    rt_spi_send_then_recv(lsmd->spid, send_buf, 1, rx_buf, len);
}

static uint8_t lsmd_read_register(rt_device_t dev, uint8_t addr)
{
    rt_lsmd_t *lsmd = (rt_lsmd_t*)dev;
    uint8_t send_buf[1], recv_buf[1];
    
    send_buf[0] = (1<<7) | addr;
    rt_spi_send_then_recv(lsmd->spid, send_buf, 1, recv_buf, 1);
    return recv_buf[0];
}




void lsdm_pin_cb(void *args)
{
//    printf("%s\r\n", __FUNCTION__);
}


static rt_err_t rt_lsmd_open(rt_device_t dev, rt_uint16_t oflag)
{
    int i;
    rt_lsmd_t *lsmd = (rt_lsmd_t*)dev;
    
    /* reset */
    lsmd_write_register(dev, LSM6DSL_ACC_GYRO_CTRL3_C, 0x01);
    rt_thread_mdelay(1);
    
    for(i=0; i<2; i++)
    {
        if(lsmd_read_register(dev, LSM6DSL_ACC_GYRO_WHO_AM_I_REG) == 0x6B)
        {
            lsmd->chip_id = 0x6B;
            break;
        }
    }
    
    if(lsmd->chip_id == 0x6B)
    {
        LSMD_TRACE("LSDM found:0x%X\r\n", lsmd->chip_id);
        /* ACC: 8G, 416Hz */
        lsmd_write_register(dev, LSM6DSL_ACC_GYRO_CTRL1_XL, (0<<2) | (6<<4));
        lsmd->acc_ratio = acc_rg_sel[0].ratio;
        
        /* GYR: 2000DPS, 416Hz */
        lsmd_write_register(dev, LSM6DSL_ACC_GYRO_CTRL2_G,  (lsdm_gyr_rg_sel[3].reg_val<<2) | (6<<4));
        lsmd->gyr_ratio = lsdm_gyr_rg_sel[3].ratio;
        
        /* data registoer shadow mode */
        lsmd_write_register(dev, LSM6DSL_ACC_GYRO_CTRL3_C, 0x44);
        
        
        /* enable GYR LPF1 */
        lsmd_write_register(dev, LSM6DSL_ACC_GYRO_CTRL4_C, 1<<1);
        
        /* enable GYR LPF1: 416HzRAW -> 48Hz */
        lsmd_write_register(dev, LSM6DSL_ACC_GYRO_CTRL6_C, 5<<0);
        
        /* turn on time: 35ms */
        rt_thread_mdelay(100);
        
    }
    else
    {
        LSMD_TRACE("LSDM not found:0x%X\r\n", lsmd->chip_id);
        return RT_ERROR;
    }
    
    return RT_EOK;
}

static void _process_for(int16_t *buf, uint8_t for_setting)
{
    int16_t tmp;
    switch(for_setting)
    {
        case RT_ROT_Z_P90: /*  X' = Y,  Y' = -X */
            tmp = buf[0];
            buf[0] = buf[1];
            buf[1] = -tmp;
            break;
        case RT_ROT_Z_N90:  /*  X' = -Y,  Y' = X */
            tmp = buf[0];
            buf[0] = -buf[1];
            buf[1] = tmp;
            break;
        case RT_ROT_Z_P180: /*  X' = -X,  Y' = -Y */
            buf[0] = -buf[0];
            buf[1] = -buf[1];
            break;
    }
}

static rt_size_t rt_lsmd_read(rt_device_t dev, rt_off_t pos, void* buffer, rt_size_t size)
{
    float *buf = buffer;
    int16_t *p = RT_NULL;
    rt_lsmd_t *lsmd = (rt_lsmd_t*)dev;
    
    switch(pos)
    {
        case RT_SENSOR_POS_ACC:
            lsmd_read(dev, LSM6DSL_ACC_GYRO_OUTX_L_XL, lsmd->rx_acc_dma_buf, 6);
            p = (int16_t*)&lsmd->rx_acc_dma_buf[0];
        
            _process_for(p, lsmd->for_setting);
        
            buf[0] = lsmd->acc_ratio * (float)p[0];
            buf[1] = lsmd->acc_ratio * (float)p[1];
            buf[2] = lsmd->acc_ratio * (float)p[2];
            break;
        case RT_SENSOR_POS_GYR:
            lsmd_read(dev, LSM6DSL_ACC_GYRO_OUTX_L_G, lsmd->rx_gyr_dma_buf, 6);
            p = (int16_t*)&lsmd->rx_gyr_dma_buf[0];
        
            _process_for(p, lsmd->for_setting);
        
            buf[0] = lsmd->gyr_ratio * (float)p[0];
            buf[1] = lsmd->gyr_ratio * (float)p[1];
            buf[2] = lsmd->gyr_ratio * (float)p[2];
            break;
        default:
            break;
    }
    return size;
}

static rt_size_t rt_lsmd_write(rt_device_t dev, rt_off_t pos, const void* buffer, rt_size_t size)
{
    rt_lsmd_t *lsmd = (rt_lsmd_t*)dev;
    uint8_t *val = (uint8_t *)buffer;
    
    switch(pos)
    {
        case RT_SENSOR_POS_ACC_BW:
            /* TBD */
            break;
        case RT_SENSOR_POS_ACC_RG:
            lsmd_write_register(dev, LSM6DSL_ACC_GYRO_CTRL1_XL, (acc_rg_sel[*val].reg_val<<2) | (6<<4));
            lsmd->acc_ratio = acc_rg_sel[*val].ratio;
            break;
        case RT_SENSOR_POS_GYR_BW:
            /* TBD */
            break;
        case RT_SENSOR_ENTER_LOW_POWER:
            /* enable wake up fun */
            lsmd_write_register(dev, LSM6DSL_ACC_GYRO_WAKE_UP_THS, (4<<0)); /* wake up thr */
            lsmd_write_register(dev, LSM6DSL_ACC_GYRO_TAP_CFG1, (3<<5) | (1<<7)); /* enable wakup int, set thr, enter low power */
            lsmd_write_register(dev, LSM6DSL_ACC_GYRO_MD1_CFG, 1<<5); /* enable wake up int */
            
            rt_pin_mode(lsmd->irq, PIN_MODE_INPUT_PULLDOWN);
            rt_pin_attach_irq(lsmd->irq, PIN_IRQ_MODE_RISING, lsdm_pin_cb, RT_NULL);
            rt_pin_irq_enable(lsmd->irq, 1);
            break;
        case RT_SENSOR_POS_GYR_RG:
            if(*val == 4)
            {
                lsmd_write_register(dev, LSM6DSL_ACC_GYRO_CTRL2_G,  (1<<0) | (6<<4)); /* 4000DPS, 416Hz */
            }
            else
            {
                lsmd_write_register(dev, LSM6DSL_ACC_GYRO_CTRL2_G,  (lsdm_gyr_rg_sel[*val].reg_val<<2) | (6<<4));
            }
            lsmd->gyr_ratio = lsdm_gyr_rg_sel[*val].ratio;
            break;
        case RT_SENSOR_POS_FOR:
            lsmd->for_setting = val[0];
            break;
    }
    return size;
}



int rt_hw_lsmd_init(const char *name, const char *spid_name, uint32_t irq)
{
    rt_lsmd_t *lsmd = rt_malloc(sizeof(rt_lsmd_t));
    if(!lsmd)
    {
        return RT_ERROR;
    }
    lsmd->spid = (struct rt_spi_device *)rt_device_find(spid_name);
    if(!lsmd->spid)
    {
        return RT_ERROR;
    }
    
	lsmd->parent.type               = RT_Device_Class_Miscellaneous;
	lsmd->parent.rx_indicate        = RT_NULL;
	lsmd->parent.tx_complete        = RT_NULL;
	lsmd->parent.init               = RT_NULL;
	lsmd->parent.open               = rt_lsmd_open;
	lsmd->parent.close              = RT_NULL;
	lsmd->parent.read               = rt_lsmd_read;
	lsmd->parent.write              = rt_lsmd_write;
	lsmd->parent.control            = RT_NULL;
	lsmd->parent.user_data          = &lsmd;
 
    lsmd->irq = irq;
    lsmd->gyr_fifo_read_size = 1;
    lsmd->acc_fifo_read_size = 5;
    lsmd->for_setting = 0;
    
    return rt_device_register(&lsmd->parent, name, RT_DEVICE_FLAG_RDWR);
}



