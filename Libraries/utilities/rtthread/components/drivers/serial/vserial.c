#include <rtdevice.h>
#include <stdint.h>

#define RB_SIZE (32)


typedef struct
{
    struct rt_device       dev;
    struct rt_ringbuffer   *rb;
    rt_device_t            dev_src;
}vserial_t;



static rt_size_t vserial_read(rt_device_t dev, rt_off_t pos, void *buffer, rt_size_t size)
{
    vserial_t *vserial = (vserial_t *)dev->user_data;
    return rt_ringbuffer_getchar(vserial->rb, (rt_uint8_t *)buffer);
}

static rt_size_t vserial_write(rt_device_t dev, rt_off_t pos, const void *buffer, rt_size_t size)
{
    int len = 0;
    vserial_t *vserial = (vserial_t*)dev->user_data;
    uint16_t oflag = vserial->dev_src->open_flag;

    vserial->dev_src->open_flag &= ~RT_DEVICE_FLAG_DMA_TX;
    len = rt_device_write(vserial->dev_src, 0, buffer, size);
    vserial->dev_src->open_flag = oflag;
    return len;
}

void rt_hw_vserial_rx_isr(rt_device_t dev, uint8_t *buf, uint32_t size)
{
    vserial_t *vserial = (vserial_t*)dev->user_data;
    rt_ringbuffer_put(vserial->rb, buf, size);
    if(vserial->dev.rx_indicate) vserial->dev.rx_indicate(&vserial->dev, size);
}

int rt_hw_vserial_init(const char *name, const char *src_name)
{
    vserial_t *vserial = rt_malloc(sizeof(vserial_t));
    
    RT_ASSERT(vserial != RT_NULL);
    
    rt_memset(vserial, 0, sizeof(vserial_t));
    
    vserial->rb = rt_ringbuffer_create(RB_SIZE);
    vserial->dev.type = RT_Device_Class_Char;
    vserial->dev.read = vserial_read;
    vserial->dev.write = vserial_write;
    vserial->dev.user_data = vserial;
    vserial->dev_src = rt_device_find(src_name);
    
    RT_ASSERT(vserial->dev_src != RT_NULL);
    
    return rt_device_register(&vserial->dev, name, RT_DEVICE_FLAG_RDWR);
}
