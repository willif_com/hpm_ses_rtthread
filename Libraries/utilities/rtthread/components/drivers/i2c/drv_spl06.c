
#include <rtthread.h>
#include <stdio.h>
#include <rtdevice.h>
#include <math.h>
#include "common.h"
#include "drivers/rt_sensor.h"


/* 
    https://github.com/DimianZhan/spl06

*/

#define BSPL06_DEBUG		0
#if ( BSPL06_DEBUG == 1 )
#include <stdio.h>
#define BSPL06_TRACE	printf
#else
#define BSPL06_TRACE(...)
#endif



#define SPL_CHIP_ADDR               (0x77)

#define SPL_PRODUCT_ID_ADDR         0x0D
#define SPL_PRS_CFG_ADDR            0x06
#define SPL_TMP_CFG_ADDR            0x07
#define SPL_CFG_REG_ADDR            0x09
#define SPL_MEAS_CFG_ADDR           0x08
#define SPL_RESET_ADDR              0x0C
#define SPL_TMP_B2_ADDR             0x03
#define SPL_PRS_B2_ADDR             0x00
#define SPL_COEF_START_ADDR         (0x10)


typedef struct
{	
    int16_t c0, c1;
    int32_t c00, c10;
    int16_t c01, c11, c20, c21, c30;
}coe_t;


typedef struct 
{	
    struct rt_device            parent;
    struct rt_i2c_bus_device    *bus;
    uint8_t                     chip_addr;
    uint8_t                     chip_id;
    coe_t                       coe;
    int32_t                      kp;    
    int32_t                     kt;
    int32_t                     press_raw;
    int32_t                     temp_raw;
    float                       temperature;
    float                       pressure;
    float                       altitude;
}spl06_t;

static void _spl_dump_coe(coe_t *coe)
{
    BSPL06_TRACE("c0:%d\r\n", coe->c0);
    BSPL06_TRACE("c1:%d\r\n", coe->c1);
    BSPL06_TRACE("c00:%d\r\n", coe->c00);
    BSPL06_TRACE("c10:%d\r\n", coe->c10);
    BSPL06_TRACE("c01:%d\r\n", coe->c01);
    BSPL06_TRACE("c11:%d\r\n", coe->c11);
    BSPL06_TRACE("c20:%d\r\n", coe->c20);
    BSPL06_TRACE("c21:%d\r\n", coe->c21);
    BSPL06_TRACE("c30:%d\r\n", coe->c30);
}

static uint32_t spl_write_reg(spl06_t *dev, uint8_t reg_addr, uint8_t val)
{
    uint8_t buf[2];
    
    buf[0] = reg_addr;
    buf[1] = val;
    return rt_i2c_master_send(dev->bus, dev->chip_addr, 0, buf, sizeof(buf));
}

static uint8_t spl_read_reg(spl06_t *dev, uint8_t reg_addr)
{
    uint8_t val;
    
    rt_i2c_master_send(dev->bus, dev->chip_addr, 0, &reg_addr, 1);
    rt_i2c_master_recv(dev->bus, dev->chip_addr, 0, &val, 1);
    return val;
}



void spl0601_rateset(spl06_t *dev, uint8_t iSensor, uint8_t u8SmplRate, uint8_t u8OverSmpl)
{
    uint8_t reg = 0;
    int32_t i32kpkt = 0;
    switch(u8SmplRate)
    {
        case 2:
            reg |= (1<<5);
            break;
        case 4:
            reg |= (2<<5);
            break;
        case 8:
            reg |= (3<<5);
            break;
        case 16:
            reg |= (4<<5);
            break;
        case 32:
            reg |= (5<<5);
            break;
        case 64:
            reg |= (6<<5);
            break;
        case 128:
            reg |= (7<<5);
            break;
        case 1:
        default:
            break;
    }
    switch(u8OverSmpl)
    {
        case 2:
            reg |= 1;
            i32kpkt = 1572864;
            break;
        case 4:
            reg |= 2;
            i32kpkt = 3670016;
            break;
        case 8:
            reg |= 3;
            i32kpkt = 7864320;
            break;
        case 16:
            i32kpkt = 253952;
            reg |= 4;
            break;
        case 32:
            i32kpkt = 516096;
            reg |= 5;
            break;
        case 64:
            i32kpkt = 1040384;
            reg |= 6;
            break;
        case 128:
            i32kpkt = 2088960;
            reg |= 7;
            break;
        case 1:
        default:
            i32kpkt = 524288;
            break;
    }

    if(iSensor == 0)
    {
        dev->kp = i32kpkt;
        spl_write_reg(dev, SPL_PRS_CFG_ADDR, reg);
        if(u8OverSmpl > 8)
        {
            reg = spl_read_reg(dev, SPL_CFG_REG_ADDR);
            spl_write_reg(dev, SPL_CFG_REG_ADDR, reg | 0x04);
        }
    }
    if(iSensor == 1)
    {
        dev->kt = i32kpkt;
        spl_write_reg(dev, SPL_TMP_CFG_ADDR, reg | 0x80);  //Using mems temperature
        if(u8OverSmpl > 8)
        {
            reg = spl_read_reg(dev, SPL_CFG_REG_ADDR);
            spl_write_reg(dev, SPL_CFG_REG_ADDR, reg | 0x08);
        }
    }
}



uint32_t spl_get_prs_tem(spl06_t *dev, float *prs, float *tmp)
{
    uint8_t buf[6];
    int32_t raw_temp, raw_prs;
    
    uint8_t reg_addr = SPL_PRS_B2_ADDR;
    
    rt_i2c_master_send(dev->bus, dev->chip_addr, 0, &reg_addr, 1);
    rt_i2c_master_recv(dev->bus, dev->chip_addr, 0, buf, sizeof(buf));

    
    raw_temp = ((int32_t)buf[3]<<16) | ((int32_t)buf[4]<<8) | ((int32_t)buf[5]);
    raw_temp = (raw_temp & 0x800000) ? (0xFF000000 | raw_temp) : raw_temp;
    dev->temp_raw = raw_temp;
    
    raw_prs = ((int32_t)buf[0]<<16) | ((int32_t)buf[1]<<8) | ((int32_t)buf[2]);
    raw_prs = (raw_prs & 0x800000) ? (0xFF000000 | raw_prs) : raw_prs;
    dev->press_raw = raw_prs;
    
    // calculate
    float ftsc = (float)dev->temp_raw / dev->kt;
    float fpsc = (float)dev->press_raw / dev->kp;
    float qua2 = (float)dev->coe.c10 + fpsc * ((float)dev->coe.c20 + fpsc * (float)dev->coe.c30);
    float qua3 = ftsc * fpsc * ((float)dev->coe.c11 + fpsc * (float)dev->coe.c21);
            

    float fp = (float)dev->coe.c00 + fpsc * qua2 + ftsc * (float)dev->coe.c01 + qua3;

    // altitude calculations based on http://www.kansasflyer.org/index.asp?nav=Avi&sec=Alti&tab=Theory&pg=1

    // tropospheric properties (0-11km) for standard atmosphere
    const float T1 = 15.0f + 273.15f;       // temperature at base height in Kelvin
    const float a  = -6.5f / 1000.0f;       // temperature gradient in degrees per metre
    const float g  = 9.80665f;              // gravity constant in m/s/s
    const float R  = 287.05f;               // ideal gas constant in J/kg/K
    const float msl_pressure = 101325.0f;   // in Pa
    
    float pK = fp / msl_pressure;
    dev->temperature = (float)dev->coe.c0 * 0.5f + (float)dev->coe.c1 * ftsc;
    dev->pressure = fp;
    dev->altitude = (((powf(pK, (-(a * R) / g))) * T1) - T1) / a;
            
    *prs = dev->pressure;
    *tmp = dev->temperature;
    return 0;
}


//static float prs2altitude(float prs)
//{
//    return (1.0 - pow(prs / 101325, 0.190284)) * 287.15 / 0.0065;
//}


void spl0601_get_calib_param(spl06_t *dev)
{
    uint8_t buf[18] = { 0 };

    uint8_t reg_addr = SPL_COEF_START_ADDR;
    rt_i2c_master_send(dev->bus, dev->chip_addr, 0, &reg_addr, 1);
    rt_i2c_master_recv(dev->bus, dev->chip_addr, 0, buf, sizeof(buf));

     
    dev->coe.c0 = (uint16_t)buf[0] << 4 | (uint16_t)buf[1] >> 4;
    dev->coe.c0 = (dev->coe.c0 & 1 << 11) ? (0xf000 | dev->coe.c0) : dev->coe.c0;

    dev->coe.c1 = (uint16_t)(buf[1] & 0x0f) << 8 | (uint16_t)buf[2];
    dev->coe.c1 = (dev->coe.c1 & 1 << 11) ? (0xf000 | dev->coe.c1) : dev->coe.c1;

    dev->coe.c00 = (uint32_t)buf[3] << 12 | (uint32_t)buf[4] << 4 | (uint16_t)buf[5] >> 4;
    dev->coe.c00 = (dev->coe.c00 & 1 << 19) ? (0xfff00000 | dev->coe.c00) : dev->coe.c00;

    dev->coe.c10 = (uint32_t)(buf[5] & 0x0f) << 16 | (uint32_t)buf[6] << 8 | (uint32_t)buf[7];
    dev->coe.c10 = (dev->coe.c10 & 1 << 19) ? (0xfff00000 | dev->coe.c10) : dev->coe.c10;

    dev->coe.c01 = (uint16_t)buf[8] << 8 | buf[9];
    dev->coe.c11 = (uint16_t)buf[10] << 8 | buf[11];
    dev->coe.c20 = (uint16_t)buf[12] << 8 | buf[13];
    dev->coe.c21 = (uint16_t)buf[14] << 8 | buf[15];
    dev->coe.c30 = (uint16_t)buf[16] << 8 | buf[17];
    
}

static rt_err_t rt_spl06_open(rt_device_t dev, rt_uint16_t oflag)
{
    uint8_t val = 0;
    int ret;

    spl06_t *spl06_dev = (spl06_t *)dev;
    
    val = spl_read_reg(spl06_dev, SPL_PRODUCT_ID_ADDR);
    

    if((val & 0xF0) == 0x10)
    {
        spl06_dev->chip_id = val;
        BSPL06_TRACE("SPL06 found addr:0x%X id:0x%X\r\n", dev->chip_addr, dev->chip_id);
        
//        for(i=0; i<0x29; i++)
//        {
//            val = spl_read_reg(spl06_dev, i);
//            BSPL06_TRACE("0x%02X: 0x%02X\r\n", i, val);
//        }
        
        spl0601_get_calib_param(spl06_dev);
        _spl_dump_coe(&spl06_dev->coe);

        spl0601_rateset(spl06_dev, 0, 64, 32);
        spl0601_rateset(spl06_dev, 1, 32, 8);
        
        spl_write_reg(spl06_dev, SPL_CFG_REG_ADDR, (1<<2) | (1<<3));
        spl_write_reg(spl06_dev, SPL_MEAS_CFG_ADDR, 0x07);              /* continue P and T */
        
        ret = RT_EOK;
    }
    else
    {
        ret = RT_EIO;
    }

    return ret;
}

static rt_size_t rt_spl06_read(rt_device_t dev, rt_off_t pos, void* buffer, rt_size_t size)
{
    float t, p;
    spl06_t *spl06_dev = (spl06_t *)dev;
    spl_get_prs_tem(spl06_dev, &p, &t);
    
    //p = prs2altitude(p);
        
    rt_memcpy(buffer, (uint8_t*)&p, sizeof(p));
    return size;
}

int rt_hw_spl06_init(const char *bus_name, const char *name)
{
    spl06_t *spl06 = rt_malloc(sizeof(spl06_t));
    struct rt_i2c_bus_device *bus = rt_i2c_bus_device_find(bus_name);
    if (bus == RT_NULL)
    {
        return RT_ENOSYS;
    }
    
	spl06->parent.type               = RT_Device_Class_Miscellaneous;
	spl06->parent.rx_indicate        = RT_NULL;
	spl06->parent.tx_complete        = RT_NULL;
	spl06->parent.init               = rt_spl06_init;
	spl06->parent.open               = rt_spl06_open;
	spl06->parent.close              = RT_NULL;
	spl06->parent.read               = rt_spl06_read;
	spl06->parent.write              = RT_NULL;
	spl06->parent.user_data          = RT_NULL;
 
    spl06->bus = bus;
    spl06->chip_addr = SPL_CHIP_ADDR;
    
    rt_device_register(&spl06->parent, name, RT_DEVICE_FLAG_RDWR);
    return RT_EOK;
}





