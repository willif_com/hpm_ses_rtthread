#include "rtthread.h"
#include <rtdevice.h>


#define CW2015_CHIP_ADDR       (0xC4>>1)


#define REG_VERSION     0x0
#define REG_VCELL       0x2
#define REG_SOC         0x4
#define REG_RRT_ALERT   0x6
#define REG_CONFIG      0x8
#define REG_MODE        0xA
#define REG_BATINFO     0x10


typedef struct 
{
    struct rt_device            parent;
    struct rt_i2c_bus_device    *bus;
    uint8_t                     chip_addr;
}cw2015_t;

static rt_err_t rt_cw2015_init(rt_device_t dev)
{    
    return RT_EOK;
}


static uint8_t cw2015_read_reg(cw2015_t *dev, uint8_t reg_addr)
{
    uint8_t val;
    
    rt_i2c_master_send(dev->bus, dev->chip_addr, 0, &reg_addr, 1);
    rt_i2c_master_recv(dev->bus, dev->chip_addr, 0, &val, 1);
    return val;
}

static uint32_t cw2015_write_reg(cw2015_t *dev, uint8_t reg_addr, uint8_t val)
{
    uint8_t buf[2];
    
    buf[0] = reg_addr;
    buf[1] = val;
    return rt_i2c_master_send(dev->bus, dev->chip_addr, 0, buf, sizeof(buf));
}

static uint8_t cw2015_read(cw2015_t *dev, uint8_t reg_addr, uint8_t *buf, uint32_t len)
{
    rt_i2c_master_send(dev->bus, dev->chip_addr, 0, &reg_addr, 1);
    return rt_i2c_master_recv(dev->bus, dev->chip_addr, 0, buf, len);
}




int vcell(cw2015_t *dev)
{
      long int date=0;         
      float prt_date=0.0;    

     date = cw2015_read_reg(dev, 0x02);
     date <<=8;
     date +=cw2015_read_reg(dev, 0x03);
	
     prt_date = date*0.305/1000;	

 //    printf("Vcell is %.4f V\r\n ",prt_date);

}	


	
int soc(cw2015_t *dev)
{
    int zhengshu=0;   // Soc  ge wei
	int xiaoshu=0;    // Soc  xiaoshu 
	int ptr_num=0;
	
	zhengshu=cw2015_read_reg(dev, 0x04);
	ptr_num=cw2015_read_reg(dev, 0x05);
        	 
	if(ptr_num&0x80) xiaoshu +=500;
    if(ptr_num&0x40) xiaoshu +=250;
	if(ptr_num&0x20) xiaoshu +=125;
	if(ptr_num&0x10) xiaoshu +=62;
	if(ptr_num&0x08) xiaoshu +=31;
	if(ptr_num&0x04) xiaoshu +=15;
    if(ptr_num&0x02) xiaoshu +=7;
	if(ptr_num&0x01) xiaoshu +=3;        
	
	xiaoshu /=10;

	//printf("Current electricity is %d.%d%%\r\n",zhengshu,xiaoshu);	
}

static rt_err_t rt_cw2015_open(rt_device_t dev, rt_uint16_t oflag)
{
    uint8_t val = 0x00;
    
    cw2015_t *cw2015 = (cw2015_t *)dev;
    
    val = cw2015_write_reg(cw2015, REG_MODE, 0x00);
    
    val = cw2015_read_reg(cw2015, REG_VERSION);
    printf("[0x%X]:0x%X\r\n", REG_VERSION, val);
    
    val = cw2015_read_reg(cw2015, REG_CONFIG);
    printf("[0x%X]:0x%X\r\n", REG_CONFIG, val);
    
    val = cw2015_read_reg(cw2015, REG_MODE);
    printf("[0x%X]:0x%X\r\n", REG_MODE, val);
    return RT_EOK;
    
}

static rt_size_t rt_cw2015_read(rt_device_t dev, rt_off_t pos, void* buffer, rt_size_t size)
{
    cw2015_t *cw2015 = (cw2015_t *)dev;
    
    cw2015_write_reg(cw2015, REG_MODE, 0x00);
    vcell(cw2015);
    soc(cw2015);
    return size;
}

int rt_hw_cw2015_init(const char *bus_name, const char *name)
{
    static cw2015_t cw2015;
    struct rt_i2c_bus_device *bus;

    bus = rt_i2c_bus_device_find(bus_name);
    if (bus == RT_NULL)
    {
        return RT_ENOSYS;
    }
    
    cw2015.parent.type               = RT_Device_Class_Miscellaneous;
	cw2015.parent.rx_indicate        = RT_NULL;
	cw2015.parent.tx_complete        = RT_NULL;
	cw2015.parent.init               = rt_cw2015_init;
	cw2015.parent.open               = rt_cw2015_open;
	cw2015.parent.close              = RT_NULL;
	cw2015.parent.read               = rt_cw2015_read;
	cw2015.parent.write              = RT_NULL;
	cw2015.parent.user_data          = RT_NULL;
    
    cw2015.bus = bus;
    cw2015.chip_addr = CW2015_CHIP_ADDR;

    rt_device_register(&cw2015.parent, name, RT_DEVICE_FLAG_RDWR);
    return RT_EOK;
}
