import sys
import os
import binascii
import intelhex
import itertools
import struct
from array import array
from intelhex import bin2hex
    
    
    
MARKER_VAL = 0XFFEB6B6 
HDR_MAGIC_VAL = 0xFEEDA5A5

# crc field offset inside header
HDR_CRC_OFS = 16

# len field offset inside header
HDR_LEN_OFS = 12

# image start field offset inside header
HDR_IMG_START_OFS = 20

# header len
HDR_LEN = (4*6)


def ranges(i):
    for _, b in itertools.groupby(enumerate(i), lambda x_y: x_y[1] - x_y[0]):
        b = list(b)
        yield b[0][1], b[-1][1]


def main():
    file_path = str(sys.argv[1]);
    
    bin_file_path = os.path.splitext(file_path)[0] + ".bin"
    #print('input_hex_path:' +  file_path + '\r\noutput_bin_path:' + bin_file_path + '\r\n');

    
    new_hex_file = intelhex.IntelHex()
    new_hex_file.padding = 0xFF
    new_hex_file.fromfile(file_path, format='hex')

    # Get the starting and ending address
    addresses = new_hex_file.addresses()
    addresses.sort()
    start_end_pairs = list(ranges(addresses))
    regions = len(start_end_pairs)
    assert regions == 1, ("Error - only 1 region allowed in "
                          "hex file %i found." % regions)
    start, end = start_end_pairs[0]
    size = end - start + 1

    print("{:<10s} 0x{:X}".format("START:", start))
    print("{:<10s} 0x{:X}".format("END:", end))
    print("{:<10s} {:d}({:.2f}KB)".format("LEGTH:", size, size/1024))
    
    # get binrary
    data = new_hex_file.tobinarray(start=start, size=size)
    data = bytearray(data)
    
    #print(data.find(b'\xb6\xb6\xfe\x0f'))
    marker_addr = data.find(MARKER_VAL.to_bytes(4, byteorder='little'))
    
    if marker_addr == -1:
        print('MARKER NOT FOUND!!')
        return marker_addr
        
    print(f"Pattern {MARKER_VAL:#0X} found at position {marker_addr:#0X}.")
    MARKER = struct.unpack('<I', data[marker_addr:marker_addr+4])
    MARKER = MARKER[0]

    if MARKER == MARKER_VAL:
        header_addr = struct.unpack('<I', data[marker_addr+4:marker_addr+8])
        header_addr = header_addr[0]
        #print('MARKER:' + '%#X'%MARKER_VAL +  ' found, head_addr:' + '%#X'%header_addr)
        header_ofs = header_addr - start
        header_data = struct.unpack('<6I', data[header_ofs:header_ofs+HDR_LEN])
        
        # header magic found
        if header_data[0] == HDR_MAGIC_VAL:
            print('image header magic:' + '%#X'%header_data[0] +  ' found')
            header_data = list(header_data)
            header_data[3] = size;
            header_data[5] = start;
            
            # write size and start info to image file 
            data[header_ofs+HDR_LEN_OFS:header_ofs+HDR_LEN_OFS+4] = size.to_bytes(4, byteorder="little", signed=False)
            data[header_ofs+HDR_IMG_START_OFS:header_ofs+HDR_IMG_START_OFS+4] = start.to_bytes(4, byteorder="little", signed=False)
            
            crc_content = data.copy();
            del crc_content[header_ofs+HDR_CRC_OFS:header_ofs+HDR_CRC_OFS+4]
            crc = binascii.crc32(crc_content)
            print("CRC:" , '%#X'%crc)
            header_data[4] = crc;
            #print(header_data)
            
            # convert updated header data to bytes 
            bytes = struct.pack('<' + 'I'*len(header_data), *header_data)
            
            # update the header content
            data[header_ofs:header_ofs + HDR_LEN] = bytes

            # write to .bin
            with open(bin_file_path, 'wb') as f:
                f.write(data)
            
            # write to .hex
            bin2hex(bin_file_path, file_path, offset=start)
            print(f"BOOT INFO(OFFSET:{marker_addr:#0X}) SUCCESSFULLY WRITTEN! ")
        else:
            print('IHDR NOT FOUND!!!')
    else:
        print('MARKER NOT FOUND!!!')

    print('SCRIPT COMPLETE')

if __name__ == '__main__':
    main()
