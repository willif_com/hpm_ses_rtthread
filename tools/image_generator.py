import sys
import os
import binascii

len_addr = 0x14C
len_size = 4

crc_addr = 0x150
crc_size = 4


in_file_path='dsbl_app.bin'
out_file_path = './dsbl_app_crc.bin'





fp = open(in_file_path, 'rb')
f_size = os.path.getsize(in_file_path)

print('input file:', in_file_path);
print('file_size:', f_size);
print('write to', out_file_path)

data = fp.read() 
data = bytearray(data)

# set len filed
data[len_addr:len_addr+len_size] =(f_size - 4).to_bytes(4, byteorder="little", signed=False)

# get crc
crc_content = data.copy();
del crc_content[crc_addr:crc_addr+crc_size]


crc = binascii.crc32(crc_content)
print("Binary CRC is " , '%#x'%crc)

# set CRC
data[crc_addr:crc_addr+crc_size] = crc.to_bytes(4, byteorder="little", signed=False)


with open(out_file_path, 'wb') as f:
    f.write(data)

