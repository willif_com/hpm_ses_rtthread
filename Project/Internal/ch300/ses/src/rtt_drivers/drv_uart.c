/*
 * Copyright (c) 2021 hpmicro
 *
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Change Logs:
 * Date         Author      Notes
 * 2021-09-19   HPMICRO     First version
 *
 */
#include "drv_uart.h"
#include "board.h"
#include "hpm_sysctl_drv.h"
#include "hpm_uart_drv.h"
#include <rtdbg.h>
#include <rtdevice.h>
#include <rtthread.h>

#ifdef RT_USING_SERIAL

#define BSP_USING_UART0
#define BSP_USING_UART3
//#define BSP_USING_UART5
#define BSP_USING_UART8
#define BSP_USING_UART10
#define BSP_USING_UART11

enum
{
#ifdef BSP_USING_UART0
    UART0_INDEX,
#endif
#ifdef BSP_USING_UART3
    UART3_INDEX,
#endif
#ifdef BSP_USING_UART5
    UART5_INDEX,
#endif
#ifdef BSP_USING_UART8
    UART8_INDEX,
#endif
#ifdef BSP_USING_UART10
    UART10_INDEX,
#endif
#ifdef BSP_USING_UART11
    UART11_INDEX,
#endif
};

typedef struct
{
    UART_Type *UARTx;
    DMA_Type  *DMAx;
    clock_name_t uart_clock;
    uint32_t baud;
    uint32_t irq_uart;
    uint32_t irq_dma;

    GPTMR_Type *GPTMRx;
    clock_name_t gptmr_clock;
    uint8_t gptmr_irq;
    uint8_t cmp_ch; /* use for idleline timeout */
    uint8_t cap_ch;

    uint8_t dmamux_rx_src;
    uint8_t dmamux_tx_src;
    uint8_t dma_rx_chl;
    uint8_t dma_tx_chl;

    TRGM_Type *TRGMx;
    uint8_t trgm_input_src;          /* Corresponding to Pin setting */
    uint8_t trgm_output_gptmr_in;    /* Corresponding to GPTMR cap inst */
    uint8_t trgm_output_gptmr_synci; /* Corresponding to GPTMR cmp inst */

    const char *name;
} hpm_uart_resource_t;

/* hpm uart dirver class */
struct hpm_uart
{
    uint8_t uart_rx_dma_buf[RT_SERIAL_RB_BUFSZ];
    uint8_t uart_tx_dma_buf[RT_SERIAL_RB_BUFSZ];
    hpm_uart_resource_t *resource;
    struct rt_serial_device serial;
};

hpm_uart_resource_t hpm_uart_resource[] =
    {
#ifdef BSP_USING_UART0
        {
            .UARTx = HPM_UART0,
            .DMAx = HPM_XDMA,
            .uart_clock = clock_uart0,
            .baud = 115200,
            .irq_uart = IRQn_UART0,
            .irq_dma = IRQn_XDMA,

            .GPTMRx = HPM_GPTMR0,
            .gptmr_clock = clock_gptmr0,
            .gptmr_irq = IRQn_GPTMR0,
            .cap_ch = GPTMR_CHANNEL_CH2,
            .cmp_ch = GPTMR_CHANNEL_CH0,
            .dma_rx_chl = DMAMUX_MUXCFG_XDMA_MUX0,
            .dma_tx_chl = DMAMUX_MUXCFG_XDMA_MUX1,
            .dmamux_rx_src = HPM_DMA_SRC_UART0_RX,
            .dmamux_tx_src = HPM_DMA_SRC_UART0_TX,

            .TRGMx = HPM_TRGM0,
            .trgm_input_src = HPM_TRGM0_INPUT_SRC_TRGM0_P8,
            .trgm_output_gptmr_in = HPM_TRGM0_OUTPUT_SRC_GPTMR0_IN2,
            .trgm_output_gptmr_synci = HPM_TRGM0_OUTPUT_SRC_GPTMR0_SYNCI,
            .name = "uart0",
        },
#endif
#ifdef BSP_USING_UART3
        {
            .UARTx = HPM_UART3,
            .DMAx = HPM_XDMA,
            .uart_clock = clock_uart3,
            .baud = 921600,
            .irq_uart = IRQn_UART3,
            .irq_dma = IRQn_XDMA,

            .GPTMRx = HPM_GPTMR4,
            .gptmr_clock = clock_gptmr4,
            .gptmr_irq = IRQn_GPTMR4,
            .cap_ch = GPTMR_CHANNEL_CH2,
            .cmp_ch = GPTMR_CHANNEL_CH0,
            .dma_rx_chl = DMAMUX_MUXCFG_XDMA_MUX2,
            .dma_tx_chl = DMAMUX_MUXCFG_XDMA_MUX3, /* do not support Tx DMA */
            .dmamux_rx_src = HPM_DMA_SRC_UART3_RX,
            .dmamux_tx_src = HPM_DMA_SRC_UART3_TX,

            .TRGMx = HPM_TRGM2,
            .trgm_input_src = HPM_TRGM2_INPUT_SRC_TRGM2_P1,
            .trgm_output_gptmr_in = HPM_TRGM2_OUTPUT_SRC_GPTMR4_IN2,
            .trgm_output_gptmr_synci = HPM_TRGM2_OUTPUT_SRC_GPTMR4_SYNCI,
            .name = "uart3",
        },
#endif
#ifdef BSP_USING_UART5
        {
            .UARTx = HPM_UART5,
            .DMAx = HPM_XDMA,
            .uart_clock = clock_uart5,
            .baud = 921600,
            .irq_uart = IRQn_UART5,
            .irq_dma = IRQn_XDMA,

            .GPTMRx = HPM_GPTMR5,
            .gptmr_clock = clock_gptmr5,
            .gptmr_irq = IRQn_GPTMR5,
            .cap_ch = GPTMR_CHANNEL_CH2,
            .cmp_ch = GPTMR_CHANNEL_CH0,
            .dma_rx_chl = DMAMUX_MUXCFG_XDMA_MUX4,
            .dma_tx_chl = DMAMUX_MUXCFG_XDMA_MUX5,
            .dmamux_rx_src = HPM_DMA_SRC_UART5_RX,
            .dmamux_tx_src = HPM_DMA_SRC_UART5_TX,

            .TRGMx = HPM_TRGM2,
            .trgm_input_src = HPM_TRGM2_INPUT_SRC_TRGM2_P4,
            .trgm_output_gptmr_in = HPM_TRGM2_OUTPUT_SRC_GPTMR5_IN2,
            .trgm_output_gptmr_synci = HPM_TRGM2_OUTPUT_SRC_GPTMR5_SYNCI,
            .name = "uart5",
        },
#endif
#ifdef BSP_USING_UART8
        {
            .UARTx = HPM_UART8,
            .DMAx = HPM_XDMA,
            .uart_clock = clock_uart8,
            .baud = 115200,
            .irq_uart = IRQn_UART8,
            .irq_dma = IRQn_XDMA,

            .GPTMRx = HPM_GPTMR2,
            .gptmr_clock = clock_gptmr2,
            .gptmr_irq = IRQn_GPTMR2,
            .cap_ch = GPTMR_CHANNEL_CH2,
            .cmp_ch = GPTMR_CHANNEL_CH0,
            .dma_rx_chl = DMAMUX_MUXCFG_XDMA_MUX6,
            .dma_tx_chl = DMAMUX_MUXCFG_XDMA_MUX7,
            .dmamux_rx_src = HPM_DMA_SRC_UART8_RX,
            .dmamux_tx_src = HPM_DMA_SRC_UART8_TX,

            .TRGMx = HPM_TRGM1,
            .trgm_input_src = HPM_TRGM1_INPUT_SRC_TRGM1_P3,
            .trgm_output_gptmr_in = HPM_TRGM1_OUTPUT_SRC_GPTMR2_IN2,
            .trgm_output_gptmr_synci = HPM_TRGM1_OUTPUT_SRC_GPTMR2_SYNCI,
            .name = "uart8",
        },
#endif
#ifdef BSP_USING_UART10
        {
            .UARTx = HPM_UART10,
            .DMAx = HPM_HDMA,
            .uart_clock = clock_uart10,
            .baud = 115200,
            .irq_uart = IRQn_UART10,
            .irq_dma = IRQn_HDMA,

            .GPTMRx = HPM_GPTMR3,
            .gptmr_clock = clock_gptmr3,
            .gptmr_irq = IRQn_GPTMR3,
            .cap_ch = GPTMR_CHANNEL_CH2,
            .cmp_ch = GPTMR_CHANNEL_CH0,
            .dma_rx_chl = DMAMUX_MUXCFG_HDMA_MUX0,
            .dma_tx_chl = DMAMUX_MUXCFG_HDMA_MUX1,
            .dmamux_rx_src = HPM_DMA_SRC_UART10_RX,
            .dmamux_tx_src = HPM_DMA_SRC_UART10_TX,

            .TRGMx = HPM_TRGM1,
            .trgm_input_src = HPM_TRGM1_INPUT_SRC_TRGM1_P0,
            .trgm_output_gptmr_in = HPM_TRGM1_OUTPUT_SRC_GPTMR3_IN2,
            .trgm_output_gptmr_synci = HPM_TRGM1_OUTPUT_SRC_GPTMR3_SYNCI,
            .name = "uart10",
        },
#endif
#ifdef BSP_USING_UART11
        {
            .UARTx = HPM_UART11,
            .DMAx = HPM_HDMA,
            .uart_clock = clock_uart11,
            .baud = 115200,
            .irq_uart = IRQn_UART11,
            .irq_dma = IRQn_HDMA,

            .GPTMRx = HPM_GPTMR1,
            .gptmr_clock = clock_gptmr1,
            .gptmr_irq = IRQn_GPTMR1,
            .cap_ch = GPTMR_CHANNEL_CH2,
            .cmp_ch = GPTMR_CHANNEL_CH0,
            .dma_rx_chl = DMAMUX_MUXCFG_HDMA_MUX2,
            .dma_tx_chl = DMAMUX_MUXCFG_HDMA_MUX3, /* do not support Tx DMA */
            .dmamux_rx_src = HPM_DMA_SRC_UART11_RX,
            .dmamux_tx_src = HPM_DMA_SRC_UART11_TX,

            .TRGMx = HPM_TRGM0,
            .trgm_input_src = HPM_TRGM0_INPUT_SRC_TRGM0_P1,
            .trgm_output_gptmr_in = HPM_TRGM0_OUTPUT_SRC_GPTMR1_IN2,
            .trgm_output_gptmr_synci = HPM_TRGM0_OUTPUT_SRC_GPTMR1_SYNCI,
            .name = "uart11",
        },
#endif
};

static __attribute__((section(".noncacheable"), aligned(8))) struct hpm_uart uart_obj[ARRAY_SIZE(hpm_uart_resource)] = {0};

extern void init_uart_pins(UART_Type *ptr);
static rt_err_t hpm_uart_configure(struct rt_serial_device *serial, struct serial_configure *cfg);
static rt_err_t hpm_uart_control(struct rt_serial_device *serial, int cmd, void *arg);
static int hpm_uart_putc(struct rt_serial_device *serial, char ch);
static int hpm_uart_getc(struct rt_serial_device *serial);

static void uart_isr(struct rt_serial_device *serial)
{
    struct hpm_uart *uart;

    RT_ASSERT(serial != RT_NULL);

    uart = rt_container_of(serial, struct hpm_uart, serial);
    RT_ASSERT(uart != RT_NULL);

    /* enter interrupt */
    rt_interrupt_enter();

    /* UART in mode Receiver */
    rt_hw_serial_isr(serial, RT_SERIAL_EVENT_RX_IND);

    /* leave interrupt */
    rt_interrupt_leave();
}

static void hdma_isr(void)
{
    //printf("hdma_isr\r\n");
    volatile uint32_t INSTATUS = dma_get_irq_status(HPM_HDMA);
    dma_clear_irq_status(HPM_HDMA, INSTATUS);

    if (INSTATUS & (1 << (DMA_STATUS_TC_SHIFT + (uart_obj[UART10_INDEX].resource->dma_tx_chl % DMA_SOC_CHANNEL_NUM))))
        rt_hw_serial_isr(&uart_obj[UART10_INDEX].serial, RT_SERIAL_EVENT_TX_DMADONE);
    if (INSTATUS & (1 << (DMA_STATUS_TC_SHIFT + (uart_obj[UART11_INDEX].resource->dma_tx_chl % DMA_SOC_CHANNEL_NUM))))
        rt_hw_serial_isr(&uart_obj[UART11_INDEX].serial, RT_SERIAL_EVENT_TX_DMADONE);
}

SDK_DECLARE_EXT_ISR_M(IRQn_HDMA, hdma_isr)

static void xdma_isr(void)
{
    //printf("xdma_isr\r\n");
    volatile uint32_t INSTATUS = dma_get_irq_status(HPM_XDMA);
    dma_clear_irq_status(HPM_XDMA, INSTATUS);

    if (INSTATUS & (1 << (DMA_STATUS_TC_SHIFT + (uart_obj[UART0_INDEX].resource->dma_tx_chl % DMA_SOC_CHANNEL_NUM))))
        rt_hw_serial_isr(&uart_obj[UART0_INDEX].serial, RT_SERIAL_EVENT_TX_DMADONE);
    if (INSTATUS & (1 << (DMA_STATUS_TC_SHIFT + (uart_obj[UART3_INDEX].resource->dma_tx_chl % DMA_SOC_CHANNEL_NUM))))
        rt_hw_serial_isr(&uart_obj[UART3_INDEX].serial, RT_SERIAL_EVENT_TX_DMADONE);
    //if (INSTATUS & (1 << (DMA_STATUS_TC_SHIFT + (uart_obj[UART5_INDEX].resource->dma_tx_chl % DMA_SOC_CHANNEL_NUM))))
    //    rt_hw_serial_isr(&uart_obj[UART5_INDEX].serial, RT_SERIAL_EVENT_TX_DMADONE);
    if (INSTATUS & (1 << (DMA_STATUS_TC_SHIFT + (uart_obj[UART8_INDEX].resource->dma_tx_chl % DMA_SOC_CHANNEL_NUM))))
        rt_hw_serial_isr(&uart_obj[UART8_INDEX].serial, RT_SERIAL_EVENT_TX_DMADONE);
}

SDK_DECLARE_EXT_ISR_M(IRQn_XDMA, xdma_isr)

static void uart_rx_dma(struct rt_serial_device *serial)
{
    struct rt_serial_rx_fifo *rx_fifo;
    struct hpm_uart *uart;

    RT_ASSERT(serial != RT_NULL);
    uart = rt_container_of(serial, struct hpm_uart, serial);
    rx_fifo = (struct rt_serial_rx_fifo *)serial->serial_rx;

    dma_handshake_config_t config;
    config.ch_index = uart->resource->dma_rx_chl % DMA_SOC_CHANNEL_NUM;
    config.dst = core_local_mem_to_sys_address(HPM_CORE0, (uint32_t)rx_fifo->buffer);
    config.dst_fixed = false;
    config.src = (uint32_t)&uart->resource->UARTx->RBR;
    config.src_fixed = true;
    config.size_in_byte = serial->config.bufsz;

    dma_setup_handshake(uart->resource->DMAx, &config, true);
    /* disable interrupt and enable chl */
    uart->resource->DMAx->CHCTRL[uart->resource->dma_rx_chl % DMA_SOC_CHANNEL_NUM].CTRL |= 0x0F;

    rx_fifo->get_index = 0;
    rx_fifo->put_index = 0;
    rx_fifo->is_full = RT_FALSE;
}

static void uart_rx_gptmr_isr(struct rt_serial_device *serial)
{
    struct hpm_uart *uart;
    rt_size_t recv_total_index, recv_len;
    rt_enter_critical();
    RT_ASSERT(serial != RT_NULL);
    uart = rt_container_of(serial, struct hpm_uart, serial);

    hpm_uart_resource_t *instance = uart->resource;

    /* cmp: idleline */
    if (gptmr_check_status(instance->GPTMRx, GPTMR_CH_CMP_STAT_MASK(instance->cmp_ch, 0)))
    {
        gptmr_clear_status(instance->GPTMRx, GPTMR_CH_CMP_STAT_MASK(instance->cmp_ch, 0)); /* clear cmp status */
        gptmr_stop_counter(instance->GPTMRx, instance->cmp_ch);                            /* stop counter */
        gptmr_channel_reset_count(instance->GPTMRx, instance->cmp_ch);                     /* clear counter */
        gptmr_disable_irq(instance->GPTMRx, GPTMR_CH_CMP_IRQ_MASK(instance->cmp_ch, 0));   /* disable cmp irq */

        recv_len = serial->config.bufsz - uart->resource->DMAx->CHCTRL[instance->dma_rx_chl % DMA_SOC_CHANNEL_NUM].TRANSIZE;
        if (recv_len)
        {
            uart_rx_dma(serial);
            rt_hw_serial_isr(serial, RT_SERIAL_EVENT_RX_DMADONE | (recv_len << 8));
        }

        gptmr_enable_irq(instance->GPTMRx, GPTMR_CH_CAP_IRQ_MASK(instance->cap_ch));
        gptmr_start_counter(instance->GPTMRx, instance->cap_ch);
    }

    /* capture: start */
    if (gptmr_check_status(instance->GPTMRx, GPTMR_CH_CAP_STAT_MASK(instance->cap_ch)))
    {
        gptmr_clear_status(instance->GPTMRx, GPTMR_CH_CAP_STAT_MASK(instance->cap_ch)); /* clear capture status */
        gptmr_stop_counter(instance->GPTMRx, instance->cap_ch);                         /* stop counter */
        gptmr_channel_reset_count(instance->GPTMRx, instance->cap_ch);                  /* clear counter */
        gptmr_disable_irq(instance->GPTMRx, GPTMR_CH_CAP_IRQ_MASK(instance->cap_ch));   /* disable capture irq */

        /* start gptmr cmp for rx idle detection */
        gptmr_enable_irq(instance->GPTMRx, GPTMR_CH_CMP_IRQ_MASK(instance->cmp_ch, 0));
        gptmr_start_counter(instance->GPTMRx, instance->cmp_ch);
    }
    rt_exit_critical();
}

static void uart_idle_tmr_config(const hpm_uart_resource_t *instance)
{
    board_init_gptmr_clock(instance->GPTMRx);
    /* config_gptmr_to_detect_uart_rx_start(cap) */
    gptmr_channel_config_t gptmr_config = {0};
    gptmr_channel_get_default_config(instance->GPTMRx, &gptmr_config);
    gptmr_config.mode = gptmr_work_mode_capture_at_falling_edge;

    gptmr_channel_config(instance->GPTMRx, instance->cap_ch, &gptmr_config, false);
    gptmr_channel_reset_count(instance->GPTMRx, instance->cap_ch);

    /* configure uart_rx -> TRGM INPUT IO -> GPTMR IN */
    trgm_output_update_source(instance->TRGMx, instance->trgm_output_gptmr_in, instance->trgm_input_src);
    gptmr_enable_irq(instance->GPTMRx, GPTMR_CH_CAP_IRQ_MASK(instance->cap_ch)); /* enable capture irq */
    gptmr_start_counter(instance->GPTMRx, instance->cap_ch);

    /* config_gptmr_to_detect_uart_rx_idle (cmp) */
    uint32_t gptmr_freq, bits;
    bits = 12 * 3; /* use max value: 1 start bit, 8 data bits, 2 stop bits, 1 parity bit */
    gptmr_freq = clock_get_frequency(instance->gptmr_clock);
    gptmr_channel_get_default_config(instance->GPTMRx, &gptmr_config);
    gptmr_config.cmp[0] = gptmr_freq / instance->baud * bits; /* Time to transmit a byte */
    gptmr_config.cmp[1] = 0xffffffff;
    gptmr_config.synci_edge = gptmr_synci_edge_both;
    gptmr_channel_config(instance->GPTMRx, instance->cmp_ch, &gptmr_config, false);
    gptmr_channel_reset_count(instance->GPTMRx, instance->cmp_ch);

    /* config uart rx -> TRGM INPUT IO -> GPTMR SYNCI */
    trgm_output_update_source(instance->TRGMx, instance->trgm_output_gptmr_synci, instance->trgm_input_src);
    gptmr_enable_irq(instance->GPTMRx, GPTMR_CH_CMP_IRQ_MASK(instance->cmp_ch, 0)); /* enable compare irq */
    /* not start gptmr cmp channel counter, start this channel counter when rx reception start */

    intc_m_enable_irq_with_priority(instance->gptmr_irq, 1); /* enable gptmr irq */
}

static void hpm_dma_config(struct rt_serial_device *serial, rt_ubase_t flag)
{
    struct hpm_uart *uart;

    RT_ASSERT(serial != RT_NULL);
    uart = rt_container_of(serial, struct hpm_uart, serial);

    dmamux_config(HPM_DMAMUX, uart->resource->dma_rx_chl, uart->resource->dmamux_rx_src, true);
    uart_rx_dma(serial);

    uart_idle_tmr_config(uart->resource);
}

#if defined(BSP_USING_UART0)
void uart0_isr(void)
{
    /* enter interrupt */
    rt_interrupt_enter();

    uart_isr(&(uart_obj[UART0_INDEX].serial));

    /* leave interrupt */
    rt_interrupt_leave();
}
SDK_DECLARE_EXT_ISR_M(IRQn_UART0, uart0_isr)

void gptmr0_isr(void)
{
    uart_rx_gptmr_isr(&(uart_obj[UART0_INDEX].serial));
}
SDK_DECLARE_EXT_ISR_M(IRQn_GPTMR0, gptmr0_isr)
#endif

#if defined(BSP_USING_UART3)
void uart3_isr(void)
{
    /* enter interrupt */
    rt_interrupt_enter();

    uart_isr(&(uart_obj[UART3_INDEX].serial));

    /* leave interrupt */
    rt_interrupt_leave();
}
SDK_DECLARE_EXT_ISR_M(IRQn_UART3, uart3_isr)

void gptmr4_isr(void)
{
    uart_rx_gptmr_isr(&(uart_obj[UART3_INDEX].serial));
}
SDK_DECLARE_EXT_ISR_M(IRQn_GPTMR4, gptmr4_isr)
#endif

#if defined(BSP_USING_UART5)
void uart5_isr(void)
{
    /* enter interrupt */
    rt_interrupt_enter();

    uart_isr(&(uart_obj[UART5_INDEX].serial));

    /* leave interrupt */
    rt_interrupt_leave();
}
SDK_DECLARE_EXT_ISR_M(IRQn_UART5, uart5_isr)

void gptmr5_isr(void)
{
    uart_rx_gptmr_isr(&(uart_obj[UART5_INDEX].serial));
}
SDK_DECLARE_EXT_ISR_M(IRQn_GPTMR5, gptmr5_isr)
#endif

#if defined(BSP_USING_UART8)
void uart8_isr(void)
{
    /* enter interrupt */
    rt_interrupt_enter();

    uart_isr(&(uart_obj[UART8_INDEX].serial));

    /* leave interrupt */
    rt_interrupt_leave();
}
SDK_DECLARE_EXT_ISR_M(IRQn_UART8, uart8_isr)

void gptmr2_isr(void)
{
    uart_rx_gptmr_isr(&(uart_obj[UART8_INDEX].serial));
}
SDK_DECLARE_EXT_ISR_M(IRQn_GPTMR2, gptmr2_isr)
#endif

#if defined(BSP_USING_UART10)
void uart10_isr(void)
{
    /* enter interrupt */
    rt_interrupt_enter();

    uart_isr(&(uart_obj[UART10_INDEX].serial));

    /* leave interrupt */
    rt_interrupt_leave();
}
SDK_DECLARE_EXT_ISR_M(IRQn_UART10, uart10_isr)

void gptmr3_isr(void)
{
    uart_rx_gptmr_isr(&(uart_obj[UART10_INDEX].serial));
}
SDK_DECLARE_EXT_ISR_M(IRQn_GPTMR3, gptmr3_isr)
#endif

#if defined(BSP_USING_UART11)
void uart11_isr(void)
{
    /* enter interrupt */
    rt_interrupt_enter();

    uart_isr(&(uart_obj[UART11_INDEX].serial));

    /* leave interrupt */
    rt_interrupt_leave();
}
SDK_DECLARE_EXT_ISR_M(IRQn_UART11, uart11_isr)

void gptmr1_isr(void)
{
    uart_rx_gptmr_isr(&(uart_obj[UART11_INDEX].serial));
}
SDK_DECLARE_EXT_ISR_M(IRQn_GPTMR1, gptmr1_isr)
#endif

static void uart_tx_trigger_dma(struct rt_serial_device *serial, rt_uint8_t *buf, rt_size_t size)
{
    struct hpm_uart *uart = rt_container_of(serial, struct hpm_uart, serial);
    dma_handshake_config_t config;

    memcpy(uart->uart_tx_dma_buf, buf, size);
    config.src = (uint32_t)uart->uart_tx_dma_buf;

    config.ch_index = uart->resource->dma_tx_chl % DMA_SOC_CHANNEL_NUM;
    config.dst = (uint32_t)&uart->resource->UARTx->THR;
    config.dst_fixed = true;

    config.src_fixed = false;
    config.size_in_byte = size;
    dma_setup_handshake(uart->resource->DMAx, &config, true);
}

static rt_size_t hpm_dma_transmit(struct rt_serial_device *serial, rt_uint8_t *buf, rt_size_t size, int direction)
{
    int i;
    struct hpm_uart *uart = rt_container_of(serial, struct hpm_uart, serial);
   
    if (direction == RT_SERIAL_DMA_TX)
    {
        if (uart->resource->DMAx->CHCTRL[uart->resource->dma_tx_chl % DMA_SOC_CHANNEL_NUM].TRANSIZE != 0) /* the last transfer is not complete, abort! */
        {
            rt_hw_serial_isr(serial, RT_SERIAL_EVENT_TX_DMADONE);
            return 0;
        }

        dmamux_config(HPM_DMAMUX, uart->resource->dma_tx_chl, uart->resource->dmamux_tx_src, true);
        uart_tx_trigger_dma(serial, buf, size);
     //   rt_hw_serial_isr(serial, RT_SERIAL_EVENT_TX_DMADONE);
    }

    return size;
}

static rt_err_t hpm_uart_configure(struct rt_serial_device *serial, struct serial_configure *cfg)
{
    struct hpm_uart *uart;
    RT_ASSERT(serial != RT_NULL);
    RT_ASSERT(cfg != RT_NULL);

    uart = rt_container_of(serial, struct hpm_uart, serial);

    uart_config_t uart_config;
    uart_default_config(uart->resource->UARTx, &uart_config);
    uart_config.dma_enable = true;
    uart_config.src_freq_in_hz = board_init_uart_clock(uart->resource->UARTx); // clock_get_frequency(clock_uart0);//
    uart_config.baudrate = cfg->baud_rate;
    uart_init(uart->resource->UARTx, &uart_config);
}

static rt_err_t hpm_uart_control(struct rt_serial_device *serial, int cmd, void *arg)
{
    rt_ubase_t ctrl_arg = (rt_ubase_t)arg;
    RT_ASSERT(serial != RT_NULL);
    uint32_t arg_val;
    struct hpm_uart *uart = rt_container_of(serial, struct hpm_uart, serial);

    switch (cmd)
    {
    case RT_DEVICE_CTRL_CLR_INT:
        /* disable rx irq */
        uart_disable_irq(uart->resource->UARTx, uart_intr_rx_data_avail_or_timeout);
        intc_m_disable_irq(uart->resource->irq_uart);
        break;

    case RT_DEVICE_CTRL_SET_INT:
        /* enable rx irq */
        uart_enable_irq(uart->resource->UARTx, uart_intr_rx_data_avail_or_timeout);
        intc_m_enable_irq_with_priority(uart->resource->irq_uart, 1);
        break;
    case RT_DEVICE_CTRL_CONFIG:
        intc_m_enable_irq_with_priority(uart->resource->irq_dma, 1);
        hpm_dma_config(serial, ctrl_arg);

        /* FIXME HPM6750 dcache issue, must use nan-cacheable RAM for DMA operation */
        struct rt_serial_rx_fifo *rx_fifo = (struct rt_serial_rx_fifo *)serial->serial_rx;
        rx_fifo->buffer = (void *)uart->uart_rx_dma_buf;
        break;
    }
    return RT_EOK;
}

static int hpm_uart_putc(struct rt_serial_device *serial, char ch)
{
    struct hpm_uart *uart = rt_container_of(serial, struct hpm_uart, serial);
    uart_send_byte(uart->resource->UARTx, ch);
}

static int hpm_uart_getc(struct rt_serial_device *serial)
{
    int ch = -1;
    RT_ASSERT(serial != RT_NULL);
    struct hpm_uart *uart = rt_container_of(serial, struct hpm_uart, serial);

    if (uart_check_status(uart->resource->UARTx, uart_stat_data_ready))
    {
        uart_receive_byte(uart->resource->UARTx, (uint8_t *)&ch);
    }
    return ch;
}

static const struct rt_uart_ops hpm_uart_ops = {
    hpm_uart_configure,
    hpm_uart_control,
    hpm_uart_putc,
    hpm_uart_getc,
    hpm_dma_transmit,
};

int rt_hw_usart_init(void)
{
    struct serial_configure config = RT_SERIAL_CONFIG_DEFAULT;
    rt_err_t result = 0;

    for (int i = 0; i < ARRAY_SIZE(uart_obj); i++)
    {
        /* init UART object */
        uart_obj[i].resource = &hpm_uart_resource[i];
        uart_obj[i].serial.ops = &hpm_uart_ops;
        config.baud_rate = uart_obj[i].resource->baud;
        uart_obj[i].serial.config = config;

        /* register UART device */
        result = rt_hw_serial_register(&uart_obj[i].serial, uart_obj[i].resource->name, RT_DEVICE_FLAG_RDWR | RT_DEVICE_FLAG_INT_RX | RT_DEVICE_FLAG_DMA_RX | RT_DEVICE_FLAG_DMA_TX, NULL);

        RT_ASSERT(result == RT_EOK);

        init_uart_pins(uart_obj[i].resource->UARTx);
        board_init_uart_clock(uart_obj[i].resource->UARTx);
    }

    return result;
}

#endif /* RT_USING_SERIAL */