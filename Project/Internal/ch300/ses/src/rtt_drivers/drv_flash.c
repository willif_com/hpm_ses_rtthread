#include <rtthread.h>
#include "board.h"
#include "hpm_romapi.h"
#include <rthw.h>

#define FS_BASE_ADDR    (0)

#define FAL_RAMFUNC __attribute__((section(".isr_vector")))

#define FAL_ENTER_CRITICAL()       \
    do                             \
    {                              \
        rt_hw_interrupt_disable(); \
        disable_irq_from_intc();   \
        fencei();                  \
    } while (0)

#define FAL_EXIT_CRITICAL()                                          \
    do                                                               \
    {                                                                \
        ROM_API_TABLE_ROOT->xpi_driver_if->software_reset(HPM_XPI0); \
        fencei();                                                    \
        rt_hw_interrupt_disable();                                   \
        enable_irq_from_intc();                                      \
    } while (0)

typedef struct
{
    xpi_nor_config_t                    xpi_nor_cfg;
    uint32_t                            base_addr;
    struct rt_device_blk_geometry       geo;
} oc_flash_t;

static oc_flash_t ocf;

static rt_err_t oc_flash_init(rt_device_t dev)
{
    return RT_EOK;
}

static rt_err_t oc_flash_open(rt_device_t dev, rt_uint16_t oflag)
{
    uint32_t flash_size=0, sector_size=0, page_size=0;
    xpi_nor_config_option_t nor_option = {0};
    
    /* CH300 */
    extern const uint32_t option[4];
    nor_option.header.U = option[0];
    nor_option.option0.U = option[1];
    nor_option.option1.U = option[2];
    nor_option.option2.U = option[3];

    FAL_ENTER_CRITICAL();
    hpm_stat_t status = rom_xpi_nor_auto_config(HPM_XPI0, &ocf.xpi_nor_cfg, &nor_option);
    FAL_EXIT_CRITICAL();

    rom_xpi_nor_get_property(HPM_XPI0, &ocf.xpi_nor_cfg, xpi_nor_property_total_size, &flash_size);
    rom_xpi_nor_get_property(HPM_XPI0, &ocf.xpi_nor_cfg, xpi_nor_property_sector_size, &sector_size);
    rom_xpi_nor_get_property(HPM_XPI0, &ocf.xpi_nor_cfg, xpi_nor_property_page_size, &page_size);

    ocf.geo.block_size = ocf.geo.bytes_per_sector = sector_size;
    ocf.geo.sector_count = xpi_nor_property_total_size / sector_size;
    ocf.base_addr = FS_BASE_ADDR;
    return RT_EOK;
}

static rt_size_t oc_flash_read(rt_device_t dev, rt_off_t pos, void *buffer, rt_size_t size)
{
    //  printf("%s pos:%d size:%d\r\n", __FUNCTION__, pos, size);
    FAL_ENTER_CRITICAL();
    rom_xpi_nor_read(HPM_XPI0, xpi_xfer_channel_auto, &ocf.xpi_nor_cfg, buffer, pos + ocf.base_addr, size);
    FAL_EXIT_CRITICAL();
    return size;
}

static rt_size_t oc_flash_write(rt_device_t dev, rt_off_t pos, const void *buffer, rt_size_t size)
{
    // printf("%s pos:%d size:%d\r\n", __FUNCTION__, pos, size);
    FAL_ENTER_CRITICAL();
    rom_xpi_nor_program(HPM_XPI0, xpi_xfer_channel_auto, &ocf.xpi_nor_cfg, buffer, pos + ocf.base_addr, size);
    FAL_EXIT_CRITICAL();
    return size;
}

static rt_err_t oc_control(rt_device_t dev, int cmd, void *args)
{
    uint32_t blk = (uint32_t)args;
    uint32_t erase_addr;
    // printf("%s cmd:0x%X block:%d\r\n", __FUNCTION__, cmd, blk);

    if (cmd == RT_DEVICE_CTRL_BLK_GETGEOME)
    {
        struct rt_device_blk_geometry *geo;

        geo = (struct rt_device_blk_geometry *)args;
        if (geo == RT_NULL)
        {
            return -RT_ERROR;
        }

        geo->bytes_per_sector = geo->block_size = ocf.geo.bytes_per_sector;
        geo->sector_count = ocf.geo.sector_count;
    }

    if (cmd == RT_DEVICE_CTRL_BLK_ERASE)
    {
        erase_addr = blk * ocf.geo.block_size + ocf.base_addr;
        FAL_ENTER_CRITICAL();
        rom_xpi_nor_erase(HPM_XPI0, xpi_xfer_channel_auto, &ocf.xpi_nor_cfg, erase_addr, ocf.geo.bytes_per_sector);
        FAL_EXIT_CRITICAL();
    }
    return RT_EOK;
}

int rt_hw_on_chip_flash_init(void)
{
    struct rt_device *dev;

    dev = rt_malloc(sizeof(struct rt_device));
    dev->type = RT_Device_Class_MTD;
    dev->rx_indicate = RT_NULL;
    dev->tx_complete = RT_NULL;
    dev->init = oc_flash_init;
    dev->open = oc_flash_open;
    dev->close = RT_NULL;
    dev->read = oc_flash_read;
    dev->write = oc_flash_write;
    dev->control = oc_control;
    dev->user_data = RT_NULL;

    return rt_device_register(dev, "ocflash", RT_DEVICE_FLAG_RDWR);
}




