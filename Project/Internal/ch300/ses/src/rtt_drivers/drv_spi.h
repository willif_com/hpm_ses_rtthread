/*
 * Copyright (c) 2021 hpmicro
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef DRV_SPI_H
#define DRV_SPI_H

#include <rtthread.h>
#include <rtdevice.h>
#include <rthw.h>
#include <stdint.h>




rt_err_t rt_hw_spi_device_attach(const char *bus_name, const char *device_name, uint8_t cs_pin);

int rt_hw_spi_init(void);



#endif /* DRV_SPI_H */

