#include <rtdevice.h>
#include <stdio.h>
#include <stdint.h>

static uint8_t sda_pin;
static uint8_t scl_pin;

static void at32_set_sda(void *data, rt_int32_t state)
{
    rt_pin_write(sda_pin, state);
}

static void at32_set_scl(void *data, rt_int32_t state)
{
    rt_pin_write(scl_pin, state);
}

static rt_int32_t at32_get_sda(void *data)
{
    return rt_pin_read(sda_pin);
}

static rt_int32_t at32_get_scl(void *data)
{
    return rt_pin_read(scl_pin);
}

static void _udelay(rt_uint32_t us)
{
	rt_int32_t i;
	for (; us > 0; us--)
	{
		i = 3;
		while (i > 0)
		{
			i--;
		}
	}
}


static const struct rt_i2c_bit_ops bit_ops = {
	RT_NULL,
	at32_set_sda,
	at32_set_scl,
	at32_get_sda,
	at32_get_scl,

	_udelay,

	1,
	2
};


int rt_i2c_init(void)
{
	struct rt_i2c_bus_device *bus;

	bus = rt_malloc(sizeof(struct rt_i2c_bus_device));

	rt_memset((void *)bus, 0, sizeof(struct rt_i2c_bus_device));

	bus->priv = (void *)&bit_ops;

	rt_i2c_bit_add_bus(bus, "i2c0");

    sda_pin = 14;
    scl_pin = 15;
    rt_pin_mode(scl_pin, PIN_MODE_OUTPUT_OD);
    rt_pin_mode(sda_pin, PIN_MODE_OUTPUT_OD);
    
	return RT_EOK;
}
