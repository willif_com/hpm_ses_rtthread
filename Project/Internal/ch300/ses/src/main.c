#include "board.h"
#include <rthw.h>
#include <rtthread.h>

#define MCHTMR_FREQ_IN_HZ (1000000)

void board_init(void);

ATTR_PLACE_AT(".framebuffer")
static uint8_t rtt_heap[512 * 1024];

void init_thread_entry(void *parameter);

void os_tick_config(void)
{
    int div;
    div = clock_get_frequency(clk_osc0clk0) / MCHTMR_FREQ_IN_HZ;
    sysctl_config_clock(HPM_SYSCTL, clock_node_mchtmr0, clock_source_osc0_clk0, div);
    sysctl_add_resource_to_cpu0(HPM_SYSCTL, sysctl_resource_mchtmr0);
    mchtmr_set_compare_value(HPM_MCHTMR, MCHTMR_FREQ_IN_HZ / RT_TICK_PER_SECOND);
    enable_mchtmr_irq();
}

void mchtmr_isr(void)
{
    rt_interrupt_enter();
    HPM_MCHTMR->MTIMECMP = HPM_MCHTMR->MTIME + MCHTMR_FREQ_IN_HZ / RT_TICK_PER_SECOND;
    rt_tick_increase();
    rt_interrupt_leave();
}

void rt_application_init(void *parameter)
{
    rt_thread_startup(rt_thread_create("init", init_thread_entry, RT_NULL, 2 * 1024, 8, 20));
}

int main(void)
{
    board_init();

    rt_hw_interrupt_disable();
    os_tick_config();

    rt_system_timer_init();
    rt_system_scheduler_init();

    rt_system_timer_thread_init();
    rt_thread_idle_init();
    rt_system_heap_init((void *)rtt_heap, (void *)(rtt_heap + sizeof(rtt_heap)));
    rt_application_init(RT_NULL);
    rt_system_scheduler_start();
    return 0;
}

/* redefine memory function */ 
void *malloc(size_t n)
{
    void *p = rt_malloc(n);
    RT_ASSERT(p != RT_NULL);
    return p;
}

void *realloc(void *rmem, size_t newsize)
{
    return rt_realloc(rmem, newsize);
}

void *calloc(size_t nelem, size_t elsize)
{
    return rt_calloc(nelem, elsize);
}

void free(void *rmem)
{
    rt_free(rmem);
}