#include "bl_api.h"

typedef struct 
{
    uint32_t tag;            /* 0x0: offset to boot_header start */
    uint32_t image_hdr_addr;              /* 0x4: size in bytes */
    ihdr_t idhr;
} hipnuc_fw_finder_hdr_t;


__attribute__ ((section(".hipnuc_boot"))) const hipnuc_fw_finder_hdr_t hipnuc_boot_info = 
{
    .tag = MAKRER_MAGIC,
    .image_hdr_addr = (uint32_t)&hipnuc_boot_info.idhr,
    .idhr.header_marker = IHDR_MAGIC,
    .idhr.img_type = 0,
    .idhr.bl_api = 0,
    .idhr.crc_value = 0x55AA1234, 

    /* 0x55AA1234 is a backdoor key, when BL see this partten, it will not check CRC
       when use IDE to download and debug application, the IDE will use elf file instead of hex file which is modify by py script, in that case,
       backdoor key is use to debug, when release， the CRC_value shoud set to other value, which will be replace by external py script
     */
};

