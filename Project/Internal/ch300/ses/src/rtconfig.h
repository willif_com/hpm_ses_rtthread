#ifndef RT_CONFIG_H__
#define RT_CONFIG_H__

/* Automatically generated file; DO NOT EDIT. */
/* RT-Thread Configuration */

/* RT-Thread Kernel */

#define RT_NAME_MAX 8
#define RT_ALIGN_SIZE 8
#define RT_THREAD_PRIORITY_32
#define RT_THREAD_PRIORITY_MAX 32
#define RT_TICK_PER_SECOND 1000
#define RT_USING_OVERFLOW_CHECK
#define RT_USING_HOOK
#define RT_HOOK_USING_FUNC_PTR
#define RT_USING_IDLE_HOOK
#define RT_IDLE_HOOK_LIST_SIZE 4
#define IDLE_THREAD_STACK_SIZE 1024

/* kservice optimization */
#define RT_USING_TIMER_SOFT
#define RT_TIMER_THREAD_STACK_SIZE 2048
#define RT_KPRINTF_USING_LONGLONG

/* Inter-Thread communication */

#define RT_USING_SEMAPHORE
#define RT_USING_MUTEX
#define RT_USING_EVENT
#define RT_USING_MAILBOX
#define RT_USING_MESSAGEQUEUE

/* Memory Management */

#define RT_USING_MEMPOOL
#define RT_USING_SMALL_MEM
#define RT_USING_SMALL_MEM_AS_HEAP
#define RT_USING_HEAP

/* Kernel Device Object */

#define RT_USING_DEVICE
#define RT_USING_CONSOLE
#define RT_CONSOLEBUF_SIZE 256
//#define RT_CONSOLE_DEVICE_NAME "uart1"
//#define RT_VER_NUM 0x40101
//#define ARCH_ARM
//#define RT_USING_CPU_FFS
//#define ARCH_ARM_CORTEX_M
//#define ARCH_ARM_CORTEX_M4


/* RT-Thread Components */

//#define RT_USING_COMPONENTS_INIT
//#define RT_USING_USER_MAIN
//#define RT_MAIN_THREAD_STACK_SIZE 2048
//#define RT_MAIN_THREAD_PRIORITY 10
#define RT_USING_MSH
#define RT_USING_FINSH
#define FINSH_USING_MSH
#define FINSH_THREAD_NAME "tshell"
#define FINSH_THREAD_PRIORITY 20
#define FINSH_THREAD_STACK_SIZE 8192
#define FINSH_USING_SYMTAB
#define FINSH_CMD_SIZE 128
#define MSH_USING_BUILT_IN_COMMANDS
#define FINSH_ECHO_DISABLE_DEFAULT
#define FINSH_ARG_MAX 10





#define RT_USING_LIBC



/* kservice optimization */

//#define RT_DEBUG
//#define RT_DEBUG_COLOR






#define RT_USING_ULOG
#define ULOG_OUTPUT_FLOAT
#define ULOG_BACKEND_USING_CONSOLE
//#define ULOG_USING_FILTER
//#define ULOG_OUTPUT_TIME
#define ULOG_OUTPUT_TAG
#define ULOG_OUTPUT_LVL  LOG_LVL_DBG
#define ULOG_LINE_BUF_SIZE  256





#define RT_USING_SERIAL
//#define RT_USING_SERIAL_V2
#define RT_SERIAL_USING_DMA
#define RT_SERIAL_RB_BUFSZ 2048
#define RT_USING_PIN
#define RT_USING_RTC
#define RT_USING_SPI
#define RT_USING_I2C
#define RT_USING_I2C_BITOPS
#define RT_USING_HWTIMER


/* Using USB */


/* C/C++ and POSIX layer */

#define RT_LIBC_DEFAULT_TIMEZONE 8

#define SOC_HPM6000

/* On-chip Peripheral Drivers */

#define BSP_USING_GPIO
#define BSP_USING_UART


#define BSP_USING_SPI
#define BSP_USING_SPI2
#define BSP_USING_RTC
#define BSP_USING_GPTMR7


#endif
