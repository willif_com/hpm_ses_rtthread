#ifndef _APP_DEF_H
#define _APP_DEF_H

#include "board.h"
#include "drivers/rt_sensor.h"
#include <rtdevice.h>
#include <rtthread.h>

#define APP_VERSION (203)
#define APP_NAME "CH310"
#define FLASH_NVM_ADDR ((512 - 8) * 1024)

/*
 VCOM0: 1st UART -> HW_UART0
 VCOM1: 2st UART -> HW_UART8
 VCOM2: 3st UART -> HW_UART10
 VCOM3: AG3335A -> HW_UART3
 VCOM4: AG3335B -> HW_UART5
 */
#define VCOM0 (0)
#define VCOM1 (1)
#define VCOM3 (3)
#define VCOM4 (4)

#define VCOM_AG3335A VCOM3
#define VCOM_AG3335B VCOM4
#define VCOM_UM982 VCOM3

#define OUTPUT_MSG_GGA (1)
#define OUTPUT_MSG_RMC (2)
#define OUTPUT_MSG_GSV (3)
#define OUTPUT_MSG_GST (4)
#define OUTPUT_MSG_VTG (5)
#define OUTPUT_MSG_TRA (6)
#define OUTPUT_MSG_SXT (7)

#define OUTPUT_MSG_IMUB_91  (10)
#define OUTPUT_MSG_GNSSRCVB (11) /* HiPNUC test msg */
#define OUTPUT_MSG_INSPVAXB (13)
#define OUTPUT_MSG_BESTPOSB (15)
#define OUTPUT_MSG_RAWIMUXB (17)
#define OUTPUT_MSG_RTCM3 (21)
#define OUTPUT_MSG_MAX (32)

/**
 * general device commands
 */

/* data ready flags */

/* data ready flags */
#define EVT_GNSS_RDY_IDX (0)
#define EVT_INS_RDY_IDX (4)
#define EVT_DUALANT_FIX_IDX (5)
#define EVT_CAN_OD_SPEED_IDX (6)
#define EVT_SYNC_IDX (7)
#define EVT_FIRST_GPST_IDX (17)
#define EVT_FIRST_HEADING_IDX (19)

#define EVT_GNSS_RDY (1 << EVT_GNSS_RDY_IDX)
#define EVT_INS_RDY (1 << EVT_INS_RDY_IDX)
#define EVT_DUALANT_FIX (1 << EVT_DUALANT_FIX_IDX)
#define EVT_CAN_OD_SPEED (1 << EVT_CAN_OD_SPEED_IDX) /* car od speed */
#define EVT_SYNC_IN (1 << EVT_SYNC_IDX)
#define EVT_FIRST_GPST_GET (1 << EVT_FIRST_GPST_IDX)       /* evt for GPST syned(first time to get GPST), not cleard */
#define EVT_FIRST_HEADING_GET (1 << EVT_FIRST_HEADING_IDX) /* GNSS get intial heading from vel(vel > 2) or dual heading */
#define EVT_CLR_MASK (EVT_GNSS_RDY | EVT_INS_RDY | EVT_DUALANT_FIX | EVT_SYNC_IN | EVT_CAN_OD_SPEED)

#define GYR_FRQ (100)
#define KF_STATE_UPDATE_DIV (1)
#define MAX_COM_SIZE (2)
#define MAX_IMU_INSTANCE (4)

#define LOG_TYPE_ONNEW (0)
#define LOG_TYPE_ONCHANGED (1)
#define LOG_TYPE_ONTIME (2)
#define LOG_TYPE_ONNEXT (3)
#define LOG_TYPE_ONCE (4)
#define LOG_TYPE_ONMARK (5)

#define MOTION_MODEL_CAR (0)
#define MOTION_MODEL_PLANE (1)
#define MOTION_MODEL_TRAIN (2)
#define MOTION_MODEL_SHIP (3)

typedef struct
{
	uint32_t uart_baud[MAX_COM_SIZE];
	uint32_t can_baud[MAX_COM_SIZE];
	uint32_t rev;
	nl_t antA[3];
	nl_t antB[3];           /* A:主天线, B:定向天线， 从A到B构成基线向量(B坐标-A坐标), 基线向量方向默认与Y轴(东北天-右前上坐标系)相同 */
	uint8_t gnss_on;
	uint8_t motion_model; /* see MOTION_MODEL_CAR  */
	char ntrip_server_ip[32];
	char ntrip_port[8];
	char ntrip_mntpoint[16];
	char ntrip_uname[16];
	char ntrip_passwd[16];
	uint16_t j1939_tmr[16];
	uint8_t rev2[8];
        uint8_t nmea_tid[3];
	uint32_t log_ontime_div[MAX_COM_SIZE][OUTPUT_MSG_MAX]; /* ONTIME output data div */
	uint8_t log_type[MAX_COM_SIZE][OUTPUT_MSG_MAX];        /* 0 = ONNEW, 1 = ONCHANGED,  2 = ONTIME Output on a time interval, 3 = ONNEXT Output only the next message, 4 = ONCE, 5 = ONMARL */
	nl_imu_calib_t imu_calib;
} app_cfg_t;

typedef struct
{
	uint32_t com; /* VCOM index */
	uint32_t len;
	uint8_t buf[RT_SERIAL_RB_BUFSZ];
} com_msg_t;

typedef struct
{
	ins_t ins;
	nl_imu_t imu;
	kf_state_t kf_state;
	kf_meas_t kf_gnss;
	kf_meas_t kf_dualgnss;
	kf_meas_t kf_gravity;
	kf_meas_t kf_zupt;
	kf_meas_t kf_nhc;
	kf_meas_t kf_od;
	kf_meas_t kf_zihr; 
	app_cfg_t cfg;
	gnss_sol_t gnss_sol;
	oem_raw_t oem_raw;
	nmea_raw_t nmea_raw;
	ubx_raw_t ubx_raw;
	rtcm_t rtcm;
        nl_t v_hp[3]; /* high pass filted vel */
	uint8_t rtcm_buf[RT_SERIAL_RB_BUFSZ];
	uint32_t rtcm_buf_len;
	uint32_t evt_bitmap;
	uint32_t evt_count[32];
	uint32_t imu_stat;
	uint16_t com_msg_ctr[2][OUTPUT_MSG_MAX]; /* ONTIME output counter */
	uint8_t com_output_mask[2];              /* COM output mask, 1: enable output, 0:disable output */
        uint8_t en_gnss_data_recv;               /* 1:enable all GNSS data recv, 0: disable GNSS data recv, just use for debug */
	gtime_t gpst;
	nl_t od_speed; /* m/s */
	nl_t gnss_vel_std_norm;
	nl_t gnss_pos_std_norm;
	nl_t last_gnss_vel_std_norm;
	nl_t last_gnss_pos_std_norm;
        uint32_t gnss_pvt_valid_ms; /* last PVT valid time */
        uint32_t gnss_pvt_elapsed;   /* the time from last PVT valid in ms */
        uint32_t gnss_dual_valid_ms; /* last dual fix valid time */
        uint32_t gnss_dual_elapsed;   /* the time from last dual fix valid in ms */

	uint8_t net4g_ip[4];
	uint8_t net4g_IMEI[32]; /* 国际移动设备识别码IMEI */
	uint8_t net4g_CCID[32]; /* (Integrated circuit card identity , 集成电路卡识别码) */
	uint8_t net4g_ver[32]; /* 4G模块型号 */
	rtksvr_t *rtksvr;
	rt_device_t dev_vcom0;
	rt_device_t dev_vcom1;
	rt_device_t dev_vcom2;
	rt_device_t dev_vcom982;
	rt_device_t dev_air4g;
} eskf_svr_t;

void app_imu_init(eskf_svr_t *svr);
void hw_tic_init(const char *name);
void tic(uint32_t instance);
uint32_t toc(uint32_t instance);

int bin_0x91data(uint8_t *buf, uint8_t id, uint32_t ts, nl_t *acc, nl_t *gyr, nl_t *quat, nl_t *mag, nl_t pitch, nl_t roll, nl_t yaw, nl_t prs, nl_t temperature);
int imu_data_avg_sample(nl_imu_t *imu, nl_t *avg_acc, nl_t *avg_gyr, int frq);
int serial_data(uint32_t com, uint8_t *buf, eskf_svr_t *svr, uint32_t max_size);
void app_cfg_safe_check(app_cfg_t *cfg);

int nvdm_write(void *buf, uint32_t len);
int nvdm_read(void *buf, uint32_t len);

void app_ptpc_pps_sync_init(void);
uint32_t app_ptpc_get_gpst(gtime_t *time);
uint32_t app_ptpc_set_gpst(gtime_t time);

void rtklibsol2nlsol(gnss_sol_t *nl_sol, sol_t *rtklibsol);

void main_thread_msg_input(com_msg_t *msg);
void rtk_thread_msg_input(com_msg_t *msg);

extern eskf_svr_t eskfsvr;
#endif