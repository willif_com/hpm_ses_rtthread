
#include <rtdevice.h>

#define LOG_TAG              "INIT"
#define LOG_LVL              LOG_LVL_DBG

#include <ulog.h>

int rt_hw_vserial_init(const char *name, const char *src_name);
void rt_hw_vserial_rx_isr(rt_device_t dev, uint8_t *buf, uint32_t size);

rt_err_t rt_hw_spi_device_attach(const char *bus_name, const char *device_name, uint8_t cs_pin);

void thread_mshell_input(uint8_t idx, uint8_t *buf, uint32_t len);
static rt_device_t vserial;
static rt_device_t dev_vcom0;



static rt_err_t uart_rx_ind(rt_device_t dev, rt_size_t size)
{
    int i, len;
    static uint8_t rx_buf[1024];
    int ret;

  len = rt_device_read(dev, 0, rx_buf, size);

  rt_hw_vserial_rx_isr(vserial, rx_buf, len);

}


void init_thread_entry(void *parameter)
{
    rt_device_t dev = RT_NULL;
    struct rt_serial_device *serial = RT_NULL;
    int i, ret;

    cpu_usage_init();
    rt_hw_usart_init();

    rt_hw_pin_init();
    rt_hw_vserial_init("vserial", "uart0");
    rt_console_set_device("vserial");
    ulog_console_backend_init();

    rt_hw_hwtimer_init();
    rt_hw_spi_init();
    rt_i2c_init();
    rt_hw_on_chip_flash_init();

    vserial = rt_device_find("vserial");
     
    dev_vcom0 = rt_device_find("uart0");
    serial = (struct rt_serial_device *)dev_vcom0;
    serial->config.baud_rate = 115200;
    rt_device_open(dev_vcom0, RT_DEVICE_FLAG_DMA_RX | RT_DEVICE_FLAG_DMA_TX);
    rt_device_set_rx_indicate(dev_vcom0, uart_rx_ind);

   

    finsh_system_init();


    while (1)
    {
       rt_thread_mdelay(1000);
     }

}
