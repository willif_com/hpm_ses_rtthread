#include "board.h"
#include "bl_cfg.h"


#define UART_RX_PIN             IOC_PAD_PY07
#define UART_RX_PIN_GPIO_IRQn   IRQn_GPIO0_Y

/* input frq cannot below 1000*1000*10 Hz */
#define GPIO_IDX                (UART_RX_PIN >> 5)
#define GPIO_PIN                (UART_RX_PIN & 0x1FU)


#ifndef ABS
#define ABS(a)         (((a) < 0) ? (-(a)) : (a))
#endif



static uint32_t supported_baud[] = {9600, 115200, 230400, 256000, 460800, 921600};
static uint32_t cal_baud = 0;
static uint8_t is_baud_found = 0;   /* 0: baud not found, 1:baud found */
static uint32_t mctmr_frq = 0;

void autobaud_init(uint32_t tmr_frq)
{
    mctmr_frq = tmr_frq;
    HPM_IOC->PAD[UART_RX_PIN].FUNC_CTL = IOC_PY07_FUNC_CTL_GPIO_Y_07;
    HPM_PIOC->PAD[UART_RX_PIN].FUNC_CTL = IOC_PY07_FUNC_CTL_SOC_PY_07;

    gpio_set_pin_input(HPM_GPIO0, GPIO_IDX, GPIO_PIN);
    gpio_config_pin_interrupt(HPM_GPIO0, GPIO_IDX, GPIO_PIN, gpio_interrupt_trigger_edge_rising);
    gpio_enable_pin_interrupt(HPM_GPIO0, GPIO_IDX, GPIO_PIN);     
    intc_m_enable_irq_with_priority(UART_RX_PIN_GPIO_IRQn, 1);
}



uint32_t autobaud_get_baud(uint32_t *baud)
{
    if(is_baud_found)
    {
        *baud = cal_baud;
        return 0;
    }
    return 1;
}


static uint32_t _get_baud(uint32_t *ts, uint32_t *baud)
{
    int i, seg[6], val, precent;
    float chk1, chk2, chk3;

    for(i=0; i<6; i++)
    {
      seg[i] = ts[i+1] - ts[i];
      BL_TRACE("seg[%d]:%d\r\n", i, seg[i]);
    }

    chk1 = (float)seg[1] / seg[2];
    chk2 = (float)seg[4] / seg[5];
    chk3 = (float)seg[0] / seg[2];
    BL_TRACE("avg:%d %.1f %.1f %.1f\r\n", seg[1]+seg[2], chk1, chk2, chk3);

/*
921600:
seg[0]:18
seg[1]:32
seg[2]:21
seg[3]:43
seg[4]:43
seg[5]:21

115200:
seg[0]:170
seg[1]:259
seg[2]:173
seg[3]:260
seg[4]:346
seg[5]:173

*/

    if(chk1>1.4 && chk1<1.6 && chk2>1.8 && chk2<2.1 && chk3>0.7 && chk3<1.2)
    {
        for(i=0; i<ARRAY_SIZE(supported_baud); i++)
        {
            val = mctmr_frq / supported_baud[i];
            precent = val * 100 / ((seg[1]+seg[2]) / 5);
            
            /* the simluarity between 70% and 115% */
            if(precent > 80 && precent <120)
            {
                BL_TRACE("found! baud:%d, prcent match:%d\r\n", supported_baud[i], precent);
                is_baud_found = 1;
                *baud = supported_baud[i];
                return 0;
            }
            BL_TRACE("baud:%d, val:%d precent%d\%\r\n", supported_baud[i], val, precent);
        }
    }
    return 1;
}


    
void isr_gpio(void)
{
    gpio_clear_pin_interrupt_flag(HPM_GPIO0, GPIO_IDX, GPIO_PIN);

    static uint8_t cnt = 0;
    static uint32_t tick_time_stamp[7];
    
    tick_time_stamp[cnt++] = (uint32_t)mchtmr_get_count(HPM_MCHTMR);
    
    if(cnt == 7)
    {
        _get_baud(tick_time_stamp, &cal_baud);
        HPM_IOC->PAD[UART_RX_PIN].FUNC_CTL = IOC_PY07_FUNC_CTL_UART0_RXD;
        gpio_disable_pin_interrupt(HPM_GPIO0, GPIO_IDX, GPIO_PIN);  
        gpio_clear_pin_interrupt_flag(HPM_GPIO0, GPIO_IDX, GPIO_PIN);
    }
}
SDK_DECLARE_EXT_ISR_M(UART_RX_PIN_GPIO_IRQn, isr_gpio)

