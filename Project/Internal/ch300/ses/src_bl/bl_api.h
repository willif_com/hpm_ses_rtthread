#ifndef __BL_API_H__
#define __BL_API_H__

#include "stdint.h"

#define BL_VERSION (200)

#define MAKRER_MAGIC (0x0FFEB6B6) /**< protection hdr in front of ihdr addr(addr offset: 0x24) */
#define IHDR_MAGIC (0xFEEDA5A5)   /**< magic of image hdr (addr offset: 0x28) */
#define API_BOOT_TAG (0xEBU)      /**< ROM API parameter tag */

#define BL_API_ENTRY ((const blapi_t *)(*(uint32_t *)0x80002010))

#define PRODUCE_CODE_SIZE (16)

typedef struct
{
    uint32_t header_marker; /*!< Image header marker should always be set to 0xFEEDA5A5 */
    uint32_t img_type;      /*!< Image check type, with or without optional CRC */
    uint32_t bl_api;        /*!< addr of bl_api struct */
    uint32_t img_len;       /*!< Image length or the length of image CRC check should be done. */
    uint32_t crc_value;     /*!< CRC value  */
    uint32_t img_start;     /*!< img_start FIXME: not implmentated */
} ihdr_t;

typedef struct
{
    uint32_t version;
    int (*run_bootloader)(void *arg);
    int (*rev_api1)(void *arg);
    int (*rev_api2)(void *arg);
    int (*rev_api3)(void *arg);
    uint8_t produce_code[PRODUCE_CODE_SIZE];
    uint8_t calb[512];
    uint8_t rev[32];
} blapi_t;

typedef struct
{
    uint32_t index : 8;      /**< Image index */
    uint32_t peripheral : 8; /**< Boot peripheral */
    uint32_t src : 8;        /**< Boot source */
    uint32_t tag : 8;        /**< ROM API parameter tag, must be 0xEB */

    uint32_t mdbus_id;
    uint32_t uart_baud[2];
    uint32_t co_id;
    uint32_t can_baud[2];
    uint32_t timeout_ms;
} blapi_arg_t;

static inline int bl_enter_bootloader(void *ctx)
{
    if (BL_API_ENTRY)
        return BL_API_ENTRY->run_bootloader(ctx);
    return -1;
}

static inline int bl_get_version(void)
{
    if(BL_API_ENTRY && (BL_API_ENTRY != 0xFFFFFFFF))
        return BL_API_ENTRY->version;
    return 0;
}

static inline int bl_get_produce_code(uint8_t *code, uint8_t len)
{
    int i;
    for (i = 0; i < len; i++)
        code[i] = '\0';

    for (i = 0; i < sizeof(BL_API_ENTRY->produce_code); i++)
    {
        code[i] = BL_API_ENTRY->produce_code[i];
    }
    return 0;
    return -1;
}

#endif