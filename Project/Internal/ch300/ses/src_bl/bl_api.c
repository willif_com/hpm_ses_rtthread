#include "bl_api.h"
#include "board.h"

const blapi_t BL_API;

__attribute__( ( section(".noinit"))) blapi_arg_t boot_arg ;



typedef struct 
{
    uint32_t tag;            /* 0x0: offset to boot_header start */
    uint32_t image_hdr_addr;              /* 0x4: size in bytes */
    ihdr_t idhr;
} hipnuc_fw_finder_hdr_t;



__attribute__ ((section(".hipnuc_boot"))) const hipnuc_fw_finder_hdr_t hipnuc_boot_info = 
{
    .tag = MAKRER_MAGIC,
    .image_hdr_addr = (uint32_t)&hipnuc_boot_info.idhr,
    .idhr.header_marker = IHDR_MAGIC,
    .idhr.img_type = 0,
    .idhr.bl_api = (uint32_t)&BL_API,
};


int run_bootloader(void *arg)
{
    blapi_arg_t *ctx = (blapi_arg_t*)arg;
    
    if(ctx->tag == API_BOOT_TAG)
    {
        memcpy(&boot_arg, arg, sizeof(blapi_arg_t));
        ppor_sw_reset(HPM_PPOR, 1000);
    }
    return -1;
}


const blapi_t BL_API =
{
    .version = BL_VERSION,
    .run_bootloader = run_bootloader,
};

