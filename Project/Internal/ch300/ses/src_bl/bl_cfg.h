#ifndef __BL_CFG_H__
#define __BL_CFG_H__

#define BL_DEBUG		0
#if ( BL_DEBUG == 1 )
#include <stdio.h>
#define BL_TRACE	printf
#else
#define BL_TRACE(...)
#endif

#define BL_SIZE                     (64*1024)

#define FLASH_BASE                  (0x80000000)
#define APP_SEARCH_START            (FLASH_BASE + 0x13000)


#define DEFAULT_BL_TIMEOUT          (100)
#define DEFAULT_UART_BAUD           (115200)
#define DEFAULT_CAN_BAUD            (500*1000)

#define TARGET_FLASH_SIZE           (2*1024*1024)
#define TARGET_RAM_BASE             (0x1050000)
#define TARGET_RAM_SIZE             (512*1024)

#endif
