#include "bl_cfg.h"
#include "bl_api.h"
#include "board.h"
#include "mcuboot.h"
#include "boot_common.h"


#define TMR_PERIOD_MS      (10)
#define UART_INSTANCE       HPM_UART0
#define UART_CLOCK          clock_uart0

void autobaud_init(uint32_t tmr_frq);
uint32_t autobaud_get_baud(uint32_t *baud);
extern blapi_arg_t boot_arg;

typedef struct
{
    uint8_t         en_jump;                    /* timeout jump flag */
    mcuboot_t       mcuboot;
    uint8_t         is_autobaud_complete;
    uint32_t        protocol;
    uint32_t        uuid[2];
    uint32_t        mctmr_frq;
}blsvr_t;

static blsvr_t blsvr = {0};



static int mcuboot_send(uint8_t *buf, uint32_t len)
{
    uart_send_data(UART_INSTANCE, buf, len);
}

static void mcuboot_reset(void)
{
    ppor_sw_reset(HPM_PPOR, 1000);
}


static void mcuboot_complete(void) {}

int main(void)
{
    uint8_t c;
    int baud;

    board_init();
    memory_init();

    board_get_uuid(blsvr.uuid);

    if(boot_arg.tag != API_BOOT_TAG)
    {
        boot_arg.timeout_ms = DEFAULT_BL_TIMEOUT;
        boot_arg.uart_baud[0] = DEFAULT_UART_BAUD;
        boot_arg.uart_baud[1] = DEFAULT_UART_BAUD;
        boot_arg.can_baud[0] = DEFAULT_CAN_BAUD;
        boot_arg.can_baud[1] = DEFAULT_CAN_BAUD;
        boot_arg.co_id = 0x08;
        boot_arg.mdbus_id = 0x50;
    }
    boot_arg.tag = 0;

    uart_config_t config = {0};

    uart_default_config(UART_INSTANCE, &config);
    config.src_freq_in_hz = clock_get_frequency(UART_CLOCK);
    config.baudrate = boot_arg.uart_baud[0];
    uart_init(UART_INSTANCE, &config);
    clock_cpu_delay_ms(1);

    BL_TRACE("HIPNUC BOOT %d\r\n", bl_get_version());
    BL_TRACE("%-*.*s\r\n",        12, 12, "FLASH INFO");
    BL_TRACE("%-*.*s:%dKB\r\n",     12, 12, "TOTAL", memory_get_total_size()/1024);
    BL_TRACE("%-*.*s:%d\r\n",     12, 12, "SECTOR", memory_get_sector_size());
    BL_TRACE("%-*.*s:%d\r\n",     12, 12, "TIMEOUT", boot_arg.timeout_ms);
    BL_TRACE("%-*.*s:0x%02X\r\n", 12, 12, "MODBUS_ID", boot_arg.mdbus_id);
    BL_TRACE("%-*.*s:0x%02X\r\n", 12, 12, "CANOPEN_ID", boot_arg.co_id);
    BL_TRACE("%-*.*s:%d, %d\r\n", 12, 12, "UART", boot_arg.uart_baud[0], boot_arg.uart_baud[1]);
    BL_TRACE("%-*.*s:%d, %d\r\n", 12, 12, "CAN", boot_arg.can_baud[0], boot_arg.can_baud[1]);

    blsvr.mctmr_frq = clock_get_frequency(clock_mchtmr0);

    /* config the mcuboot */
    blsvr.mcuboot.op_send = mcuboot_send;
    blsvr.mcuboot.op_reset = mcuboot_reset;
    blsvr.mcuboot.op_jump = jump_to_app;
    blsvr.mcuboot.op_complete = mcuboot_complete;

    blsvr.mcuboot.op_mem_erase = memory_erase;
    blsvr.mcuboot.op_mem_write = memory_write;
    blsvr.mcuboot.op_mem_read = memory_read;

    blsvr.mcuboot.cfg_flash_start = APP_SEARCH_START;
    blsvr.mcuboot.cfg_flash_size = memory_get_total_size();
    blsvr.mcuboot.cfg_flash_sector_size = memory_get_sector_size();
    blsvr.mcuboot.cfg_ram_start = TARGET_RAM_BASE;
    blsvr.mcuboot.cfg_ram_size = TARGET_RAM_SIZE;

    blsvr.mcuboot.cfg_device_id = 0x12345678;
    blsvr.mcuboot.cfg_uuid = blsvr.uuid[1];

    mcuboot_init(&blsvr.mcuboot);
    autobaud_init(blsvr.mctmr_frq);

    mchtmr_set_compare_value(HPM_MCHTMR, mchtmr_get_count(HPM_MCHTMR) + (blsvr.mctmr_frq) / (1000 / TMR_PERIOD_MS));
    enable_mchtmr_irq();

    while (1)
    {
        if (uart_receive_byte(UART_INSTANCE, &c) == status_success)
        {
            mcuboot_recv(&blsvr.mcuboot, &c, 1);
        }

        /* waitting for autobaud */
        if (autobaud_get_baud(&baud) == 0 && blsvr.is_autobaud_complete == 0)
        {
            config.baudrate = baud;
            init_uart_pins(UART_INSTANCE);
            uart_init(UART_INSTANCE, &config);
            /* feed mcuboot for the char use for autobaud(if not timeout)*/
            c = 0x5A;
            mcuboot_recv(&blsvr.mcuboot, &c, 1);
            c = 0xA6;
            mcuboot_recv(&blsvr.mcuboot, &c, 1);
            mcuboot_proc(&blsvr.mcuboot);
            blsvr.is_autobaud_complete = 1;
        }

        if (blsvr.en_jump == 1)
        {
            jump_to_app();
            blsvr.en_jump = 0;
        }
        mcuboot_proc(&blsvr.mcuboot);
    }

    return 0;
}

void isr_mchtmr(void)
{
    HPM_MCHTMR->MTIMECMP = HPM_MCHTMR->MTIME + (blsvr.mctmr_frq) / (1000 / TMR_PERIOD_MS);

    static volatile int timeout = 0;

    if (timeout > boot_arg.timeout_ms)
    {
        if (mcuboot_is_connected(&blsvr.mcuboot) == 0)
        {
            blsvr.en_jump = 1;
        }
        disable_mchtmr_irq();
    }
    timeout += TMR_PERIOD_MS;
}
SDK_DECLARE_MCHTMR_ISR(isr_mchtmr)