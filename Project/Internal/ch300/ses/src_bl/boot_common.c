#include "boot_common.h"
#include "bl_api.h"
#include "crc32.h"

typedef struct
{
    xpi_nor_config_t cfg;
    uint32_t flash_size;
    uint32_t sector_size;
    uint32_t page_size;
} xpi_t;

static xpi_t xpi;

/* ALIGN */
#define ALIGN_DOWN(x, a) ((x) & -(a))
#define ALIGN_UP(x, a) (-(-(x) & -(a)))
#define AHB2IP_ADDR(x) (x & 0x7FFFFFFF)

static volatile __attribute__((section(".noncacheable"), aligned(8))) uint8_t write_buf[8192] = {0};

int memory_get_total_size(void)
{
    return xpi.flash_size;
}

int memory_get_sector_size(void)
{
    return xpi.sector_size;
}

int memory_init(void)
{
    xpi_nor_config_option_t nor_option = {0};

    /* CH300 */
    extern const uint32_t option[4];
    nor_option.header.U = option[0];
    nor_option.option0.U = option[1];
    nor_option.option1.U = option[2];
    nor_option.option2.U = option[3];

    hpm_stat_t status = rom_xpi_nor_auto_config(HPM_XPI0, &xpi.cfg, &nor_option);

    rom_xpi_nor_get_property(HPM_XPI0, &xpi.cfg, xpi_nor_property_total_size, &xpi.flash_size);
    rom_xpi_nor_get_property(HPM_XPI0, &xpi.cfg, xpi_nor_property_sector_size, &xpi.sector_size);
    rom_xpi_nor_get_property(HPM_XPI0, &xpi.cfg, xpi_nor_property_page_size, &xpi.page_size);
}

int memory_erase(uint32_t addr, uint32_t len)
{
    int ret;
    addr = AHB2IP_ADDR(addr);
    if (addr < BL_SIZE)
        return 1;

    disable_global_irq(CSR_MSTATUS_MIE_MASK);
    ret = rom_xpi_nor_erase(HPM_XPI0, xpi_xfer_channel_auto, &xpi.cfg, ALIGN_DOWN(addr, xpi.sector_size), ALIGN_UP(len, xpi.sector_size) + xpi.sector_size);
    enable_global_irq(CSR_MSTATUS_MIE_MASK);
    return ret;
}

int memory_write(uint32_t addr, uint8_t *buf, uint32_t len)
{
    int ret;

    addr = AHB2IP_ADDR(addr);
    if (addr < BL_SIZE)
        return 1;
    memcpy(write_buf, buf, len);

    disable_global_irq(CSR_MSTATUS_MIE_MASK);
    ret = rom_xpi_nor_program(HPM_XPI0, xpi_xfer_channel_auto, &xpi.cfg, (uint32_t *)write_buf, addr, ALIGN_UP(len, xpi.page_size));
    enable_global_irq(CSR_MSTATUS_MIE_MASK);
    return ret;
}

int memory_read(uint32_t addr, uint8_t *buf, uint32_t len)
{
    addr = AHB2IP_ADDR(addr);
    return rom_xpi_nor_read(HPM_XPI0, xpi_xfer_channel_auto, &xpi.cfg, (uint32_t *)buf, addr, len);
}


/*
ret:
  0: SUCC or BACKDOOR found.
 -1: invalid image
*/
static int scan_valid_boot_addr(uint32_t search_start, uint32_t *boot_addr)
{
    uint32_t addr, tag, ihdr_addr, cal_crc, hdr_ofs, possible_image_start;

    for (addr = search_start; addr <= (search_start + 128 * 1024); addr += 4)
    {
        tag = *(uint32_t *)addr;
        ihdr_addr = *(uint32_t *)(addr + 4);
        if (tag == MAKRER_MAGIC)
        {
            ihdr_t *ihdr = (ihdr_t *)(ihdr_addr);
            if (ihdr && ihdr->header_marker == IHDR_MAGIC)
            {
                BL_TRACE("ihdr found! ADDR:0x%08X, CRC:0x%08X, LEN:0x%08X\r\n", addr, ihdr->crc_value, ihdr->img_len);

                if (ihdr->crc_value == 0x55AA1234)
                {
                    BL_TRACE("BACKDOOR KEY FOUND!, it's possible to be download by IDE\r\n");
                    *boot_addr = APP_SEARCH_START;
                    return 0;
                }
                else if (!ihdr->crc_value || !ihdr->img_len)
                {
                    continue;
                }
                else
                {
                    /* if no image_start, guess image start must be 4K align down */
                    (ihdr->img_start)?(possible_image_start = ihdr->img_start):(possible_image_start = ALIGN_DOWN(addr, 4096));

                    hdr_ofs = ihdr_addr - possible_image_start;
                    BL_TRACE("hdr_ofs:0x%X\r\n", hdr_ofs);
                    crc32_init(&cal_crc);
                    crc32_generate(&cal_crc, (uint8_t *)possible_image_start, hdr_ofs + 4 * 4);
                    crc32_generate(&cal_crc, (uint8_t *)possible_image_start + hdr_ofs + 4 * 5, ihdr->img_len - hdr_ofs - 4 * 5);
                    crc32_complete(&cal_crc);
                    
                    BL_TRACE("CAL CRC:0x%08X\r\n", cal_crc);
                    if(cal_crc == ihdr->crc_value)
                    {
                        *boot_addr = possible_image_start;
                        return 0;
                    }
                    else
                    {
                        continue;
                    }
                }
            }
        }
    }
    return -1;
}

static void _jump(uint32_t addr)
{

    typedef void (*app_entry_t)(void);

    static app_entry_t s_application = 0;

    s_application = (app_entry_t)addr;
    s_application();

    // Should never reach here.
    while (1)
        ;
}

void jump_to_app(void)
{
    uint32_t boot_addr = 0;
    int ret;
    
    ret = scan_valid_boot_addr(APP_SEARCH_START, &boot_addr);

    switch(ret)
    {
        case 0:
          BL_TRACE("IMAGE CHECK PASSED\r\n");
          break;
        case -1:
          BL_TRACE("IMAGE CHECK FAILED, BUT IT'S LEGCY IMAGE\r\n");
          break;
        case -2:
          BL_TRACE("IMAGE CHECK FAILED\r\n");
          break;
    }

    if(ret == 0 || ret == -1)
    {
        if(ret == -1)
        {
            boot_addr = APP_SEARCH_START;
        }

        BL_TRACE("jump to 0x%08X\r\n", boot_addr);
        l1c_dc_invalidate_all();
        l1c_dc_disable();
        l1c_ic_disable();
        l1c_dc_invalidate_all();

        disable_mchtmr_irq();
        _jump(boot_addr);
    }

}