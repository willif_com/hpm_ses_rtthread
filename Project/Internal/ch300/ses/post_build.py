import sys
import os
import struct
import subprocess

file_path1 = sys.argv[1]
file_path2 = sys.argv[2]
file_name =  sys.argv[3]

def main():
    full_file_name = f"{file_path1}/{file_path2}/{file_name}.hex"
    print(f"Full file name: {full_file_name}")
    # 获取当前工作目录
    current_directory = os.getcwd()

    # 构建脚本文件的相对路径
    current_directory = os.path.dirname(current_directory)
    current_directory = os.path.dirname(current_directory)
    current_directory = os.path.dirname(current_directory)
    current_directory = os.path.dirname(current_directory)
    script_path = os.path.join(current_directory, 'tools', 'post_compute_crc.py')
    print(script_path)
    
    subprocess.call(['python', script_path, full_file_name])

    
    # import post_compute_crc
    # post_compute_crc

if __name__ == '__main__':
    main() 