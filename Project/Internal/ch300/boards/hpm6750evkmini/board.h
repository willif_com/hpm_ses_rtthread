/*
 * Copyright (c) 2021 hpmicro
 *
 * SPDX-License-Identifier: BSD-3-Clause
 *
 */

#ifndef _HPM_BOARD_H
#define _HPM_BOARD_H
#include <stdio.h>
#include "hpm_common.h"
#include "hpm_soc.h"
#include "hpm_soc_feature.h"
#include "hpm_clock_drv.h"
#include "hpm_mchtmr_drv.h"
#include "hpm_trgm_drv.h"
#include "hpm_gptmr_drv.h"
#include "hpm_ppor_drv.h"
#include "hpm_romapi.h"
#include "hpm_gpio_drv.h"
#include "hpm_ptpc_drv.h"
#include "hpm_dma_drv.h"
#include "hpm_dmamux_drv.h"
#include "hpm_l1c_drv.h"
#include "hpm_ppor_drv.h"
#include "hpm_uart_drv.h"
#include "hpm_clock_drv.h"
#include "hpm_wdg_drv.h"
#include "pinmux.h"




#define GPIO_LED                (IOC_PAD_PC16)
#define GPIO_TEST               (IOC_PAD_PC08)
#define GPIO_GYR_INT            (255)
#define GPIO_ACC_INT            (255)
#define GPIO_SYNC_IN0           (IOC_PAD_PC25)  /* OR E25 */
#define GPIO_SYNC_IN1           (IOC_PAD_PC04)  /* OR B14 */
#define GPIO_SYNC_OUT           (IOC_PAD_PA21)
#define GPIO_GNSS_RST           (IOC_PAD_PE17)
#define GPIO_GNSS_ERR_STAT      (IOC_PAD_PE20)
#define GPIO_PPS                (IOC_PAD_PE14)

#define SPI20_CS_PIN            IOC_PAD_PE21
#define SPI21_CS_PIN            IOC_PAD_PE22
#define SPI22_CS_PIN            IOC_PAD_PE29
#define SPI23_CS_PIN            IOC_PAD_PE31

#define SPI24_CS_PIN            IOC_PAD_PD13
#define SPI25_CS_PIN            IOC_PAD_PD12
#define SPI26_CS_PIN            IOC_PAD_PD15
#define SPI27_CS_PIN            IOC_PAD_PD14


#define SPI29_CS_PIN            IOC_PAD_PE18

//#define BOARD_CPU_FREQ (816000000UL)
#define BOARD_CPU_FREQ (600000000U)


#if defined(__cplusplus)
extern "C" {
#endif /* __cplusplus */


void board_init(void);
void board_init_console(void);
void board_init_uart(UART_Type *ptr);
void init_can_pin(CAN_Type *ptr);
void board_init_clock(void);
uint32_t board_init_uart_clock(UART_Type *ptr);
uint32_t board_init_spi_clock(SPI_Type *ptr);
uint32_t board_init_can_clock(CAN_Type *ptr);
void board_init_pmp(void);
uint32_t board_init_gptmr_clock(GPTMR_Type *ptr);
void board_get_uuid(uint32_t *UUID);
char* board_get_cpu_name(void);
uint32_t board_get_image_size(void);
uint32_t board_get_image_start(void);
void board_mctmr_init(void);
uint64_t board_mctmr_get_us(void);

#if defined(__cplusplus)
}
#endif /* __cplusplus */
#endif /* _HPM_BOARD_H */
