/*
 * Copyright (c) 2021 hpmicro
 * SPDX-License-Identifier: BSD-3-Clause
 *
 */

#include "board.h"
#include "assert.h"
#include "hpm_clock_drv.h"
#include "hpm_debug_console.h"
#include "hpm_dram_drv.h"
#include "hpm_gpio_drv.h"
#include "hpm_gptmr_drv.h"
#include "hpm_i2c_drv.h"
#include "hpm_lcdc_drv.h"
#include "hpm_pllctl_drv.h"
#include "hpm_pmp_drv.h"
#include "hpm_pwm_drv.h"
#include "hpm_sdxc_drv.h"
#include "hpm_sdxc_soc_drv.h"
#include "hpm_sysctl_drv.h"
#include "hpm_uart_drv.h"
#include "pinmux.h"

#if defined(FLASH_XIP) && FLASH_XIP
__attribute__((section(".nor_cfg_option"))) const uint32_t option[4] = {0xfcf90002, 0x00000007, 0x100, 0x0}; /* CH300 */
#endif

void board_init_console(void)
{
    console_config_t cfg;

    /* Configure the UART clock to 24MHz */
    clock_set_source_divider(clock_uart0, clk_src_osc24m, 1U);

    cfg.type = console_type_uart;
    cfg.base = (uint32_t)HPM_UART0;
    cfg.src_freq_in_hz = clock_get_frequency(clock_uart0);
    cfg.baudrate = 115200;

    init_uart_pins((UART_Type *)cfg.base);

    console_init(&cfg);
}

void board_print_clock_freq(void)
{
    printf("==============================\n");
    printf("Board Clock Summary:\n");
    printf("cpu0:            %10.3fMHz\n", clock_get_frequency(clock_cpu0) / 1e6);
    printf("cpu1:            %10.3fMHz\n", clock_get_frequency(clock_cpu1) / 1e6);
    printf("axi0:            %10.3fMHz\n", clock_get_frequency(clock_axi0) / 1e6);
    printf("axi1:            %10.3fMHz\n", clock_get_frequency(clock_axi1) / 1e6);
    printf("axi2:            %10.3fMHz\n", clock_get_frequency(clock_axi2) / 1e6);
    printf("ahb:             %10.3fMHz\n", clock_get_frequency(clock_ahb) / 1e6);
    printf("mchtmr0:         %10.3fMHz\n", clock_get_frequency(clock_mchtmr0) / 1e6);
    printf("mchtmr1:         %10.3fMHz\n", clock_get_frequency(clock_mchtmr1) / 1e6);
    printf("xpi0:            %10.3fMHz\n", clock_get_frequency(clock_xpi0) / 1e6);
    printf("xpi1:            %10.3fMHz\n", clock_get_frequency(clock_xpi1) / 1e6);
    printf("clk_osc0clk0:    %10.3fMHz\n", clock_get_frequency(clk_osc0clk0) / 1e6);
    printf("clk_pll0clk0:    %10.3fMHz\n", clock_get_frequency(clk_pll0clk0) / 1e6);
    printf("clk_pll1clk0:    %10.3fMHz\n", clock_get_frequency(clk_pll1clk0) / 1e6);
    printf("clk_pll1clk1:    %10.3fMHz\n", clock_get_frequency(clk_pll1clk1) / 1e6);
    printf("clk_pll2clk0:    %10.3fMHz\n", clock_get_frequency(clk_pll2clk0) / 1e6);
    printf("clk_pll2clk1:    %10.3fMHz\n", clock_get_frequency(clk_pll2clk1) / 1e6);
    printf("clk_pll3clk0:    %10.3fMHz\n", clock_get_frequency(clk_pll3clk0) / 1e6);
    printf("clk_pll4clk0:    %10.3fMHz\n", clock_get_frequency(clk_pll4clk0) / 1e6);
    printf("dram:            %10.3fMHz\n", clock_get_frequency(clock_dram) / 1e6);
    printf("uart0:           %10.3fMHz\n", clock_get_frequency(clock_uart0) / 1e6);
    printf("clock_gpio:      %10.3fMHz\n", clock_get_frequency(clock_gpio) / 1e6);
    printf("pdma:            %10.3fMHz\n", clock_get_frequency(clock_pdma) / 1e6);
    printf("clock_gptmr0:    %10.3fMHz\n", clock_get_frequency(clock_gptmr0) / 1e6);
    printf("ptpc:            %10.3fMHz\n", clock_get_frequency(clock_ptpc) / 1e6);
    printf("==============================\n");
}

char *board_get_cpu_name(void)
{
    return "HPM6750";
}

uint32_t board_get_image_size(void)
{
    return 0;
}

uint32_t board_get_image_start(void)
{
    return 0;
}

void board_init(void)
{
    board_init_clock();
    board_init_console();
    board_init_pmp();
    clock_set_source_divider(clock_ahb, clk_src_pll1_clk1, 2); /*200m hz*/

    clock_update_core_clock();
    board_mctmr_init();

    board_init_uart_clock(HPM_UART0);
    init_uart_pins(HPM_UART0);
}

uint32_t board_init_uart_clock(UART_Type *ptr)
{
    uint32_t freq = 0;

    if (ptr == HPM_UART0)
    {
        clock_set_source_divider(clock_uart0, clk_src_osc24m, 0);
        freq = clock_get_frequency(clock_uart0);
    }
    else if (ptr == HPM_UART1)
    {
        clock_set_source_divider(clock_uart1, clk_src_osc24m, 0);
        freq = clock_get_frequency(clock_uart1);
    }
    else if (ptr == HPM_UART2)
    {
        clock_set_source_divider(clock_uart2, clk_src_osc24m, 0);
        freq = clock_get_frequency(clock_uart2);
    }
    else if (ptr == HPM_UART3)
    {
        clock_set_source_divider(clock_uart3, clk_src_osc24m, 0);
        freq = clock_get_frequency(clock_uart3);
    }
    else if (ptr == HPM_UART4)
    {
        clock_set_source_divider(clock_uart4, clk_src_osc24m, 0);
        freq = clock_get_frequency(clock_uart4);
    }
    else if (ptr == HPM_UART5)
    {
        clock_set_source_divider(clock_uart5, clk_src_osc24m, 0);
        freq = clock_get_frequency(clock_uart5);
    }
    else if (ptr == HPM_UART6)
    {
        clock_set_source_divider(clock_uart6, clk_src_osc24m, 0);
        freq = clock_get_frequency(clock_uart6);
    }
    else if (ptr == HPM_UART7)
    {
        clock_set_source_divider(clock_uart7, clk_src_osc24m, 0);
        freq = clock_get_frequency(clock_uart7);
    }
    else if (ptr == HPM_UART8)
    {
        clock_set_source_divider(clock_uart8, clk_src_osc24m, 0);
        freq = clock_get_frequency(clock_uart8);
    }
    else if (ptr == HPM_UART9)
    {
        clock_set_source_divider(clock_uart9, clk_src_osc24m, 0);
        freq = clock_get_frequency(clock_uart9);
    }
    else if (ptr == HPM_UART10)
    {
        clock_set_source_divider(clock_uart10, clk_src_osc24m, 0);
        freq = clock_get_frequency(clock_uart10);
    }
    else if (ptr == HPM_UART11)
    {
        clock_set_source_divider(clock_uart11, clk_src_osc24m, 0);
        freq = clock_get_frequency(clock_uart11);
    }
    else if (ptr == HPM_UART12)
    {
        clock_set_source_divider(clock_uart12, clk_src_osc24m, 0);
        freq = clock_get_frequency(clock_uart12);
    }
    else if (ptr == HPM_UART13)
    {
        clock_set_source_divider(clock_uart13, clk_src_osc24m, 0);
        freq = clock_get_frequency(clock_uart13);
    }
    else if (ptr == HPM_UART14)
    {
        clock_set_source_divider(clock_uart14, clk_src_osc24m, 0);
        freq = clock_get_frequency(clock_uart14);
    }
    else if (ptr == HPM_UART15)
    {
        clock_set_source_divider(clock_uart15, clk_src_osc24m, 0);
        freq = clock_get_frequency(clock_uart15);
    }
    else if (ptr == HPM_PUART)
    {
        clock_set_source_divider(clock_puart, clk_src_osc24m, 0);
        freq = clock_get_frequency(clock_puart);
    }
    else
    {
        /* Unsupported instance */
    }

    return freq;
}

uint32_t board_init_spi_clock(SPI_Type *ptr)
{
    uint32_t freq = 0;
    if (ptr == HPM_SPI0)
    {
        clock_add_to_group(clock_spi0, 0);
        clock_set_source_divider(clock_spi0, clk_src_pll1_clk1, 5U);
        freq = clock_get_frequency(clock_spi0);
    }
    else if (ptr == HPM_SPI1)
    {
        clock_add_to_group(clock_spi1, 0);
        clock_set_source_divider(clock_spi1, clk_src_pll1_clk1, 5U);
        freq = clock_get_frequency(clock_spi1);
    }
    else if (ptr == HPM_SPI2)
    {
        clock_add_to_group(clock_spi2, 0);
        clock_set_source_divider(clock_spi2, clk_src_pll1_clk1, 5U);
        freq = clock_get_frequency(clock_spi2);
    }
    else if (ptr == HPM_SPI3)
    {
        clock_add_to_group(clock_spi3, 0);
        clock_set_source_divider(clock_spi3, clk_src_pll1_clk1, 5U);
        freq = clock_get_frequency(clock_spi3);
    }
    else
    {
        return 0;
    }
}

uint32_t board_init_can_clock(CAN_Type *ptr)
{
    uint32_t freq = 0;
    if (ptr == HPM_CAN0)
    {
        /* Set the CAN0 peripheral clock to 80MHz */
        clock_set_source_divider(clock_can0, clk_src_pll1_clk1, 5);
        freq = clock_get_frequency(clock_can0);
    }
    else if (ptr == HPM_CAN1)
    {
        /* Set the CAN1 peripheral clock to 80MHz */
        clock_set_source_divider(clock_can1, clk_src_pll1_clk1, 5);
        freq = clock_get_frequency(clock_can1);
    }
    else if (ptr == HPM_CAN2)
    {
        /* Set the CAN2 peripheral clock to 80MHz */
        clock_set_source_divider(clock_can2, clk_src_pll1_clk1, 5);
        freq = clock_get_frequency(clock_can2);
    }
    else if (ptr == HPM_CAN3)
    {
        /* Set the CAN3 peripheral clock to 80MHz */
        clock_set_source_divider(clock_can3, clk_src_pll1_clk1, 5);
        freq = clock_get_frequency(clock_can3);
    }
    else
    {
        /* Invalid CAN instance */
    }
    return freq;
}

void board_mctmr_init(void)
{
    int div;
    div = clock_get_frequency(clk_pll0clk0) / (1000*1000);
    sysctl_config_clock(HPM_SYSCTL, clock_node_mchtmr0, clock_source_pll0_clk0, div);
    sysctl_add_resource_to_cpu0(HPM_SYSCTL, sysctl_resource_mchtmr0);
    mchtmr_init_counter(HPM_MCHTMR, 0);
}

uint64_t board_mctmr_get_us(void)
{
  return (mchtmr_get_count(HPM_MCHTMR));
}

uint32_t board_init_gptmr_clock(GPTMR_Type *ptr)
{
    uint32_t freq = 0;

    if (ptr == HPM_GPTMR0)
    {
        clock_add_to_group(clock_gptmr0, 0);
        clock_set_source_divider(clock_gptmr0, clk_src_pll1_clk1, 4);
        freq = clock_get_frequency(clock_gptmr0);
    }
    else if (ptr == HPM_GPTMR1)
    {
        clock_add_to_group(clock_gptmr1, 0);
        clock_set_source_divider(clock_gptmr1, clk_src_pll1_clk1, 4);
        freq = clock_get_frequency(clock_gptmr1);
    }
    else if (ptr == HPM_GPTMR2)
    {
        clock_add_to_group(clock_gptmr2, 0);
        clock_set_source_divider(clock_gptmr2, clk_src_pll1_clk1, 4);
        freq = clock_get_frequency(clock_gptmr2);
    }
    else if (ptr == HPM_GPTMR3)
    {
        clock_add_to_group(clock_gptmr3, 0);
        clock_set_source_divider(clock_gptmr3, clk_src_pll1_clk1, 4);
        freq = clock_get_frequency(clock_gptmr3);
    }
    else if (ptr == HPM_GPTMR4)
    {
        clock_add_to_group(clock_gptmr4, 0);
        clock_set_source_divider(clock_gptmr4, clk_src_pll1_clk1, 4);
        freq = clock_get_frequency(clock_gptmr4);
    }
    else if (ptr == HPM_GPTMR5)
    {
        clock_add_to_group(clock_gptmr5, 0);
        clock_set_source_divider(clock_gptmr5, clk_src_pll1_clk1, 4);
        freq = clock_get_frequency(clock_gptmr5);
    }
    else if (ptr == HPM_GPTMR6)
    {
        clock_add_to_group(clock_gptmr6, 0);
        clock_set_source_divider(clock_gptmr6, clk_src_pll1_clk1, 4);
        freq = clock_get_frequency(clock_gptmr6);
    }
    else if (ptr == HPM_GPTMR7)
    {
        clock_add_to_group(clock_gptmr7, 0);
        clock_set_source_divider(clock_gptmr7, clk_src_pll1_clk1, 4);
        freq = clock_get_frequency(clock_gptmr7);
    }
    else
    {
        /* Invalid instance */
    }
}

void board_init_pmp(void)
{
    extern uint32_t __noncacheable_start__[];
    extern uint32_t __noncacheable_end__[];

    uint32_t start_addr = (uint32_t)__noncacheable_start__;
    uint32_t end_addr = (uint32_t)__noncacheable_end__;
    uint32_t length = end_addr - start_addr;

    if (length == 0)
    {
        return;
    }

    /* Ensure the address and the length are power of 2 aligned */
    assert((length & (length - 1U)) == 0U);
    assert((start_addr & (length - 1U)) == 0U);

    pmp_entry_t pmp_entry[1];
    pmp_entry[0].pmp_addr = PMP_NAPOT_ADDR(start_addr, length);
    pmp_entry[0].pmp_cfg.val = PMP_CFG(READ_EN, WRITE_EN, EXECUTE_EN, ADDR_MATCH_NAPOT, REG_UNLOCK);
    pmp_entry[0].pma_addr = PMA_NAPOT_ADDR(start_addr, length);
    pmp_entry[0].pma_cfg.val = PMA_CFG(ADDR_MATCH_NAPOT, MEM_TYPE_MEM_NON_CACHE_BUF, AMO_EN);

    pmp_config(&pmp_entry[0], ARRAY_SIZE(pmp_entry));
}

void board_init_clock(void)
{
    uint32_t cpu0_freq = clock_get_frequency(clock_cpu0);
    if (cpu0_freq == PLLCTL_SOC_PLL_REFCLK_FREQ)
    {
        /* Configure the External OSC ramp-up time: ~9ms */
        pllctl_xtal_set_rampup_time(HPM_PLLCTL, 32UL * 1000UL * 9U);

        /* Select clock setting preset1 */
        sysctl_clock_set_preset(HPM_SYSCTL, sysctl_preset_1);
    }

    /* Add most Clocks to group 0 */
    clock_add_to_group(clock_cpu0, 0);
    clock_add_to_group(clock_mchtmr0, 0);
    clock_add_to_group(clock_axi0, 0);
    clock_add_to_group(clock_axi1, 0);
    clock_add_to_group(clock_axi2, 0);
    clock_add_to_group(clock_ahb, 0);
    // clock_add_to_group(clock_dram, 0);
    clock_add_to_group(clock_xpi0, 0);
    clock_add_to_group(clock_xpi1, 0);
    clock_add_to_group(clock_gptmr0, 0);
    clock_add_to_group(clock_gptmr1, 0);
    clock_add_to_group(clock_gptmr2, 0);
    clock_add_to_group(clock_gptmr3, 0);
    clock_add_to_group(clock_gptmr4, 0);
    clock_add_to_group(clock_gptmr5, 0);
    clock_add_to_group(clock_gptmr6, 0);
    clock_add_to_group(clock_gptmr7, 0);
    clock_add_to_group(clock_uart0, 0);
    clock_add_to_group(clock_uart1, 0);
    clock_add_to_group(clock_uart2, 0);
    clock_add_to_group(clock_uart3, 0);
    clock_add_to_group(clock_uart4, 0);
    clock_add_to_group(clock_uart5, 0);
    clock_add_to_group(clock_uart6, 0);
    clock_add_to_group(clock_uart7, 0);
    clock_add_to_group(clock_uart8, 0);
    clock_add_to_group(clock_uart9, 0);
    clock_add_to_group(clock_uart10, 0);
    clock_add_to_group(clock_uart11, 0);
    // clock_add_to_group(clock_uart13, 0);
    // clock_add_to_group(clock_uart14, 0);
    // clock_add_to_group(clock_puart, 0);
    // clock_add_to_group(clock_i2c0, 0);
    // clock_add_to_group(clock_i2c1, 0);
    // clock_add_to_group(clock_i2c2, 0);
    // clock_add_to_group(clock_i2c3, 0);
    // clock_add_to_group(clock_spi0, 0);
    // clock_add_to_group(clock_spi1, 0);
    clock_add_to_group(clock_spi2, 0);
    // clock_add_to_group(clock_spi3, 0);
    // clock_add_to_group(clock_can0, 0);
    // clock_add_to_group(clock_can1, 0);
    clock_add_to_group(clock_can2, 0);
    clock_add_to_group(clock_can3, 0);
    // clock_add_to_group(clock_display, 0);
    // clock_add_to_group(clock_sdxc0, 0);
    // clock_add_to_group(clock_sdxc1, 0);
    // clock_add_to_group(clock_camera0, 0);
    // clock_add_to_group(clock_camera1, 0);
    clock_add_to_group(clock_ptpc, 0);
    clock_add_to_group(clock_ref0, 0);
    clock_add_to_group(clock_ref1, 0);
    clock_add_to_group(clock_watchdog0, 0);
    // clock_add_to_group(clock_eth0, 0);
    // clock_add_to_group(clock_eth1, 0);
    clock_add_to_group(clock_sdp, 0);
    clock_add_to_group(clock_xdma, 0);
    clock_add_to_group(clock_ram0, 0);
    clock_add_to_group(clock_ram1, 0);
    // clock_add_to_group(clock_usb0, 0);
    // clock_add_to_group(clock_usb1, 0);
    // clock_add_to_group(clock_jpeg, 0);
    clock_add_to_group(clock_pdma, 0);
    // clock_add_to_group(clock_kman, 0);
    clock_add_to_group(clock_gpio, 0);
    clock_add_to_group(clock_mbx0, 0);
    clock_add_to_group(clock_hdma, 0);
    // clock_add_to_group(clock_rng, 0);
    clock_add_to_group(clock_mot0, 0);
    clock_add_to_group(clock_mot1, 0);
    clock_add_to_group(clock_mot2, 0);
    clock_add_to_group(clock_mot3, 0);
    // clock_add_to_group(clock_acmp, 0);
    // clock_add_to_group(clock_dao, 0);
    clock_add_to_group(clock_msyn, 0);
    clock_add_to_group(clock_lmm0, 0);
    clock_add_to_group(clock_lmm1, 0);

    // clock_add_to_group(clock_adc0, 0);
    // clock_add_to_group(clock_adc1, 0);
    // clock_add_to_group(clock_adc2, 0);
    // clock_add_to_group(clock_adc3, 0);

    // clock_add_to_group(clock_i2s0, 0);
    // clock_add_to_group(clock_i2s1, 0);
    // clock_add_to_group(clock_i2s2, 0);
    // clock_add_to_group(clock_i2s3, 0);

    /* Add the CPU1 clock to Group1 */
    clock_add_to_group(clock_mchtmr1, 1);
    clock_add_to_group(clock_mbx1, 1);

    /* Connect Group0 to CPU0 */
    clock_connect_group_to_cpu(0, 0);

    if (status_success != pllctl_init_int_pll_with_freq(HPM_PLLCTL, 0, BOARD_CPU_FREQ))
    {
        printf("Failed to set pll0_clk0 to %dHz\n", BOARD_CPU_FREQ);
        while (1)
            ;
    }

    clock_set_source_divider(clock_cpu0, clk_src_pll0_clk0, 1);
    // clock_set_source_divider(clock_cpu1, clk_src_pll0_clk0, 1);
    /* Connect Group1 to CPU1 */
    //  clock_connect_group_to_cpu(1, 1);
}

void board_get_uuid(uint32_t *UUID)
{
    uint32_t uuid[4];
    uint16_t uuid_addr = 88;
    for (uint8_t i = 0; i < 4; i++)
    {
        uuid[i] = ROM_API_TABLE_ROOT->otp_driver_if->read_from_shadow(uuid_addr++);
    }
    UUID[0] = uuid[0] ^ uuid[1];
    UUID[1] = uuid[2] ^ uuid[3];
}