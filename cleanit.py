#!bin/env python
import sys
import os
import os.path
import string
import shutil

# delete ide template compile files
del_file_type = ['.o', '.bak', '.__i', '.crf', '.iex', '.lnp', '.tra', '.d', '.lst', '.dep',
                 '.TMP', '.axf', '.map', '.hex', '.lst', '.pbi', '.pbi.cout', '.pbd', '.pbd.browse', '.out',
                 '.scvd', '.zip'
                ]
# ide template folders
del_folder_name = ['debug', 'DebugConfig', 'Listings', 'Objects',]
# ide or debugger setting files
del_file_name = ['EventRecorderStub.scvd', 'JLinkLog.txt', 'JLinkSettings.ini']
# keil ide template files
del_keil_name = [".uvguix.", ".uvgui."]

# main process
def main(object):

    print("!!! Clean-Up Folder !!!")
    # Get Project Root Path
    path = os.path.abspath('..')
    print("Projct Root Path is " + path)
    # delete ide or debugger setting files
    print("*** Delete ide setting files ***")
    for root, dirs, files in os.walk(path):
        for name in files:
            for file_name in del_file_name:
                if(name == file_name):
                    os.remove(os.path.join(root,name))
                    print("*** Delete Files: " + name) 

    # delete ide template folders
    print("*** Delete Folders ***")
    for root, dirs, files in os.walk(path):
        for name in dirs:
            for folder_name in del_folder_name:
                if(name == folder_name):
                    shutil.rmtree(os.path.join(root,name))
                    print("*** Delete Folder: " + os.path.join(root,name)) 

    # delete ide template compile files
    print("*** Delete ide template files ***")
    for root, dirs, files in os.walk(path):
        for name in files:
           for file_tail in del_file_type:
                if(name.endswith(file_tail)):
                    os.remove(os.path.join(root,name))
                    print("*** Delete files: " + name)

    # delete keil ide template file
    print("*** Delete keil ide template files ***")
    for root, dirs, files in os.walk(path):
        for name in files:
           for file_tail in del_keil_name:
                if name.find(file_tail)>= 0:
                    os.remove(os.path.join(root,name))
                    print("*** Delete files: " + name)

    print("!!! End !!!")

if __name__ == '__main__':
    main(sys.argv)

